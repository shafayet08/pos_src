from accounts.models import CashFlow, CreditMethod
def create_cashflow_for_sale(obj, credit_method=None, is_debit=False, context=None, amount=None):
    if amount is None:
        amount = obj.get_total_payable

    if credit_method is not None:
        credit_method = CreditMethod.objects.get(method=credit_method)
        CashFlow.objects.create(
            credit_method=credit_method,
            description=context,
            amount=amount,
            remaining_credit=credit_method.total_amount,
            is_debit=is_debit,)
    else:
        raise ValueError('went somethig wrong')



# A callable function-1(CreditMethod) for creating 'CashFlow'.
def create_cashflow(credit_method, context, amount, is_debit=False):
    CashFlow.objects.create(
        credit_method=credit_method,
        description=context,
        amount=amount,
        remaining_credit=credit_method.total_amount,
        is_debit=is_debit,)
