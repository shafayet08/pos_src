from django.urls import path

from . import views

app_name = 'suppliers'
urlpatterns = [
    path('supplier/', views.supplier, name='supplier'),
    path('create-supplier/', views.create_supplier, name='create_supplier'),
    path('update-supplier/<int:pk>/', views.update_supplier, name='update_supplier'),
    path('supplier-profile/<int:pk>/', views.supplier_profile, name='supplier_profile'),
    path('invoicewise-supplier-payments/<int:pk>', views.invoicewise_supplier_payments, name='invoicewise_supplier_payments')
    
]
