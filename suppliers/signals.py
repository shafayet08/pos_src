from django.core.files import File
from django.db.models.signals import post_save, pre_save
from django.dispatch import receiver
from django.core.files.base import ContentFile


from PIL import Image
import barcode
from barcode.writer import ImageWriter
from io import BytesIO


from suppliers.models import Supplier

@receiver(pre_save, sender=Supplier)
def generate_Product_thumbnail(sender, instance, **kwargs):
    supplier_img = Image.open(instance.picture)
    supplier_img = supplier_img.convert("RGB")

    if supplier_img.height > 300 or supplier_img.width > 300:
        THUMBNAIL_SIZE = (300, 300)
        supplier_img.thumbnail(THUMBNAIL_SIZE)
        temp_thumb = BytesIO()
        supplier_img.save(temp_thumb, 'JPEG')
        temp_thumb.seek(0)

        instance.picture.save(
            instance.picture.name,
            ContentFile(temp_thumb.read()),
            save=False,
        )
        temp_thumb.close()
