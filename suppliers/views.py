from django.shortcuts import render, redirect
from django.contrib import messages

from django.contrib.auth.decorators import login_required, user_passes_test


from suppliers.models import Supplier
from suppliers.forms import SupplierModelForm
from purchases.models import PurchaseInvoice




@login_required()
@user_passes_test(lambda u: u.is_active or u.is_manager or u.is_owner or u.is_superuser)
def supplier(request):
    suppliers = Supplier.objects.all().order_by('-created')
    form = SupplierModelForm()
    context = {
        'suppliers':suppliers,
        'form': form,
        'title': 'Supplier'
    }
    return render(request, 'suppliers/supplier.html', context)



@login_required()
@user_passes_test(lambda u: u.is_active or u.is_manager or u.is_owner or u.is_superuser)
def create_supplier(request):
    if request.method == 'POST':
        form = SupplierModelForm(request.POST, request.FILES)
        if form.is_valid():
            instance = form.save(commit=False)
            instance.added_by = request.user
            instance.save()
            messages.success(request, 'Supplier has been successfully created !')
            return redirect(request.META['HTTP_REFERER'])
    return redirect(request.META['HTTP_REFERER'])


@login_required()
@user_passes_test(lambda u: u.is_active or u.is_manager or u.is_owner or u.is_superuser)
def update_supplier(request, pk):
    supplier = Supplier.objects.get(pk=pk)
    form = SupplierModelForm(instance=supplier)
    if request.method == 'POST':
        form = SupplierModelForm(request.POST, request.FILES, instance=supplier)
        if form.is_valid():
            form.save()
            messages.success(request, 'Supplier has been successfully updated ')
            return redirect('suppliers:supplier')
        else:
            messages.error(request, 'Oops! Something went wrong')
            return redirect(request.META['HTTP_REFERER'])
    
    context = {
        'form': form,
    }

    return render(request, 'suppliers/supplier-update-form.html', context)

@login_required()
@user_passes_test(lambda u: u.is_active or u.is_manager or u.is_owner or u.is_superuser)
def supplier_profile(request, pk):
    title = 'Supplier Profile'
    supplier = Supplier.objects.get(pk=pk)
    purchase_invoices = supplier.purchaseinvoice_set.all()
    supplier_payments = supplier.supplierpayment_set.all()

    context = {
        'title': title,
        'supplier': supplier,
        'purchase_invoices': purchase_invoices,
        'supplier_payments': supplier_payments,
    }

    return render(request, 'suppliers/supplier-profile.html', context)



@login_required()
@user_passes_test(lambda u: u.is_active or u.is_salesman or u.is_manager or u.is_owner or u.is_superuser)
def invoicewise_supplier_payments(request, pk):
    purchase_invoice = PurchaseInvoice.objects.get(id=pk)
    supplier_payments = purchase_invoice.supplierpayment_set.all()
    
    context = {
        'title': 'Invoicewise Supplier Payments',
        'supplier_payments': supplier_payments,
    }
    return render(request, 'suppliers/individual-supplier-payment.html', context)

    
