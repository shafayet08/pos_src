from django import forms
from django.forms import fields

from suppliers.models import Supplier


class SupplierModelForm(forms.ModelForm):
    class Meta:
        model = Supplier
        exclude = ('added_by', 'updated_by',)


