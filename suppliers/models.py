from django.db import models
from django.conf import settings
from base_model.base import CommonInfo



class Supplier(CommonInfo):
    picture         = models.ImageField(upload_to='supplier', default='supplier/avatar.png')
    added_by        = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True, related_name='supplier_add_user')
    updated_by      = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True, blank=True, related_name='supplier_edit_user')
    address2        = None
    def __str__(self):
        return f'{self.first_name} {self.last_name}'