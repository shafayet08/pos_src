from django.urls import path
from inventories import views

app_name = 'inventories'
urlpatterns = [
    path('inventory/', views.inventory, name='inventory'),

]