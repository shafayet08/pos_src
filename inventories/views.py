from django.shortcuts import render


from django.contrib.auth.decorators import login_required, user_passes_test

from inventories.models import Inventory




@login_required()
@user_passes_test(lambda u: u.is_active or u.is_salesman or u.is_manager or u.is_owner or u.is_superuser)
def inventory(request):
    inventories = Inventory.objects.all()
    context = {
        'inventories': inventories,
    }
    return render(request, 'inventories/inventory.html', context)
