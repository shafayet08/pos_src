from django.db import models
from django.db.models import fields


from products.models import Product
from sales.models import ReturnProduct, Sale, SaleItem, DamageProduct
# Create your models here.
class Inventory(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE, null=True)
    created = models.DateTimeField(auto_now_add=True)
    # def __str__(self):
    #     if self.product.name != None:
    #         return f'{self.product.name} {self.product.id}'

    @property
    def get_total_purchased_price(self):
        try:
            return self.product.purchase_qty * self.product.cost_price
        except:
            pass

    @property
    def get_total_selling_price(self):
        product_id = self.product.id
        products = SaleItem.objects.filter(product=product_id)
        total = 0
        for product in products:
            total = total + product.selling_price
        return total
    
    @property
    def get_total_return_product(self):
        return_products = ReturnProduct.objects.filter(product=self.product)
        total = 0
        for product in return_products:
           total = total + product.qty
        return total


    @property
    def get_total_damaged_product(self):
        damage_products = DamageProduct.objects.filter(product=self.product)
        total = 0
        for product in damage_products:
            total = total + product.damage_qty
        return total

    @property
    def get_total_sold_product(self):
        sale_items = SaleItem.objects.filter(product=self.product)
        return sum(item.quantity for item in sale_items)
    
    @property
    def get_total_sold_price(self):
        sale_items = SaleItem.objects.filter(product=self.product)
        return sum(item.get_total_price for item in sale_items)

    @property
    def get_total_remaining_product(self):
        try:
            return self.product.purchase_qty - (self.get_total_sold_product + self.get_total_damaged_product)
        except:
            pass
    
    @property
    def get_total_remaining_purchase_amount(self):
        try:
            return self.product.cost_price * self.get_total_remaining_product
        except:
            pass
    

    @property
    def get_total_remaining_selling_amount(self):
        try:
            return self.product.selling_price * self.get_total_remaining_product
        except:
            pass