$(document).ready(function(){
    const csrf = $('input[name=csrfmiddlewaretoken').val();
    // Create Cash payment
    $('#js-cash-payment').click(function(){
        $('#cash-payment-modal').dialog({
            title: 'Do you want to proceed?',
            width: 400,
            height: 300,
            modal: true,
            resizable: false,
            draggable: true,

        });



        $('#no').click(function(e){
            e.stopImmediatePropagation()
            $('#cash-payment-modal').dialog('close');   
        })


        $('#yes').click(function(e){
            e.stopImmediatePropagation()
            const product_names = [];
            const product_qty = [];
            const product_price = [];
            const cart_item = new Array();
            $('#style-custom-narrow .js-product').each(function(){
                var name = $(this).text()
                product_names.push(name)
                
            });
            $('#style-custom-narrow .js-qty-editable').each(function(){
                var qty = parseInt($(this).text());
                product_qty.push(qty);
                
            });
            $('#style-custom-narrow .js-price-editable').each(function(){
                var price = parseInt($(this).text());
                product_price.push(price);
                
            });
            if(product_names != ''){
                for(var i=0; i < product_names.length; i++){
                    obj = {'name': product_names[i], 'qty': product_qty[i], 'price': product_price[i], 'is_complete': 'True'};
                    cart_item.push(obj);
                };
            };

            $('#cash-payment-modal').dialog('close');        
            if(product_names == ''){
                // alert_('error', "toast-top-full-width", '<strong>There is no product item in the cart to proceed!</strong> Please add item first.');
                Toast.fire({
                    icon: 'error',
                    title: 'Oops! There is no product item in the cart to proceed. Please add product item first.'
                }) 
                $(this).off()
            }
            
            

            if(product_names != ''){
                var payment_method = $('.payment-method-selection').val();
                var other_fee = parseInt($('#id_other_fee').val()) || 0;
                var is_discount_parcentage = $('#id_dis_checkbox').is(":checked");
                var discount = parseInt($('#id_discount').val()) || 0;
                var take_amount = parseInt($('#id_paid_or_take').val()) || 0;
                var due_amount = parseInt($('#id_due_amount').text()) || 0;
                var return_amount = parseInt($('#id_return_amount').text())
                var customer = $('#customer-search').val();
                var delivery_date = $('#calendar').val();
                var cart_payment_table = {
                        'delivery_date': delivery_date,
                        'customer': customer,
                        'payment_method': payment_method,
                        'other_fee': other_fee,
                        'is_discount_parcentage': is_discount_parcentage,
                        'discount': discount,
                        'take_amount': take_amount,
                        'due_amount': due_amount,
                        'return_amount': return_amount,
                        'is_complete': true,
                        'is_walking_customer': false,
                    };


                if(take_amount == 0){
                    // alert_('error', "toast-top-full-width", "Please fill <strong>Pay/Take</strong> field");
                    Toast.fire({
                        icon: 'error',
                        title: 'Please add payment'
                    })
                    $('#id_paid_or_take').focus();
                    $(this).off()
                }else if(due_amount > 0){
                    Toast.fire({
                        icon: 'error',
                        title: 'Please place order because you have due amount'
                    })
                    $(this).off()
                }else if(customer == ''){
                    // alert_('error', "toast-top-full-width", "Please fill <strong>Customer</strong> field");
                    Toast.fire({
                        icon: 'error',
                        title: 'Please add customer.'
                    })
                    $(this).off()
                    $('#customer-search').focus();
                }else{
                    if(delivery_date == ''){
                        // alert_('error', "toast-top-full-width", "Please select <strong>Customer</strong> delivery date");
                        Toast.fire({
                            icon: 'error',
                            title: 'Please add delivery date'
                        })
                        $('#calendar').focus();
                        $(this).off()
                    }
                };

                if(take_amount != 0 && customer != '' && delivery_date != '' && due_amount == 0){
                    $.ajax({
                        type: 'POST',
                        url: '/success-payment/',
                        dataType: 'json',
                        data: {
                            'csrfmiddlewaretoken': csrf,
                            'cart_item': JSON.stringify(cart_item),
                            'cart_payment_table': JSON.stringify(cart_payment_table),
                        },
                        success: function(response){
                            if(response.payment_success == true){
                                window.location.reload();
                                Toast.fire({
                                    icon: 'success',
                                    title: '<strong>Success! </strong> Payment completed'
                                });
                                
                            }else{
                                Toast.fire({
                                    icon: 'error',
                                    title: '<strong>Error! </strong> Customer does not exist, please add customer first.'
                                });
                            }

                        },

                        error: function(response){
                            console.log(response);
                        }
                    });
                }
                
            };
        }); // yes event ended
    }); // js-cash-payment ended






    // Create Orders
    $('#js-create-order').click(function(){
        $('#cash-payment-modal').dialog({
            title: 'Do you want to proceed?',
            width: 400,
            height: 300,
            modal: true,
            resizable: false,
            draggable: true,

        });



        $('#no').click(function(e){
            e.stopImmediatePropagation()
            $('#cash-payment-modal').dialog('close');   
        })


        $('#yes').click(function(e){
            e.stopImmediatePropagation()
            const product_names = [];
            const product_qty = [];
            const product_price = [];
            const cart_item = new Array();
            $('#style-custom-narrow .js-product').each(function(){
                var name = $(this).text()
                product_names.push(name)
                
            });
            $('#style-custom-narrow .js-qty-editable').each(function(){
                var qty = parseInt($(this).text());
                product_qty.push(qty);
                
            });
            $('#style-custom-narrow .js-price-editable').each(function(){
                var price = parseInt($(this).text());
                product_price.push(price);
                
            });
            if(product_names != ''){
                for(var i=0; i < product_names.length; i++){
                    obj = {'name': product_names[i], 'qty': product_qty[i], 'price': product_price[i], 'is_complete': 'False'};
                    cart_item.push(obj);
                };
            };

            $('#cash-payment-modal').dialog('close');        
            if(product_names == ''){
                // alert_('error', "toast-top-full-width", '<strong>There is no product item in the cart to proceed!</strong> Please add item first.');
                Toast.fire({
                    icon: 'error',
                    title: 'Oops! There is no product item in the cart to proceed. Please add product item first.'
                }) 
                $(this).off();
            }

            if(product_names != ''){
                var payment_method = $('.payment-method-selection').val();
                var other_fee = parseInt($('#id_other_fee').val()) || 0;
                var is_discount_parcentage = $('#id_dis_checkbox').is(":checked");
                var discount = parseInt($('#id_discount').val()) || 0;
                var take_amount = parseInt($('#id_paid_or_take').val()) || 0;
                var due_amount = parseInt($('#id_due_amount').text()) || 0;
                var return_amount = parseInt($('#id_return_amount').text())
                var customer = $('#customer-search').val();
                var delivery_date = $('#calendar').val();
                var cart_payment_table = {
                        'delivery_date': delivery_date,
                        'customer': customer,
                        'payment_method': payment_method,
                        'other_fee': other_fee,
                        'is_discount_parcentage': is_discount_parcentage,
                        'discount': discount,
                        'take_amount': take_amount,
                        'due_amount': due_amount,
                        'return_amount': return_amount,
                        'is_complete': false,
                        'is_walking_customer': false,
                    };

                if(take_amount == 0){
                    // alert_('error', "toast-top-full-width", "Please fill <strong>Pay/Take</strong> field");
                    Toast.fire({
                        icon: 'error',
                        title: 'Please add payment'
                    })
                    $('#id_paid_or_take').focus(); 
                    $(this).off();
                }else if(customer == ''){
                    // alert_('error', "toast-top-full-width", "Please fill <strong>Customer</strong> field");
                    Toast.fire({
                        icon: 'error',
                        title: 'Please add customer.'
                    })
                    $('#customer-search').focus();
                    $(this).off()
                }else{
                    if(delivery_date == ''){
                        // alert_('error', "toast-top-full-width", "Please select <strong>Customer</strong> delivery date");
                        Toast.fire({
                            icon: 'error',
                            title: 'Please add delivery date'
                        })
                        $('#calendar').focus();
                        $(this).off()
                    }
                };


                if(take_amount != 0 && customer != '' && delivery_date != ''){
                    $.ajax({
                        type: 'POST',
                        url: '/success-payment/',
                        dataType: 'json',
                        data: {
                            'csrfmiddlewaretoken': csrf,
                            'cart_item': JSON.stringify(cart_item),
                            'cart_payment_table': JSON.stringify(cart_payment_table),
                        },
                        success: function(response){
                            console.log('success ',response)
                            Toast.fire({
                                icon: 'success',
                                title: '<strong>Success!</strong> Oder placed'
                            });
                            location.reload()
                        },
                    });
                };
                
            };
        }); // yes event ended
    }); // js-create-order ended








    // Payment for Walking Customer
    $('#js-walking-customer').click(function(){
        $('#cash-payment-modal').dialog({
            title: 'Do you want to proceed?',
            width: 400,
            height: 300,
            modal: true,
            resizable: false,
            draggable: true,

        });



        $('#no').click(function(e){
            e.stopImmediatePropagation()
            $('#cash-payment-modal').dialog('close');
        })


        $('#yes').click(function(e){
            e.stopImmediatePropagation()
            const product_names = [];
            const product_qty = [];
            const product_price = [];
            const cart_item = new Array();
            $('#style-custom-narrow .js-product').each(function(){
                var name = $(this).text()
                product_names.push(name)
                
            });
            $('#style-custom-narrow .js-qty-editable').each(function(){
                var qty = parseInt($(this).text());
                product_qty.push(qty);
                
            });
            $('#style-custom-narrow .js-price-editable').each(function(){
                var price = parseInt($(this).text());
                product_price.push(price);
                
            });
            if(product_names != ''){
                for(var i=0; i < product_names.length; i++){
                    obj = {'name': product_names[i], 'qty': product_qty[i], 'price': product_price[i], 'is_complete': 'True'};
                    cart_item.push(obj);
                };
            };

            $('#cash-payment-modal').dialog('close');        
            if(product_names == ''){
                // alert_('error', "toast-top-full-width", '<strong>There is no product item in the cart to proceed!</strong> Please add item first.');
                Toast.fire({
                    icon: 'error',
                    title: 'Oops! There is no product item in the cart to proceed. Please add product item first.'
                }) 
                $(this).off();
            }

            if(product_names != ''){
                var payment_method = $('.payment-method-selection').val();
                var other_fee = parseInt($('#id_other_fee').val()) || 0;
                var is_discount_parcentage = $('#id_dis_checkbox').is(":checked");
                var discount = parseInt($('#id_discount').val()) || 0;
                var take_amount = parseInt($('#id_paid_or_take').val()) || 0;
                var due_amount = parseInt($('#id_due_amount').text()) || 0;
                var return_amount = parseInt($('#id_return_amount').text())
                var customer = $('#customer-search').val();
                var delivery_date = $('#calendar').val();
                var cart_payment_table = {
                        'delivery_date': delivery_date,
                        'customer': customer,
                        'payment_method': payment_method,
                        'other_fee': other_fee,
                        'is_discount_parcentage': is_discount_parcentage,
                        'discount': discount,
                        'take_amount': take_amount,
                        'due_amount': due_amount,
                        'return_amount': return_amount,
                        'is_complete': true,
                        'is_walking_customer': true,
                    };

                if(take_amount == 0){
                    // alert_('error', "toast-top-full-width", "Please fill <strong>Pay/Take</strong> field");
                    Toast.fire({
                        icon: 'error',
                        title: 'Please add payment'
                    })
                    $('#id_paid_or_take').focus();
                }else if(due_amount > 0){
                    Toast.fire({
                        icon: 'error',
                        title: 'Please place order because you have due amount'
                    })
                    $(this).off() 
                }else{
                    if(delivery_date == ''){
                        // alert_('error', "toast-top-full-width", "Please select <strong>Customer</strong> delivery date");
                        Toast.fire({
                            icon: 'error',
                            title: 'Please add delivery date'
                        })
                        $('#calendar').focus();
                        $(this).off() 
                    }
                };

                if(take_amount != 0 && delivery_date != '' && due_amount == 0){
                    $.ajax({
                        type: 'POST',
                        url: '/success-payment/',
                        dataType: 'json',
                        data: {
                            'csrfmiddlewaretoken': csrf,
                            'cart_item': JSON.stringify(cart_item),
                            'cart_payment_table': JSON.stringify(cart_payment_table),
                        },
                        success: function(response){
                            if(response.payment_success == true){
                                window.location.reload();
                                Toast.fire({
                                    icon: 'success',
                                    title: '<strong>Success! </strong> Payment completed'
                                });                               
                            }

                        },

                    });
                };
                
            };
        }); // yes event ended
    }); // js-cash-payment ended

});