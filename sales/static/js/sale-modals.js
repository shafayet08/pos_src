$(document).ready(function(){
    $('.js-update-sale').click(function(){
       var update_id = $(this).data('update_id').toString();
       console.log(update_id)

        $('#update-sale-modal-show').dialog({
            title: 'Complete the order',
            modal: true,
            resizable: false,
            draggable: true,
    
        });
    });




    // # Deleting Sale 
    $('.js-delete-sale').click(function(){
        var sale_id = $(this).data('sale_id')
        const csrf = $('input[name=csrfmiddlewaretoken').val(); 
        $('#sale-delete-modal-show').dialog({
            title: 'Do you want to delete?',
            modal: true,
            resizable: false,
            draggable: true,
    
        });
        $('#sale_delete_no').click(function(e){
            e.stopImmediatePropagation()
            $('#sale-delete-modal-show').dialog('close'); 
        })
        $('#sale_delete_yes').click(function(){
            $.ajax({
                type: 'POST',
                url: '/delete-sale/',
                dataType: 'json',
                data: {
                    'csrfmiddlewaretoken': csrf,
                    'sale_id': sale_id,
                },
                success: function(response){
                    if(response.data == true){
                        $('#sale-delete-modal-show').dialog('close'); 
                        Toast.fire({
                            icon: 'success',
                            title: 'Success! The item successfully deleted.'
                        });
                        setTimeout(function(){
                            location.reload()
                        },2000);
                        
                        
                    }else{
                        $('#sale-delete-modal-show').dialog('close'); 
                        Toast.fire({
                            icon: 'error',
                            title: 'error! Somthing went wrong.'
                        });
                    }
                } 
            });

        });
    });// # Deleting Sale ended






    $('#add-return-product').click(function(){
        $('#show-return-product-modal').dialog({
            title: 'Add New Return Product',
            width: 600,
            modal: true,
            resizable: false,
            draggable: true,
    
        });
    });

        // # Deleting Return Product start
        $('.js-delete-return-product').click(function(){
            var return_product_id = $(this).data('return_product_id')
            const csrf = $('input[name=csrfmiddlewaretoken').val(); 
            $('#return-product-delete-modal-show').dialog({
                title: 'Do you want to delete?',
                modal: true,
                resizable: false,
                draggable: true,
    
            });
            $('#return_product_delete_no').click(function(e){
                e.stopImmediatePropagation()
                $('#return-product-delete-modal-show').dialog('close'); 
            })
            $('#return_product_delete_yes').click(function(){
                $.ajax({
                    type: 'POST',
                    url: '/delete-return-product/',
                    dataType: 'json',
                    data: {
                        'csrfmiddlewaretoken': csrf,
                        'return_product_id': return_product_id,
                    },
                    success: function(response){
                        if(response.data == true){
                            $('#sale-delete-modal-show').dialog('close'); 
                            Toast.fire({
                                icon: 'success',
                                title: 'Success! The item successfully deleted.'
                            });
                            setTimeout(function(){
                                location.reload()
                            },1000);
                            
                            
                        }else{
                            $('#sale-delete-modal-show').dialog('close'); 
                            Toast.fire({
                                icon: 'error',
                                title: 'error! Somthing went wrong.'
                            });
                        }
                    } 
                });
    
            });
        });// # Deleting Return Product ended
    

    $('#add-damage-product').click(function(){
        $('#show-damage-product-modal').dialog({
            title: 'Add New Return Product',
            width: 500,
            modal: true,
            resizable: false,
            draggable: true,
    
        });
    });




    // # Deleting Damage Product 
    $('.js-delete-damage-product').click(function(){
        var damage_product_id = $(this).data('damage_product_id')
        const csrf = $('input[name=csrfmiddlewaretoken').val(); 
        $('#damage-product-delete-modal-show').dialog({
            title: 'Do you want to delete?',
            modal: true,
            resizable: false,
            draggable: true,

        });
        $('#damage_product_delete_no').click(function(e){
            e.stopImmediatePropagation()
            $('#damage-product-delete-modal-show').dialog('close'); 
        })
        $('#damage_product_delete_yes').click(function(){
            $.ajax({
                type: 'POST',
                url: '/delete-damage-product/',
                dataType: 'json',
                data: {
                    'csrfmiddlewaretoken': csrf,
                    'damage_product_id': damage_product_id,
                },
                success: function(response){
                    if(response.data == true){
                        $('#sale-delete-modal-show').dialog('close'); 
                        Toast.fire({
                            icon: 'success',
                            title: 'Success! The item successfully deleted.'
                        });
                        setTimeout(function(){
                            location.reload()
                        },1000);
                        
                        
                    }else{
                        $('#sale-delete-modal-show').dialog('close'); 
                        Toast.fire({
                            icon: 'error',
                            title: 'error! Somthing went wrong.'
                        });
                    }
                } 
            });

        });
    });// # Deleting Damage Product ended



})