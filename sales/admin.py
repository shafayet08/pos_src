from django.contrib import admin

from sales.models import Sale, SaleItem, ReturnProduct, DamageProduct

admin.site.register(Sale)
admin.site.register(SaleItem)
admin.site.register(ReturnProduct)
admin.site.register(DamageProduct)