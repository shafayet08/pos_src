from django.core.files import File
from django.db.models.signals import post_save, pre_delete
from django.dispatch import receiver


from sales.models import ReturnProduct, Sale
from accounts.models import CreditMethod
from products.models import Product
from accounts.models import CashFlow



# A callable function-1(CreditMethod) for creating 'CashFlow'.
# def create_cashflow_for_sale(obj, credit_method=None, is_debit=False, extra_context=None):
#     context = {
#         'sale id': obj.id,
#     }
#     if extra_context is not None:
#         context.update(extra_context)

#     if credit_method is not None:
#         credit_method = CreditMethod.objects.get(method=credit_method)
#         print(obj.get_total_payable)
#         CashFlow.objects.create(
#             credit_method=credit_method,
#             description=context,
#             amount=obj.get_total_payable,
#             remaining_credit=credit_method.total_amount,
#             is_debit=is_debit,)
#     else:
#         raise ValueError('went somethig wrong')

# @receiver(post_save, sender=Sale)
# def create_cashflow(sender, instance, created, **kwargs):
#     if created or created == False:        
#         #Creating and updating CashFlow Table
#         obj = Sale.objects.get(id=instance.id)
#         extra_context = {
#             'title': 'Sale',
#             'customer': obj.customer.get_full_name
#         }
#         is_debit = False
#         credit_method = instance.payment_method
#         # Calling function-1(CreditMethod) for creating 'CashFlow'.
#         create_cashflow_for_sale(obj, credit_method, is_debit, extra_context)



@receiver(pre_delete, sender=Sale)
def pre_sale_delete(sender, instance, **kwargs):
    if instance.is_complete == False:
        sale_items = instance.saleitem_set.all()
        for item in sale_items:
            product_id = item.product.id
            product = Product.objects.get(id=product_id) 
            product.purchase_qty = product.purchase_qty + item.quantity
            product.save()
            

