from django.db import models
from django.conf import settings
from django.utils.translation import ugettext_lazy as _


from products.models import Product
from customers.models import Customer

# Create your models here.

class Sale(models.Model):
    CASH = 'cash'
    MOBILE_BANKING = 'mobile banking'
    BANK  = 'bank'
    CHEQUE = 'cheque'

    CHOICES = [
       (CASH, _('Cash')),
       (MOBILE_BANKING, _('Mobile Banking')),
       (BANK, _('Bank')),
       (CHEQUE, _('Cheque')),
   ]

    customer               = models.ForeignKey(Customer, on_delete=models.SET_NULL, null=True, blank=True)
    payment_method         = models.CharField(max_length=15, choices=CHOICES)
    delivery_date          = models.DateTimeField(auto_now=True)
    selling_date           = models.DateTimeField(auto_now_add=True)
    other_fee              = models.IntegerField(default=0)
    discount               = models.IntegerField(default=0)
    is_discount_parcentage = models.BooleanField()
    take_amount            = models.IntegerField(default=0)
    due_amount             = models.IntegerField(default=0)
    return_amount          = models.IntegerField(default=0)
    is_complete            = models.BooleanField()
    is_walking_customer    = models.BooleanField()
    added_by               = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null= True, related_name='add+')
    edited_by              = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null= True, related_name='edit+', blank=True)

    

    def __str__(self):
        try:
            return f'Invoice Id-{str(self.id)}'
        except:
            pass

    @property
    def get_sub_total(self):
        sale_items = self.saleitem_set.all()
        return sum(sale_item.get_total_price for sale_item in sale_items)
    
    @property
    def get_total_qty(self):
        sale_items = self.saleitem_set.all()
        return sum(sale_item.quantity for sale_item in sale_items)

    
    @property
    def get_discount(self):
        if self.is_discount_parcentage:
            discount = int((self.get_sub_total * self.discount) / 100)
        else:
            discount = self.discount
        return discount


    @property
    def get_total_payable(self):
        return (self.get_sub_total + self.other_fee) - self.get_discount
    
    @property
    def get_total_cost_price(self):
        sale_items = self.saleitem_set.all()
        try:
            total = 0
            for item in sale_items:
                total = total + (item.product.cost_price * item.quantity)
            return total
        except:
            pass
    
    @property
    def get_total_sold_price(self):
        sale_items = self.saleitem_set.all()
        total = 0
        for item in sale_items:
            total = total + ((item.sold_price * item.quantity) - self.get_discount)
        return total




class SaleItem(models.Model):
    product       = models.ForeignKey(Product, on_delete=models.SET_NULL, null=True)
    sale          = models.ForeignKey(Sale, on_delete=models.CASCADE)
    quantity      = models.IntegerField()
    sold_price    = models.IntegerField()
    selling_date  = models.DateTimeField(auto_now_add=True)
    is_complete   = models.BooleanField(default=True)


    # def __str__(self):
    #     return f'{self.product.name}-{self.product.id}'
    

    @property
    def get_total_price(self):
        return self.sold_price * self.quantity 

    
# class Invoice(models.Model):
#     customer = models.ForeignKey(Customer, on_delete=models.SET_NULL, null=True)
#     sale = models.ForeignKey(Sale, on_delete=models.SET_NULL, null=True)


#Return Product from customer
class ReturnProduct(models.Model):
    sale        = models.ForeignKey(Sale, on_delete=models.SET_NULL, null=True, verbose_name='Invoice')
    product     = models.ForeignKey(Product, on_delete=models.SET_NULL, null=True)
    qty         = models.IntegerField(default=0)
    cost_price  = models.IntegerField(default=0)
    sold_price  = models.IntegerField(default=0)
    selling_date = models.DateTimeField(null=True, blank=True)
    created     = models.DateTimeField(auto_now_add=True)
    added_by    = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null= True, related_name='add+')
    edited_by   = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null= True, related_name='edit+', blank=True)


    @property
    def get_customer(self):
        try:
            return self.sale.customer.get_full_name
        except:
            return 'Walking Customer'

class DamageProduct(models.Model):
    product    = models.ForeignKey(Product, on_delete=models.SET_NULL, null=True)
    damage_qty = models.IntegerField(default=0)
    cost_price  = models.IntegerField(default=0)
    created = models.DateField(auto_now_add=True)
    damaged_by = models.CharField(max_length=30)
    added_by   = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null= True, related_name='add+')

    @property
    def get_total_damage_amount(self):
        return  self.damage_qty * self.cost_price