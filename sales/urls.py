from django.urls import path
from sales import views

app_name = 'sales'
urlpatterns = [
    path('success-payment/', views.success_payment, name='success_payment'),
    path('update-sale/<int:pk>/', views.update_sale, name='update_sale'),
    path('delete-sale/', views.delete_sale, name='delete_sale'),
    path('sold-item/', views.sold_item, name='sold_item'),
    path('pending-order/', views.pending_order, name='pending_order'),
    path('sale-invoice/<int:pk>/', views.sale_invoice, name='sale_invoice'),

    path('return-sold-product/', views.return_sold_product, name='return_sold_product'),
    path('delete-return-product/', views.delete_return_product, name='delete_return_product'),
   
    
    path('damage-product/', views.damage_product, name='damage_product'),
    path('add-damage-product/', views.add_damage_product, name='add_damage_product'),
    path('delete-damage-product/', views.delete_damage_product, name='delete_damage_product'),

    path('sold-report/', views.sold_report, name='sold_report'),
    path('order-report/', views.order_report, name='order_report'),
    path('damage-product-report/', views.damage_product_report, name='damage_product_report'),
    path('return-product-report/', views.return_product_report, name='return_product_report'), 
    
]