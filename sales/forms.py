from django import forms

from sales.models import ReturnProduct, Sale, DamageProduct


class SaleUpdateForm(forms.ModelForm):
    class Meta:
        model = Sale
        fields = ['is_complete']


class ReturnProductForm(forms.ModelForm):
    class Meta:
        model = ReturnProduct
        exclude = ['added_by', 'edited_by', 'cost_price', 'sold_price', 'selling_date']

class DamageProductForm(forms.ModelForm):
    class Meta:
        model = DamageProduct
        exclude = ['added_by', 'edited_by', 'cost_price']