from django.utils.timezone import make_aware
from accounts.models import Credit, CreditMethod
from django.http.response import HttpResponseRedirect
from django.urls import reverse
from products.models import Product
from django.shortcuts import redirect, render, get_object_or_404
from django.http import JsonResponse
from django.contrib.auth.models import User
from django.db.models import Q
from django.contrib import messages
from django.utils.timezone import make_aware
from django.db import transaction
from django.contrib.auth.decorators import login_required, user_passes_test

from dateutil import parser
import datetime
import json

from purchases.models import Purchase
from customers.models import Customer
from sales.models import DamageProduct, ReturnProduct, Sale, SaleItem
from sales.forms import SaleUpdateForm, ReturnProductForm, DamageProductForm
from app_settings.models import GeneralSetting
from inventories.models import Inventory
from utils.create_cashflow import create_cashflow_for_sale


@login_required()
@user_passes_test(lambda u: u.is_active or u.is_salesman or u.is_manager or u.is_owner or u.is_superuser)
def sold_item(request):
    sold_items = Sale.objects.filter(is_complete=True)
    return render(request, 'sales/sold-items.html', {'sold_items': sold_items})


@login_required()
@user_passes_test(lambda u: u.is_active or u.is_salesman or u.is_manager or u.is_owner or u.is_superuser)
def pending_order(request):
    pending_orders = Sale.objects.filter(is_complete=False)
    return render(request, 'sales/pending-orders.html', {'pending_orders': pending_orders})


def success_payment_for_existing_customer(request, cart_payment_table, cart_items, payment_method, customer=None,):
    sale = Sale.objects.create(
        customer=customer,
        payment_method=payment_method,
        delivery_date=cart_payment_table['delivery_date'],
        other_fee=cart_payment_table['other_fee'],
        discount=cart_payment_table['discount'],
        is_discount_parcentage=cart_payment_table['is_discount_parcentage'],
        is_complete=cart_payment_table['is_complete'],
        take_amount=cart_payment_table['take_amount'],
        due_amount=cart_payment_table['due_amount'],
        return_amount=cart_payment_table['return_amount'],
        is_walking_customer=cart_payment_table['is_walking_customer'],
        added_by=request.user,
    )

    is_complete = cart_payment_table['is_complete']
    if is_complete:
        sale.edited_by = request.user
        sale.save()

    for item in cart_items:
        product = Product.objects.get(name=item['name'])
        SaleItem.objects.create(
            product=product,
            sale=sale,
            quantity=item['qty'],
            sold_price=item['price'],
            is_complete=item['is_complete'])

        # Subtracting the sold quantity from the total purchases quantity
        # product.purchase_qty = product.purchase_qty - item['qty']
        # product.save()

    '''
    Adding total payable value of products to the Accounts.
    If there is due amount it means it is and order. That is why total payable
    value will not be added to the Accounts
    '''

    if sale.due_amount == 0 and is_complete:
        creditd_method = CreditMethod.objects.get(method=payment_method)
        creditd_method.total_amount = creditd_method.total_amount + sale.get_total_payable
        creditd_method.save()

        # Creating CashFlow
        obj = sale
        credit_method = obj.payment_method
        is_debit = False
        context = {
            'invoice id': obj.id,
            'title': 'sale',
            'customer': obj.customer.get_full_name
        }
        create_cashflow_for_sale(obj, credit_method, is_debit, context)


def success_payment_for_walking_customer(request, cart_payment_table, cart_items, payment_method, customer=None,):
    sale = Sale.objects.create(
        payment_method=payment_method,
        delivery_date=cart_payment_table['delivery_date'],
        other_fee=cart_payment_table['other_fee'],
        discount=cart_payment_table['discount'],
        is_discount_parcentage=cart_payment_table['is_discount_parcentage'],
        is_complete=cart_payment_table['is_complete'],
        take_amount=cart_payment_table['take_amount'],
        due_amount=cart_payment_table['due_amount'],
        return_amount=cart_payment_table['return_amount'],
        is_walking_customer=cart_payment_table['is_walking_customer'],
        added_by=request.user,)

    is_complete = cart_payment_table['is_complete']
    if is_complete:
        sale.edited_by = request.user
        sale.save()

    for item in cart_items:
        product = Product.objects.get(name=item['name'])
        SaleItem.objects.create(
            product=product,
            sale=sale,
            quantity=item['qty'],
            sold_price=item['price'],
            is_complete=item['is_complete']
        )

        # Subtracting the sold quantity from the total purchases quantity
        # product.purchase_qty = product.purchase_qty - item['qty']
        # product.save()
    '''
        Adding total payable value of products to the Accounts.
        If there is due amount it means it is and order. That is why total payable
        value will not be added to the Accounts
        '''

    if sale.due_amount == 0:
        creditd_method = CreditMethod.objects.get(method=payment_method)
        creditd_method.total_amount = creditd_method.total_amount + sale.get_total_payable
        creditd_method.save()

    # Creating CashFlow
    obj = sale
    credit_method = obj.payment_method
    is_debit = False
    context = {
        'invoice id': obj.id,
        'title': 'sale',
        'customer': 'walk-in customer'
    }
    create_cashflow_for_sale(obj, credit_method, is_debit, context)


# This is for creating orders and success payment
def success_payment(request):
    # sales_man = User.objects.get(username=request.user)
    if request.is_ajax:
        cart_payment_table = json.loads(request.POST.get('cart_payment_table'))
        cart_items = json.loads(request.POST.get('cart_item'))
        is_walking_customer = cart_payment_table['is_walking_customer']

    if cart_payment_table['payment_method'].lower() == 'card / bank':
        payment_method = 'bank'
    else:
        payment_method = cart_payment_table['payment_method'].lower()

    try:
        customer = Customer.objects.get(
            Q(phone1=cart_payment_table['customer']) | Q(
                phone2=cart_payment_table['customer'])
        )
        customer_exist = True
        customer = Customer.objects.get(id=customer.id)
    except Exception as e:
        print(e)
        customer_exist = False

    if is_walking_customer == False and customer_exist:
        print('is walking customer: ', is_walking_customer)
        try:
            with transaction.atomic():
                success_payment_for_existing_customer(
                    request, cart_payment_table, cart_items, payment_method, customer)
                response = {'payment_success': True, }
        except Exception as e:
            response = {'payment_success': False}
            print(e)

    if is_walking_customer == True:  # Create payment for Walking Customer
        print('is walking customer: ', is_walking_customer)
        try:
            with transaction.atomic():
                success_payment_for_walking_customer(
                    request, cart_payment_table, cart_items, payment_method)
                response = {'payment_success': True}
        except Exception as e:
            response = {'payment_success': False}
            print(e)

    return JsonResponse(response, safe=False)


# Updating Sale Order
@login_required()
@user_passes_test(lambda u: u.is_active or u.is_salesman or u.is_manager or u.is_owner or u.is_superuser)
def update_sale(request, pk):
    sale = Sale.objects.get(pk=pk)
    form = SaleUpdateForm(instance=sale)

    if request.method == 'POST':
        form = SaleUpdateForm(request.POST, instance=sale)
        if form.is_valid():
            with transaction.atomic():
                instance = form.save(commit=False)
                instance.delivery_date = datetime.datetime.now()
                instance.edited_by = request.user
                instance.take_amount = instance.take_amount + instance.due_amount
                instance.due_amount = 0
                instance.save()

                # Adding total due payment to CreditMethod
                credit_method = CreditMethod.objects.get(
                    method=sale.payment_method)
                credit_method.total_amount = credit_method.total_amount + sale.get_total_payable
                credit_method.save()

                # Creating CashFlow
                obj = sale
                credit_method = obj.payment_method
                is_debit = False
                context = {
                    'invoice id': obj.id,
                    'title': 'sale(order completed)',
                    'customer': obj.customer.get_full_name
                }
                create_cashflow_for_sale(obj, credit_method, is_debit, context)
                messages.success(
                    request, 'Success!  Order has been completed.')
                return redirect('products:sales_order')
    return render(request, 'sales/sale-update-form.html', {'form': form})


@login_required()
@user_passes_test(lambda u: u.is_active or u.is_manager or u.is_owner or u.is_superuser)
# Deleting Sale Order
def delete_sale(request):
    # ajax request
    if request.method == 'POST':
        sale_id = request.POST.get('sale_id')
        Sale.objects.get(id=sale_id).delete()
        data = True
    else:
        data = False

    return JsonResponse({'data': data})


@login_required()
@user_passes_test(lambda u: u.is_active or u.is_salesman or u.is_manager or u.is_owner or u.is_superuser)
def sale_invoice(request, pk):
    invoice = Sale.objects.get(pk=pk)
    sale_items = invoice.saleitem_set.all()
    company = GeneralSetting.objects.last()
    context = {
        'invoice': invoice,
        'company': company,
        'sale_items': sale_items,
    }
    return render(request, 'sales/sale-invoice.html', context)


@login_required()
@user_passes_test(lambda u: u.is_active or u.is_manager or u.is_owner or u.is_superuser)
def return_sold_product(request):
    return_products = ReturnProduct.objects.all()
    form = ReturnProductForm()
    if request.method == 'POST':
        form = ReturnProductForm(request.POST)
        if form.is_valid():
            try:
                with transaction.atomic():
                    sale = form.cleaned_data.get('sale')
                    product = form.cleaned_data.get('product')
                    return_qty = form.cleaned_data.get('qty')

                    if sale.is_complete == False:
                        messages.error(
                            request, f'Oops! You cannot return product in pending state.')
                        return redirect('sales:return_sold_product')

                    if return_qty == 0:
                        messages.error(
                            request, f'Oops! Return qty should be greater than 0.')
                        return redirect('sales:return_sold_product')
                    sale_item = sale.saleitem_set.get(product=product)

                    if sale_item.quantity < return_qty:
                        messages.error(
                            request, f'Oops! You are returning {return_qty} quantity but sold quantity is {sale_item.quantity}')
                        return redirect('sales:return_sold_product')
                    else:
                        instance = form.save(commit=False)
                        instance.added_by = request.user
                        instance.cost_price = sale_item.product.cost_price
                        instance.sold_price = sale_item.sold_price
                        instance.selling_date = sale_item.selling_date
                        instance.save()

                        sale_item.quantity = sale_item.quantity - return_qty
                        sale_item.save()

                        return_product_obj = ReturnProduct.objects.last()
                        return_product_obj.sold_price = sale_item.sold_price
                        return_product_obj.save()

                        # Substracting total return product amount from CreditMethod
                        credit_method = CreditMethod.objects.get(
                            method=sale.payment_method)
                        credit_method.total_amount = credit_method.total_amount - \
                            (return_product_obj.sold_price * return_qty)
                        credit_method.save()

                    if sale_item.quantity == 0:
                        sale_item.delete()

                    sale.return_amount = return_qty * return_product_obj.sold_price
                    sale.save()

                    # Creating CashFlow
                    obj = sale
                    credit_method = obj.payment_method
                    is_debit = True
                    customer = 'walk-in customer' if obj.customer is None else obj.customer.get_full_name
                    amount = obj.return_amount
                    context = {
                        'invoice id': obj.id,
                        'title': 'sale(product return)',
                        'customer': customer,
                    }
                    create_cashflow_for_sale(
                        obj, credit_method, is_debit, context, amount)

                    messages.success(request, 'Success!')
                    return redirect('sales:return_sold_product')
            except Exception as e:
                print(e)
                messages.error(request, e)
                return redirect('sales:return_sold_product')

    context = {
        'title': 'Customer Return Product',
        'return_products': return_products,
        'form': form,
    }
    return render(request, 'sales/return-product.html', context)


# Deleting Retunr Product instead of editing
@login_required()
@user_passes_test(lambda u: u.is_active or u.is_manager or u.is_owner or u.is_superuser)
def delete_return_product(request):
    if request.is_ajax:
        try:
            with transaction.atomic():

                return_product_id = request.POST.get('return_product_id')

                return_product = ReturnProduct.objects.get(
                    id=return_product_id)
                sale = return_product.sale
                product = return_product.product
                qty = return_product.qty
                sold_price = return_product.sold_price
                selling_date = return_product.selling_date

                total_return_product_price = qty * sold_price
                print(total_return_product_price)
                SaleItem.objects.create(
                    product=product,
                    sale=sale,
                    quantity=qty,
                    sold_price=sold_price,
                    selling_date=selling_date
                )

                payment_method = sale.payment_method
                credit_method = CreditMethod.objects.get(method=payment_method)
                credit_method.total_amount = credit_method.total_amount + total_return_product_price
                credit_method.save()

                # Creating CashFlow
                obj = sale
                credit_method = obj.payment_method
                is_debit = False
                customer = 'walk-in customer' if obj.customer is None else obj.customer.get_full_name
                amount = total_return_product_price
                context = {
                    'invoice id': obj.id,
                    'title': 'sale(deleted product is added to the invoice)',
                    'customer': customer,
                }
                create_cashflow_for_sale(
                    obj, credit_method, is_debit, context, amount)

                return_product.delete()
                data = True
        except Exception as e:
            print(e)
            data = False

    return JsonResponse({'data': data})


@login_required()
@user_passes_test(lambda u: u.is_active or u.is_manager or u.is_owner or u.is_superuser)
def damage_product(request):
    damage_products = DamageProduct.objects.all()
    form = DamageProductForm()
    context = {
        'title': 'Damage Products',
        'damage_products': damage_products,
        'form': form,
    }
    return render(request, 'sales/damage-product.html', context)


@login_required()
@user_passes_test(lambda u: u.is_active or u.is_manager or u.is_owner or u.is_superuser)
def add_damage_product(request):
    if request.method == 'POST':
        form = DamageProductForm(request.POST)
        if form.is_valid():
            product = form.cleaned_data.get('product')
            damage_qty = form.cleaned_data.get('damage_qty')

            inventory = Inventory.objects.get(product=product)

            if inventory.get_total_remaining_product < damage_qty:
                messages.error(
                    request, 'This product is out of stock. You cannot add damage product.')
                return redirect('sales:damage_product')

            instance = form.save(commit=False)
            instance.added_by = request.user
            instance.cost_price = product.cost_price
            instance.save()

            damage_product = DamageProduct.objects.last()
            total_damage_amount = damage_product.cost_price * damage_product.damage_qty

            # Substracting total damage amount from CreditMethod
            credit_method = CreditMethod.objects.get(method='cash')
            credit_method.total_amount = credit_method.total_amount - total_damage_amount
            credit_method.save()

            messages.success(request, 'Success!')
            return redirect('sales:damage_product')


@login_required()
@user_passes_test(lambda u: u.is_active or u.is_manager or u.is_owner or u.is_superuser)
# Deleting Damage_product
def delete_damage_product(request):
    # Ajax request
    if request.method == 'POST':
        damage_product_id = request.POST.get('damage_product_id')
        damage_product = DamageProduct.objects.get(id=damage_product_id)

        # Substracting total damage amount from CreditMethod
        credit_method = CreditMethod.objects.get(method='cash')
        credit_method.total_amount = credit_method.total_amount + \
            damage_product.get_total_damage_amount
        credit_method.save()
        damage_product.delete()

        data = True
    else:
        data = False

    return JsonResponse({'data': data})


@login_required()
@user_passes_test(lambda u: u.is_active or u.is_manager or u.is_owner or u.is_superuser)
def sold_report(request):
    sales = Sale.objects.filter(is_complete=True)
    context = {
        'sales': sales,
    }
    return render(request, 'sales/sold-report.html', context)


@login_required()
@user_passes_test(lambda u: u.is_active or u.is_manager or u.is_owner or u.is_superuser)
def order_report(request):
    orders = Sale.objects.filter(is_complete=False)
    context = {
        'orders': orders,
    }
    return render(request, 'sales/order-report.html', context)


@login_required()
@user_passes_test(lambda u: u.is_active or u.is_manager or u.is_owner or u.is_superuser)
def damage_product_report(request):
    damage_products = DamageProduct.objects.all()
    context = {
        'damage_products': damage_products,
    }
    return render(request, 'sales/damage-product-report.html', context)


@login_required()
@user_passes_test(lambda u: u.is_active or u.is_manager or u.is_owner or u.is_superuser)
def return_product_report(request):
    return_products = ReturnProduct.objects.all()
    context = {
        'return_products': return_products,
    }
    return render(request, 'sales/return-product-report.html', context)
