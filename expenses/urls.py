from django.urls import path
from expenses import views

app_name = 'expenses'
urlpatterns = [
    path('expense/', views.expense, name='expense'),
    path('add-expense/', views.add_expense, name='add_expense'),
    path('delete-expense/', views.delete_expense, name='delete_expense'),
    path('expense-report/', views.expense_report, name='expense_report'),
]