from django.db import transaction
from django.shortcuts import redirect, render
from django.contrib import messages

from django.contrib.auth.decorators import login_required, user_passes_test

from django.http import JsonResponse

from expenses.models import Expense
from expenses.forms import ModelExpenseForm
from accounts.models import CreditMethod




# Create your views here.
@login_required()
@user_passes_test(lambda u: u.is_active or u.is_salesman or u.is_manager or u.is_owner or u.is_superuser)
def expense(request):
    expenses = Expense.objects.all()
    form = ModelExpenseForm()
    context = {
        'title': 'Expense',
        'expenses': expenses,
        'form': form,
    }

    return render(request, 'expenses/expense.html', context)




@login_required()
@user_passes_test(lambda u: u.is_active or u.is_salesman or u.is_manager or u.is_owner or u.is_superuser)
def add_expense(request):
    if request.method == 'POST':
        with transaction.atomic():
            form = ModelExpenseForm(request.POST)
            if form.is_valid():
                instance = form.save(commit=False)
                instance.added_by = request.user
                instance.save()

                #Substracting expense amount from CreditMethod
                pay_from = form.cleaned_data.get('pay_from')
                expense_amount = form.cleaned_data.get('amount')
                credit_method = CreditMethod.objects.get(method=pay_from)
                credit_method.total_amount = credit_method.total_amount - expense_amount
                credit_method.save()
                
                messages.success(request, 'Expense has been successfully created !')
                return redirect('expenses:expense')
            else:
                messages.error(request, 'Oops! Something went wrong. Please add again. ')
                return redirect('expenses:expense')


@login_required()
@user_passes_test(lambda u: u.is_active or u.is_manager or u.is_owner or u.is_superuser)
def delete_expense(request):
    if request.is_ajax():
        with transaction.atomic():
            expense_id = request.POST.get('expense_id')
            expense = Expense.objects.get(id=expense_id)

            expense_amount = expense.amount
            pay_from = expense.pay_from

            #Adding expense amount from CreditMethod
            credit_method = CreditMethod.objects.get(method=pay_from)
            credit_method.total_amount = credit_method.total_amount + expense_amount
            credit_method.save()

            expense.delete()
            
            response = {
                'data': True,
            }
    else:
        response = {
            'data': False,
        }
    
    return JsonResponse(response)


def expense_report(request):
    expenses = Expense.objects.all()
    context = {
        'expenses':expenses,
    }

    return render(request, 'expenses/expense-report.html', context)