$(document).ready(function(){
    $('#add-expense').click(function(){
        $('#add-expense-modal').dialog({
            title: 'Do you want to proceed?',
            width: 600,
            height: 570,
            modal: true,
            resizable: false,
            draggable: true,
    
        });
        console.log('I am from expense click event')
    });

    // # Deleting Sale 
    $('.js-delete-expense').click(function(){
        var expense_id = $(this).data('expense_id')
        const csrf = $('input[name=csrfmiddlewaretoken').val(); 
        $('#expense-delete-modal-show').dialog({
            title: 'Do you want to delete?',
            modal: true,
            resizable: false,
            draggable: true,
    
        });
        $('#expense_delete_no').click(function(e){
            e.stopImmediatePropagation()
            $('#expense-delete-modal-show').dialog('close'); 
        })
        $('#expense_delete_yes').click(function(){
            $.ajax({
                type: 'POST',
                url: '/delete-expense/',
                dataType: 'json',
                data: {
                    'csrfmiddlewaretoken': csrf,
                    'expense_id': expense_id,
                },
                success: function(response){
                    if(response.data == true){
                        $('#expense-delete-modal-show').dialog('close'); 
                        Toast.fire({
                            icon: 'success',
                            title: 'Success! The item successfully deleted.'
                        });
                        setTimeout(function(){
                            location.reload()
                        },2000);
                        
                        
                    }else{
                        $('#expense-delete-modal-show').dialog('close'); 
                        Toast.fire({
                            icon: 'error',
                            title: 'error! Somthing went wrong.'
                        });
                    }
                } 
            });

        });
    });// # Deleting Sale ended  
    
})