from datetime import date
from django.db.models.signals import post_save, pre_delete
from django.dispatch import receiver


from accounts.models import CashFlow, SupplierPayment
from accounts.signals import create_cashflow_for_credit_method
from expenses.models import Expense


@receiver(post_save, sender=Expense)
def create_cashflow(sender, instance, created, **kwargs):
    if created:
        #Creating and updating CashFlow Table
        obj = Expense.objects.get(id=instance.id)
        extra_context = {
            'title': 'expense',
            'paid to': instance.pay_to,
            'description': instance.description,
        }
        is_debit = True
        credit_method = instance.pay_from
        # Calling function-1(CreditMethod) for creating 'CashFlow'.
        create_cashflow_for_credit_method(obj, credit_method, is_debit, extra_context)

@receiver(pre_delete, sender=Expense)
def pre_purchase_invoice_delete(sender, instance, **kwargs):
    #Creating and updating CashFlow Table
    obj = Expense.objects.get(id=instance.id)
    extra_context = {
        'title': 'expense',
        'deleted': 'yes',
        'description': instance.description,
    }
    is_debit = False
    credit_method = instance.pay_from
    # Calling function-1(CreditMethod) for creating 'CashFlow'.
    create_cashflow_for_credit_method(obj, credit_method, is_debit, extra_context)
