from products.models import Product
from django.db import models
from django.conf import settings

# Create your models here.
class Expense(models.Model):

    CASH = 'cash'
    MOBILE_BANKING = 'mobile banking'
    BANK  = 'bank'

    METHOD = [
        (CASH, 'Cash'),
        (MOBILE_BANKING, 'Mobile Banking'),
        (BANK, 'Bank'),
    ]

    DAILY_EXPENSE = 'daily expense'
    LIGHT_SNACKS = 'light snacks'
    GUEST_SNACKS = 'guest snacks'
    ASSETS_PURCHASE = 'assets purchase'
    STUFF_SALARY = 'stuff salary'
    SHOP_RENT = 'shop rent'
    OTHER = 'other'

    CHOICES = [
        (DAILY_EXPENSE, 'Daily Expense'),
        (LIGHT_SNACKS, 'Light Snacks'),
        (GUEST_SNACKS, 'Guest Snacks'),
        (ASSETS_PURCHASE, 'Assets Purchase'),
        (STUFF_SALARY, 'Stuff Salary'),
        (SHOP_RENT, 'Shop Rent'),
        (OTHER, 'Other')
   ]

    title = models.CharField(max_length=30)
    pay_to = models.CharField(max_length=30)
    description = models.CharField(max_length=200, blank=True)
    types_of_cost = models.CharField(max_length=20, choices=CHOICES)
    amount = models.IntegerField()
    pay_from = models.CharField(max_length=15, choices=METHOD)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    added_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null= True, related_name='add+')
    edited_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null= True, related_name='edit+', blank=True) 
