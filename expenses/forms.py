from django.forms import ModelForm

from expenses.models import Expense
class ModelExpenseForm(ModelForm):
    class Meta:
        model   = Expense
        fields  = '__all__'
        exclude = ['added_by', 'edited_by']