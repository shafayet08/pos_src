$(document).ready(function() {
 
    var table =$("#datatable").DataTable({

        paging: true,
        autoWidth: false,
        responsive: true,
        PaginationType: "scrolling",
        order: [[ 0, "desc" ]], 
        searching: true,
        dom: 'lBfrtip',
        lengthMenu: [
            [ 10, 25, 50, -1 ],
            [ '10 rows', '25 rows', '50 rows', 'Show all' ]
        ],
        

        buttons: [
            {
                extend: 'copyHtml5',
                text: '<i class="fas fa-copy"></i> Copy',
                titleAttr: 'Copy'
            },
            {
                extend: 'excelHtml5',
                text: '<i class="fas fa-file-excel"></i> Excel',
                titleAttr: 'Excel'
            },
            {
                extend: 'csvHtml5',
                text: '<i class="fas fa-file-csv"></i> CSV',
                titleAttr: 'CSV'
            },
            {
                extend: 'pdfHtml5',
                text: '<i class="fas fa-file-pdf"></i> PDF',
                titleAttr: 'PDF',
                orientation: 'landscape',
                pageSize: 'Letter',
                messageTop: 'PDF generated by XoftNology.',
                // customize : function(doc){
                //     doc.pageMargins = [10, 10, 10, 10];
                // },
            },
            {
                extend: 'print',
                text: '<i class="fas fa-print"></i> Print',
                titleAttr: 'Print',
                messageTop: 'This page has been generated by XoftNology.',
                exportOptions: {
                    columns: ':visible',
                    search: 'applied',
                    order: 'applied',
                } 
                
            },

        ],

    })  
    table.buttons().container().prependTo($('#sales-item_wrapper .col-sm-4:eq(1)'));


    var minDate, maxDate;

    // Custom filtering function which will search data in column for between two values
    $.fn.dataTable.ext.search.push(
        function( settings, data, dataIndex ) {
            var min = minDate.val();
            var max = maxDate.val();
            var date = new Date( data[1] );

            if (
                ( min === null && max === null ) ||
                ( min === null && date <= max ) ||
                ( min <= date   && max === null ) ||
                ( min <= date   && date <= max )
            ) {
                return true;
            }
            return false;
        }
    );
    // Create date inputs
    minDate = new DateTime($('#min'), {
        format: 'MMMM Do YYYY'
    });
    maxDate = new DateTime($('#max'), {
        format: 'MMMM Do YYYY'
    });
 
    // DataTables initialisation
    var table = $('#datatable').DataTable();
 
    // Refilter the table
    $('#min, #max').on('change', function () {
        table.draw();
    });



});
