from customers.models import Customer
from django import forms



# Customer Model Form
class CustomerModelForm(forms.ModelForm):
    class Meta:
        model = Customer
        exclude = ['status', 'added_by', 'date_joined', 'edited_by']