from django.db import models
from django.conf import settings
from django.utils.translation import ugettext_lazy as _


from base_model.base import CommonInfo



class Customer(CommonInfo):
    
    MALE = 'male'
    FEMALE = 'female'

    PREMIUM = 'premium'
    GENERAL = 'general'
    SILVER  = 'silver'
    GOLD    = 'gold'

    STATUS = [
       (PREMIUM, _('Premium Customer')),
       (GENERAL, _('General Customer')),
       (SILVER, _('Silver Customer')),
       (GOLD, _('GOLD Customer')),
   ]

    GENDER = [
        (MALE, 'Male'),
        (FEMALE, 'Female'),
    ]

    gender          = models.CharField(max_length=6, choices=GENDER, default=MALE)
    picture         = models.ImageField(upload_to='customer_pic', default='customer_pic/avatar.png')
    status          = models.CharField(max_length=32, choices=STATUS, default=GENERAL)
    note            = models.CharField(max_length = 100, blank=True)
   
    rn_sph = models.CharField(max_length = 20, blank=True, verbose_name='SPH', null=True)
    rn_cyl = models.CharField(max_length = 20, blank=True, verbose_name='CYL', null=True)
    rn_axis = models.CharField(max_length = 20, blank=True, verbose_name='AXIS', null=True)
    rn_va   = models.CharField(max_length = 20, blank=True, verbose_name='V/A', null=True)

    ln_sph = models.CharField(max_length = 20, blank=True, verbose_name='SPH', null=True)
    ln_cyl = models.CharField(max_length = 20, blank=True, verbose_name='CYL', null=True)
    ln_axis = models.CharField(max_length = 20, blank=True, verbose_name='AXIS', null=True)
    ln_va   = models.CharField(max_length = 20, blank=True, verbose_name='V/A', null=True)
    

    rd_sph = models.CharField(max_length = 20, blank=True, verbose_name='SPH', null=True)
    rd_cyl = models.CharField(max_length = 20, blank=True, verbose_name='CYL', null=True)
    rd_axis = models.CharField(max_length = 20, blank=True, verbose_name='AXIS', null=True)
    rd_va   = models.CharField(max_length = 20, blank=True, verbose_name='V/A', null=True)
    

    ld_sph = models.CharField(max_length = 20, blank=True, verbose_name='SPH', null=True)
    ld_cyl = models.CharField(max_length = 20, blank=True, verbose_name='CYL', null=True)
    ld_axis = models.CharField(max_length = 20, blank=True, verbose_name='AXIS', null=True)
    ld_va   = models.CharField(max_length = 20, blank=True, verbose_name='V/A', null=True)

    adi_right = models.IntegerField(blank=True, null=True, verbose_name='')
    adi_left = models.IntegerField(blank=True, null=True, verbose_name='')


    added_by        = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True, related_name='add+',)
    edited_by       = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null= True, related_name='edit+', blank=True) 
    

    
    
    def __str__(self):
        return self.first_name
    
    @property
    def get_full_name(self):
        """
        Return the first_name plus the last_name, with a space in between.
        """
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()