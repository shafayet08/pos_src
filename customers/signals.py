from django.db.models.signals import pre_save
from django.dispatch import receiver
from django.core.files.base import ContentFile

from io import BytesIO
from PIL import Image


from customers.models import Customer

@receiver(pre_save, sender=Customer)
def generate_customer_thumbnail(sender, instance, **kwargs):
    try:
        picture = Image.open(instance.picture)
        picture = picture.convert("RGB")

        if picture.height > 300 or picture.width > 300:
            _extracted_from_generate_customer_thumbnail(picture, instance)
    except:
        pass

def _extracted_from_generate_customer_thumbnail(picture, instance):
    THUMBNAIL_SIZE = (300, 300)
    picture.thumbnail(THUMBNAIL_SIZE)
    temp_thumb = BytesIO()
    picture.save(temp_thumb, 'JPEG')
    temp_thumb.seek(0)

    instance.picture.save(
        instance.picture.name,
        ContentFile(temp_thumb.read()),
        save=False,
    )
    temp_thumb.close()