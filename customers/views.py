from django.http import JsonResponse
from django.shortcuts import redirect, render
from django.contrib import messages

from django.contrib.auth.decorators import login_required, user_passes_test

from customers.models import Customer
from customers.forms import CustomerModelForm




# Customer search. It works on Cart Section. It has Jquery file
def customer_search(request):
    if 'term' in request.GET:
        term = request.GET.get('term')
        qs = (
            Customer.objects.filter(first_name__icontains=term) | 
            Customer.objects.filter(last_name__icontains=term) |
            Customer.objects.filter(phone1__icontains=term) |
            Customer.objects.filter(phone2__icontains=term) |
            Customer.objects.filter(email__icontains=term)     
        )
        customers = []
        for customer in qs:
            if customer.phone1:
                customers.append(customer.phone1)
            else:
                if customer.phone2:
                    customers.append(customer.phone2)
    
    return JsonResponse(customers, safe=False)



@login_required()
@user_passes_test(lambda u: u.is_active or u.is_salesman or u.is_manager or u.is_owner or u.is_superuser)
def customer(request):
    customers = Customer.objects.all()
    customerForm = CustomerModelForm()
    context = {
        'customers': customers,
        'customerForm': customerForm,
        'title': 'View Customers'
    }
    return render(request, 'customers/customers.html', context)


@login_required()
@user_passes_test(lambda u: u.is_active or u.is_salesman or u.is_manager or u.is_owner or u.is_superuser)
def customer_profile(request, pk):
    customer = Customer.objects.get(pk=pk)
    orders = customer.sale_set.all()
    context ={
        'customer': customer,
        'orders': orders,
        'title': 'Customer Profile'
    }
    return render(request, 'customers/customer-profile.html', context) 



@login_required()
@user_passes_test(lambda u: u.is_active or u.is_salesman or u.is_manager or u.is_owner or u.is_superuser)
def create_customer(request):
    if request.method == 'POST':
        form = CustomerModelForm(request.POST, request.FILES)
        if form.is_valid():
            instance = form.save(commit=False)
            instance.added_by = request.user
            instance.save()
            messages.success(request, 'Customer has been successfully created ')        
        else:
            messages.error(request, 'Oops! Something went wrong. Please create again. ')
            return redirect(request.META['HTTP_REFERER'])

    return redirect(request.META['HTTP_REFERER'])


@login_required()
@user_passes_test(lambda u: u.is_active or u.is_salesman or u.is_manager or u.is_owner or u.is_superuser)
def update_customer(request, pk):
    customer = Customer.objects.get(pk=pk)
    form = CustomerModelForm(instance=customer)
    if request.method == 'POST':
        form = CustomerModelForm(request.POST, request.FILES, instance=customer)
        if form.is_valid():
            form.save()
            messages.success(request, 'Customer has been successfully updated ')
            return redirect('customers:customer')
        else:
            messages.error(request, 'Oops! Something went wrong')
            return redirect(request.META['HTTP_REFERER'])
    
    context = {
        'title': 'Update Customer',
        'form': form,
    }

    return render(request, 'customers/customer-update-form.html', context)


# This view is not needed for this pos but, 
# what has done for this view has remained 
# Just commented out
# def delete_customer(request):
#     if request.is_ajax():
#         customer_id = request.POST.get('customer_id')
#         customer = Customer.objects.get(id=customer_id)
#         customer.delete()
        
#         response = {
#             'data': True,
#         }
#     else:
#         response = {
#             'data': False,
#         }
    
#     return JsonResponse(response)

        
    