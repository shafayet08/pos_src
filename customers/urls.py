from django.urls import path

from . import views

app_name = 'customers'
urlpatterns = [
    path('customer-search/', views.customer_search, name='customer_search'),

    path('customer/', views.customer, name='customer'),
    path('create-customer/', views.create_customer, name='create_customer'),
    path('update-customer/<int:pk>/', views.update_customer, name='update_customer'),
    #This is not needed for this project just commented out for future
    # path('delete-customer/', views.delete_customer, name='delete_customer'),
    
    path('customer-profile/<int:pk>/', views.customer_profile, name='customer_profile'),

    
]
