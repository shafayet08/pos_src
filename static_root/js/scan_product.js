$(document).ready(function(){


    function get_discount(value, parcentage){
        return (value * parcentage) / 100;
    };

    function cart_handle(){
        // For changing account section.
        var discount;
        var sub_total_ = parseInt($('#js-sub-total').text());
        var discount_value = parseInt($('#id_discount').val()) || 0;
        var take_amount = parseInt($('#id_paid_or_take').val()) || 0;
        var other_fee = parseInt($('#id_other_fee').val()) || 0;
        var total_payable = parseInt($('#id_total_payable').text());
        var check_discount = $('#id_dis_checkbox').is(":checked");
        
        // checking if the discount is in parcentage or not
        if(check_discount == true){
            discount = get_discount(sub_total_, discount_value);
        }else{
            discount = discount_value;
        };

        // Comparing sub total and total_payable amount
        if(sub_total_ == total_payable){
            total_payable = (sub_total_ - discount) + other_fee;
        }else{
            if(sub_total_ !== total_payable){
                total_payable = (sub_total_ - discount) + other_fee;
            };
        };
        var return_amount = take_amount - total_payable;
        
        // Comparing total_payable and take amount and it returns 'return amount and due amount
        if(total_payable > take_amount){
            var due_amount = total_payable - take_amount;
            return_amount = 0;
        }else{
            due_amount = 0;
        }
        
        $('#id_total_payable').text(total_payable);
        $('#id_return_amount').text(return_amount);
        $('#id_due_amount').text(due_amount);
    };
    // 3. A callable function that handles down account section -> ended
    
    // 4. A callable function that handels 'return amount and due amount' -> starts
    function return_and_due_amount(){
        var take_amount = parseInt($('#id_paid_or_take').val()) || 0;
        var total_payable = parseInt($('#id_total_payable').text());
        var return_amount = take_amount - total_payable;
        if(total_payable > take_amount){
            var due_amount = total_payable - take_amount;
            return_amount = 0;
        }else{
            due_amount = 0;
        }
        $('#id_return_amount').text(return_amount);
        $('#id_due_amount').text(due_amount);
    };
    // 4. A callable function that handels 'return amount and due amount' -> ended




    // Main work starts from here.
    var product_prices = [];
    var product_codes = [];
    const keycodes = [0,1,2,3,4,5,6,7,8,9];
    const csrf = $('input[name=csrfmiddlewaretoken').val();


















    // Scanner start


    $('#psearch-input').keypress(function(e){
        if(e.which == 13){
            product_code = e.target.value;
            var product_qty = 0;
            for(var i in product_codes){
                if(product_code == product_codes[i]){
                    product_qty = product_qty + 1;
                }
            };
            product_qty = product_qty + 1

            $.ajax({
                type: 'POST',
                url: '/find_product/',
                data: {
                    'csrfmiddlewaretoken': csrf,
                    'product_code': product_code,
                    'product_qty': product_qty,
                },
                success: function(response){
                    var product_code = response.product_code;
                    var product_price_after_discount = response.product_price_after_discount;
                    var product = response.product_name;
                    
                    if(response.product_in_stock == false){
                        Toast.fire({
                            icon: 'error',
                            title: 'Product is out of stock.'
                        })
                        $('#psearch-input').val('');
                        $('#psearch-input').focus();
                    }else{
                        //appending product prices with new ones;
                        product_prices.push(product_price_after_discount);
                        //appending products with new ones;
                        product_codes.push(product_code);

                        var qty = 0;
                        var total_price = 0;
                        var total_qty = 0;
                        var sub_total = 0;

                        for(var i in product_prices){
                            if(product_price_after_discount == product_prices[i] && product_code == product_codes[i]){
                                total_price = total_price + product_price_after_discount;
                            }
                            total_qty = total_qty + 1;
                        };

                        for(var i=0; i < product_codes.length; i++){
                            if(product_code == product_codes[i] && product_price_after_discount == product_prices[i]){
                                qty++;
                            }
                        };
                        // Looping through all the product prices for sub total
                        for(var i in product_prices){
                            sub_total = sub_total + product_prices[i]
                        };
                        if(qty == 1){
                            var cart_item = `<tr>
                                                    <td class="js-product">${product}</td>
                                                    <td id="js-qty${product_code}" 
                                                        class="js-qty-editable" 
                                                        data-product_code="${product_code}"
                                                        contenteditable="true">
                                                        ${qty}
                                                    </td>
                                                    <td id="js-price${product_code}" 
                                                        class="js-price-editable"
                                                        contenteditable="true"
                                                        data-product_code="${product_code}">
                                                        ${product_price_after_discount}
                                                    </td>
                                                    <td id="js-total-price${product_code}">${total_price}</td>
                                                    <td class="js-remove icon-center-pos" data-product_code="${product_code}">
                                                        <i class="fas fa-times">               
                                                    </td>
                                                </tr> 
                                            `;
                            $('#js-cart-table #show-cart').append(cart_item);
                        };
                        console.log(qty)
                        if(qty > 1){
                            $('#show-cart #js-qty' + product_code.toString()).text(qty);
                            $('#show-cart #js-total-price' + product_code.toString()).text(total_price);
                        };
                        $('#js-total-qty').text(total_qty);
                        $('#js-sub-total').text(sub_total);
                        $('#id_total_payable').text(sub_total);

                        //For changing account section. This cart_handle() is called from the above (fn no.-3)
                        cart_handle();
                        $('#psearch-input').val('');
                        $('#psearch-input').focus();

                                            //-----------Removing item from cart starts------------
                        // Inner event of 1st event starts;
                        $('#show-cart .js-remove').click(function(){
                            var product_code = $(this).data('product_code');
                            var remove_price = parseInt($(this).siblings('#js-price' + product_code).text()); 
                            // Removing products and prices
                            lim1 = product_codes.length
                            for(var i=0; i < lim1; i++){
                                for(var j=0; j < lim1; j++){
                                    if(product_code == product_codes[i] && remove_price == product_prices[i]){
                                        product_codes.splice(i, 1);
                                        product_prices.splice(i, 1);
                                    };  
                                };
                            };
                            console.log(product_prices)
                            console.log(product_codes)
                            total_qty = product_codes.length;
                            sub_total = 0;
                            $.each(product_prices, function(index, value){
                                sub_total += value;
                            });

                            $('#js-total-qty').text(total_qty);
                            $('#js-sub-total').text(sub_total);
                            $('#id_total_payable').text(sub_total);
                            cart_handle(); //For changing account section. This cart_handle() function is called from the top (fn no.-3)
                            $(this).parent().remove();
                        }); // Inner event of 1st event ends;
                        //-----------Removing item from cart ends------------
                    } // if condition ended

                    // Inner 2nd event starts
                    //-----------Editable quantity in the cart starts------------
                    $('#show-cart .js-qty-editable').click(function(){
                        $(this).css({'outline': '2px solid red', 'background': '#fbeec6'});
                        var after_changing_qty;
                        $(this).keypress(function(e){
                            if(!(e.key in keycodes)){
                                e.preventDefault(); // Preventing characters without numbers
                            };
                            e.stopImmediatePropagation() // Preventing continuous loop
                            if(e.which == 13){
                                e.preventDefault();
                                after_changing_qty = $(this).text();
                                product_code = $(this).data('product_code')
                                $(this).css({'outline': '', 'background': ''});
                                $(this).blur();
                                
                                if(after_changing_qty == ''){
                                    // alert_('error', '<strong>Quantity cannot be empty.Please provide quantity.</strong>');
                                    $('.alert').show() // undone- add alert message
                                    $(this).css({'outline': '2px solid red', 'background': '#fbeec6'});
                                    $(this).focus();
                                };
                                if(after_changing_qty == 0){
                                    // alert_('error', '<strong>Quantity cannot be 0.Please provide quantity.</strong>');
                                    $('.alert').show() // undone- add alert message
                                    $(this).css({'outline': '2px solid red', 'background': '#fbeec6'});
                                    $(this).focus();
                                };
                                after_changing_qty = parseInt(after_changing_qty);
                                if(after_changing_qty != '' && after_changing_qty != 0){
                                    product_qty = after_changing_qty;
                                    price = parseInt($(this).siblings('#js-price' + product_code).text());

                                    $.ajax({
                                        type: 'POST',
                                        url: '/find_product/',
                                        data: {
                                            'csrfmiddlewaretoken': csrf,
                                            'product_code': product_code,
                                            'product_qty': product_qty,
                                        },
                                        success: function(response){
                                            if(response.product_in_stock == false){
                                                Toast.fire({
                                                    icon: 'error',
                                                    title: 'Product is out of stock.'
                                                })

                                                qty = response.product_available;
                                                
                                                $('#js-qty' + response.product_code.toString()).text(qty);
                                                $('#js-qty' + response.product_code.toString()).focus();
                                            }else{
                                                $('.alert').hide();
                                                $(this).blur();
                                                remove_product = response.product_code;
                                                console.log(remove_product)
                                                price = price;
                                
                                                var lim1 = parseInt(product_codes.length)
                                                for(var i=0; i < lim1; i++){
                                                    for(var j=0; j < lim1; j++){
                                                        if(remove_product == product_codes[i] && price == product_prices[i]){
                                                            product_codes.splice(i, 1);
                                                            product_prices.splice(i, 1);
                                                        };  
                                                    };
                                                };

                                            
                                                total_price = 0;
                                                // console.log(typeof(product_prices[1]))
                                                for(var i=0; i < after_changing_qty; i++){
                                                    product_prices.push(price);
                                                    product_codes.push(remove_product);
                                                    total_price = total_price + price;
                                                };
                                                console.log(product_codes)
                                                console.log(product_prices)
                                                total_qty = product_prices.length;
                                                sub_total = 0;
                                                $.each(product_prices, function(index, value){
                                                    sub_total += value;
                                                });
                                                $('#js-total-price' + remove_product).text(total_price);
                                                $('#js-total-qty').text(total_qty);
                                                $('#js-sub-total').text(sub_total);
                                                $('#id_total_payable').text(sub_total);
                                                cart_handle(); //For changing account section. This cart_handle() function is called from the top (fn no.-3)
                                                // console.log(e.isImmediatePropagationStopped());
                                            } // else ended
                                            
                                        } // 2nd success ended
                                    });
                                } // else condition ended
                            };   
                        }); // keypress ended


                        $(this).blur(function(e){
                            e.stopImmediatePropagation()
                            after_changing_qty = $(this).text();
                            product_code = $(this).data('product_code')
                            $(this).css({'outline': '', 'background': ''});
                            
                            if(after_changing_qty == ''){
                                // alert_('error', '<strong>Quantity cannot be empty.Please provide quantity.</strong>');
                                $('.alert').show() // undone- add alert message
                                $(this).css({'outline': '2px solid red', 'background': '#fbeec6'});
                                $(this).focus();
                            };
                            if(after_changing_qty == 0){
                                // alert_('error', '<strong>Quantity cannot be 0.Please provide quantity.</strong>');
                                $('.alert').show() // undone- add alert message
                                $(this).css({'outline': '2px solid red', 'background': '#fbeec6'});
                                $(this).focus();
                            };
                            after_changing_qty = parseInt(after_changing_qty);
                            if(after_changing_qty != '' && after_changing_qty != 0){
                                product_qty = after_changing_qty;
                                price = parseInt($(this).siblings('#js-price' + product_code).text());
                                

                                $.ajax({
                                    type: 'POST',
                                    url: '/find_product/',
                                    data: {
                                        'csrfmiddlewaretoken': csrf,
                                        'product_code': product_code,
                                        'product_qty': product_qty,
                                    },
                                    success: function(response){
                                        if(response.product_in_stock == false){
                                            Toast.fire({
                                                icon: 'error',
                                                title: 'Product is out of stock.'
                                            })

                                            qty = response.product_available;

                                            $('#js-qty' + response.product_code.toString()).text(qty);
                                            $('#js-qty' + response.product_code.toString()).focus();
                                        }else{
                                            // $('.alert').hide();
                                            $(this).blur();
                                            remove_product = response.product_code;
                                            price = price;
                            
                                            var lim1 = product_codes.length
                                            for(var i=0; i < lim1; i++){
                                                for(var j=0; j < lim1; j++){
                                                    if(remove_product == product_codes[i] && price == product_prices[i]){
                                                        product_codes.splice(i, 1);
                                                        product_prices.splice(i, 1);
                                                    };  
                                                };
                                            };

                                            total_price = 0;
                                            // console.log(typeof(product_prices[1]))
                                            for(var i=0; i < after_changing_qty; i++){
                                                product_prices.push(price);
                                                product_codes.push(remove_product);
                                                total_price = total_price + price;
                                            };

                                            total_qty = product_prices.length;
                                            sub_total = 0;
                                            $.each(product_prices, function(index, value){
                                                sub_total += value;
                                            });
                                            $('#js-total-price' + remove_product).text(total_price);
                                            $('#js-total-qty').text(total_qty);
                                            $('#js-sub-total').text(sub_total);
                                            $('#id_total_payable').text(sub_total);
                                            cart_handle(); //For changing account section. This cart_handle() function is called from the top (fn no.-3)
                                            // console.log(e.isImmediatePropagationStopped());
                                        } // else ended
                                        
                                    } // 2nd success ended
                                });
                            } // else condition ended

                            
                        }) // blur ended
            
                    });
                    //-----------Editable quantity in the cart ends------------


                    //-----------Editable price in the cart starts------------
                    $('#show-cart .js-price-editable').click(function(){
                        $(this).css({'outline': '2px solid red', 'background': '#fbeec6'});
                        $(this).keypress(function(e){
                            if(!(e.key in keycodes)){
                                e.preventDefault(); // Preventing characters without numbers
                            };
                            e.stopImmediatePropagation() // Preventing continuous loop
                            if(e.which == 13){
                                e.preventDefault();
                                var after_changing_price = parseInt($(this).text()) || 0; // NaN value will replace with 0;
                                product_code = $(this).data('product_code')
                                var current_qty = parseInt($(this).prev().text());

                                $(this).css({'outline': '', 'background': ''});
                                $(this).blur();
                                
                                if(after_changing_price == ''){
                                    $('.alert').show() // undone- add alert message
                                    $(this).css({'outline': '2px solid red', 'background': '#fbeec6'});
                                    $(this).focus();
                                };
                                if(after_changing_price == 0){
                                    $('.alert').show() // undone- add alert message
                                    $(this).css({'outline': '2px solid red', 'background': '#fbeec6'});
                                    $(this).focus();
                                };

                                if(after_changing_price != '' && after_changing_price != 0){
                                    product_qty = current_qty;
                                    console.log('product qty: ',product_qty)
                                    $('.alert').hide();
                                    $(this).blur();
                                    $.ajax({
                                        type: 'POST',
                                        url: '/find_product/',
                                        data: {
                                            'csrfmiddlewaretoken': csrf,
                                            'product_code': product_code,
                                            'product_qty': product_qty,
                                        },
                                        success: function(response){
                                            remove_product = response.product_code;
                                            price = response.product_price_after_discount
                            
                                            var lim1 = product_codes.length
                                            for(var i=0; i < lim1; i++){
                                                for(var j=0; j < lim1; j++){
                                                    if(remove_product == product_codes[i]){
                                                        product_codes.splice(i, 1);
                                                        product_prices.splice(i, 1);
                                                    };  
                                                };
                                            };

                                            total_price = 0;
                                            // console.log(typeof(product_prices[1]))
                                            for(var i=0; i < current_qty; i++){
                                                product_prices.push(after_changing_price);
                                                product_codes.push(remove_product);
                                                total_price = total_price + after_changing_price;
                                            };

                                            total_qty = product_prices.length;
                                            sub_total = 0;
                                            $.each(product_prices, function(index, value){
                                                sub_total += value;
                                            });
                                            $('#js-total-price' + remove_product).text(total_price);
                                            $('#js-sub-total').text(sub_total);
                                            $('#id_total_payable').text(sub_total);
                                            cart_handle(); //For changing account section. This cart_handle() function is called from the top (fn no.-3)
                                            // console.log(e.isImmediatePropagationStopped());
                                            
                                        } // 2nd success ended
                                    });
                                } // else condition ended
                            };   
                        }); // keypress ended


                        $(this).blur(function(e){
                            e.stopImmediatePropagation()
                            var after_changing_price = parseInt($(this).text()) || 0; // NaN value will replace with 0;
                            product_code = $(this).data('product_code')
                            var current_qty = parseInt($(this).prev().text());

                            $(this).css({'outline': '', 'background': ''});
                            console.log(after_changing_price)
                            if(after_changing_price == ''){
                                $('.alert').show() // undone- add alert message
                                $(this).css({'outline': '2px solid red', 'background': '#fbeec6'});
                                $(this).focus();
                            };
                            if(after_changing_price == 0){
                                $('.alert').show() // undone- add alert message
                                $(this).css({'outline': '2px solid red', 'background': '#fbeec6'});
                                $(this).focus();
                            };
                            if(after_changing_price != '' && after_changing_price != 0){
                                product_qty = current_qty;
                                $('.alert').hide();
                                $.ajax({
                                    type: 'POST',
                                    url: '/find_product/',
                                    data: {
                                        'csrfmiddlewaretoken': csrf,
                                        'product_code': product_code,
                                        'product_qty': product_qty,
                                    },
                                    success: function(response){
                                        $(this).blur();
                                        remove_product = response.product_code;
                                        price = response.product_price_after_discount
                        
                                        var lim1 = product_codes.length
                                        for(var i=0; i < lim1; i++){
                                            for(var j=0; j < lim1; j++){
                                                if(remove_product == product_codes[i]){
                                                    product_codes.splice(i, 1);
                                                    product_prices.splice(i, 1);
                                                };  
                                            };
                                        };

                                        total_price = 0;
                                        // console.log(typeof(product_prices[1]))
                                        for(var i=0; i < current_qty; i++){
                                            product_prices.push(after_changing_price);
                                            product_codes.push(remove_product);
                                            total_price = total_price + after_changing_price;
                                        };

                                        total_qty = product_prices.length;
                                        sub_total = 0;
                                        $.each(product_prices, function(index, value){
                                            sub_total += value;
                                        });
                                        $('#js-total-price' + remove_product).text(total_price);
                                        $('#js-sub-total').text(sub_total);
                                        $('#id_total_payable').text(sub_total);
                                        cart_handle(); //For changing account section. This cart_handle() function is called from the top (fn no.-3)
                                        // console.log(e.isImmediatePropagationStopped());
                                        
                                    } // 2nd success ended
                                });
                            } // else condition ended

                        }) // blur ended


                    });
                    //-----------Editable price in the cart ended------------


                }, // 1st success ended
            }); // ajax ended
        }

        
    }); // keypress ended


    // Scanner ended





























    $('.product-view-area  .js-add-to-cart').click(function(){
        
        var product_code = $(this).data('product_code');
        var product_qty = 0;
        for(var i in product_codes){
            if(product_code == product_codes[i]){
                product_qty = product_qty + 1;
            }
        };
        product_qty = product_qty + 1

        $.ajax({
            type: 'POST',
            url: '/find_product/',
            data: {
                'csrfmiddlewaretoken': csrf,
                'product_code': product_code,
                'product_qty': product_qty,
            },
            success: function(response){
                var product_code = response.product_code;
                var product_price_after_discount = response.product_price_after_discount;
                var product = response.product_name;
                console.log(product)
                if(response.product_in_stock == false){
                    Toast.fire({
                        icon: 'error',
                        title: 'Product is out of stock.'
                    })
                }else{
                    //appending product prices with new ones;
                    product_prices.push(product_price_after_discount);
                    //appending products with new ones;
                    product_codes.push(product_code);

                    var qty = 0;
                    var total_price = 0;
                    var total_qty = 0;
                    var sub_total = 0;

                    for(var i in product_prices){
                        if(product_price_after_discount == product_prices[i] && product_code == product_codes[i]){
                            total_price = total_price + product_price_after_discount;
                        }
                        total_qty = total_qty + 1;
                    };

                    for(var i=0; i < product_codes.length; i++){
                        if(product_code == product_codes[i] && product_price_after_discount == product_prices[i]){
                            qty++;
                        }
                    };
                    // Looping through all the product prices for sub total
                    for(var i in product_prices){
                        sub_total = sub_total + product_prices[i]
                    };
                    if(qty == 1){
                        var cart_item = `<tr>
                                                <td class="js-product">${product}</td>
                                                <td id="js-qty${product_code}" 
                                                    class="js-qty-editable" 
                                                    data-product_code="${product_code}"
                                                    contenteditable="true">
                                                    ${qty}
                                                </td>
                                                <td id="js-price${product_code}" 
                                                    class="js-price-editable"
                                                    contenteditable="true"
                                                    data-product_code="${product_code}">
                                                    ${product_price_after_discount}
                                                </td>
                                                <td id="js-total-price${product_code}">${total_price}</td>
                                                <td class="js-remove icon-center-pos" data-product_code="${product_code}">
                                                    <i class="fas fa-times">               
                                                </td>
                                            </tr> 
                                        `;
                        $('#js-cart-table #show-cart').append(cart_item);
                    };
                    console.log(qty)
                    if(qty > 1){
                        $('#show-cart #js-qty' + product_code.toString()).text(qty);
                        $('#show-cart #js-total-price' + product_code.toString()).text(total_price);
                    };
                    $('#js-total-qty').text(total_qty);
                    $('#js-sub-total').text(sub_total);
                    $('#id_total_payable').text(sub_total);

                    //For changing account section. This cart_handle() is called from the above (fn no.-3)
                    cart_handle();

                                        //-----------Removing item from cart starts------------
                    // Inner event of 1st event starts;
                    $('#show-cart .js-remove').click(function(){
                        var product_code = $(this).data('product_code');
                        var remove_price = parseInt($(this).siblings('#js-price' + product_code).text()); 
                        // Removing products and prices
                        lim1 = product_codes.length
                        for(var i=0; i < lim1; i++){
                            for(var j=0; j < lim1; j++){
                                if(product_code == product_codes[i] && remove_price == product_prices[i]){
                                    product_codes.splice(i, 1);
                                    product_prices.splice(i, 1);
                                };  
                            };
                        };
                        console.log(product_prices)
                        console.log(product_codes)
                        total_qty = product_codes.length;
                        sub_total = 0;
                        $.each(product_prices, function(index, value){
                            sub_total += value;
                        });

                        $('#js-total-qty').text(total_qty);
                        $('#js-sub-total').text(sub_total);
                        $('#id_total_payable').text(sub_total);
                        cart_handle(); //For changing account section. This cart_handle() function is called from the top (fn no.-3)
                        $(this).parent().remove();
                    }); // Inner event of 1st event ends;
                    //-----------Removing item from cart ends------------
                     
                } // if condition ended

                // Inner 2nd event starts
                //-----------Editable quantity in the cart starts------------
                $('#show-cart .js-qty-editable').click(function(){
                    $(this).css({'outline': '2px solid red', 'background': '#fbeec6'});
                    var after_changing_qty;
                    $(this).keypress(function(e){
                        if(!(e.key in keycodes)){
                            e.preventDefault(); // Preventing characters without numbers
                        };
                        e.stopImmediatePropagation() // Preventing continuous loop
                        if(e.which == 13){
                            e.preventDefault();
                            after_changing_qty = $(this).text();
                            product_code = $(this).data('product_code')
                            $(this).css({'outline': '', 'background': ''});
                            $(this).blur();
                            
                            if(after_changing_qty == ''){
                                // alert_('error', '<strong>Quantity cannot be empty.Please provide quantity.</strong>');
                                $('.alert').show() // undone- add alert message
                                $(this).css({'outline': '2px solid red', 'background': '#fbeec6'});
                                $(this).focus();
                            };
                            if(after_changing_qty == 0){
                                // alert_('error', '<strong>Quantity cannot be 0.Please provide quantity.</strong>');
                                $('.alert').show() // undone- add alert message
                                $(this).css({'outline': '2px solid red', 'background': '#fbeec6'});
                                $(this).focus();
                            };
                            after_changing_qty = parseInt(after_changing_qty);
                            if(after_changing_qty != '' && after_changing_qty != 0){
                                product_qty = after_changing_qty;
                                price = parseInt($(this).siblings('#js-price' + product_code).text());

                                $.ajax({
                                    type: 'POST',
                                    url: '/find_product/',
                                    data: {
                                        'csrfmiddlewaretoken': csrf,
                                        'product_code': product_code,
                                        'product_qty': product_qty,
                                    },
                                    success: function(response){
                                        if(response.product_in_stock == false){
                                            Toast.fire({
                                                icon: 'error',
                                                title: 'Product is out of stock.'
                                            })

                                            qty = response.product_available;
                                            
                                            $('#js-qty' + response.product_code.toString()).text(qty);
                                            $('#js-qty' + response.product_code.toString()).focus();
                                        }else{
                                            $('.alert').hide();
                                            $(this).blur();
                                            remove_product = response.product_code;
                                            console.log(remove_product)
                                            price = price;
                            
                                            var lim1 = parseInt(product_codes.length)
                                            for(var i=0; i < lim1; i++){
                                                for(var j=0; j < lim1; j++){
                                                    if(remove_product == product_codes[i] && price == product_prices[i]){
                                                        product_codes.splice(i, 1);
                                                        product_prices.splice(i, 1);
                                                    };  
                                                };
                                            };

                                           
                                            total_price = 0;
                                            // console.log(typeof(product_prices[1]))
                                            for(var i=0; i < after_changing_qty; i++){
                                                product_prices.push(price);
                                                product_codes.push(remove_product);
                                                total_price = total_price + price;
                                            };
                                            console.log(product_codes)
                                            console.log(product_prices)
                                            total_qty = product_prices.length;
                                            sub_total = 0;
                                            $.each(product_prices, function(index, value){
                                                sub_total += value;
                                            });
                                            $('#js-total-price' + remove_product).text(total_price);
                                            $('#js-total-qty').text(total_qty);
                                            $('#js-sub-total').text(sub_total);
                                            $('#id_total_payable').text(sub_total);
                                            cart_handle(); //For changing account section. This cart_handle() function is called from the top (fn no.-3)
                                            // console.log(e.isImmediatePropagationStopped());
                                        } // else ended
                                        
                                    } // 2nd success ended
                                });
                            } // else condition ended
                        };   
                    }); // keypress ended


                    $(this).blur(function(e){
                        e.stopImmediatePropagation()
                        after_changing_qty = $(this).text();
                        product_code = $(this).data('product_code')
                        $(this).css({'outline': '', 'background': ''});
                        
                        if(after_changing_qty == ''){
                            // alert_('error', '<strong>Quantity cannot be empty.Please provide quantity.</strong>');
                            $('.alert').show() // undone- add alert message
                            $(this).css({'outline': '2px solid red', 'background': '#fbeec6'});
                            $(this).focus();
                        };
                        if(after_changing_qty == 0){
                            // alert_('error', '<strong>Quantity cannot be 0.Please provide quantity.</strong>');
                            $('.alert').show() // undone- add alert message
                            $(this).css({'outline': '2px solid red', 'background': '#fbeec6'});
                            $(this).focus();
                        };
                        after_changing_qty = parseInt(after_changing_qty);
                        if(after_changing_qty != '' && after_changing_qty != 0){
                            product_qty = after_changing_qty;
                            price = parseInt($(this).siblings('#js-price' + product_code).text());
                            

                            $.ajax({
                                type: 'POST',
                                url: '/find_product/',
                                data: {
                                    'csrfmiddlewaretoken': csrf,
                                    'product_code': product_code,
                                    'product_qty': product_qty,
                                },
                                success: function(response){
                                    if(response.product_in_stock == false){
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'Product is out of stock.'
                                        })

                                        qty = response.product_available;

                                        $('#js-qty' + response.product_code.toString()).text(qty);
                                        $('#js-qty' + response.product_code.toString()).focus();
                                    }else{
                                        // $('.alert').hide();
                                        $(this).blur();
                                        remove_product = response.product_code;
                                        price = price;
                        
                                        var lim1 = product_codes.length
                                        for(var i=0; i < lim1; i++){
                                            for(var j=0; j < lim1; j++){
                                                if(remove_product == product_codes[i] && price == product_prices[i]){
                                                    product_codes.splice(i, 1);
                                                    product_prices.splice(i, 1);
                                                };  
                                            };
                                        };

                                        total_price = 0;
                                        // console.log(typeof(product_prices[1]))
                                        for(var i=0; i < after_changing_qty; i++){
                                            product_prices.push(price);
                                            product_codes.push(remove_product);
                                            total_price = total_price + price;
                                        };

                                        total_qty = product_prices.length;
                                        sub_total = 0;
                                        $.each(product_prices, function(index, value){
                                            sub_total += value;
                                        });
                                        $('#js-total-price' + remove_product).text(total_price);
                                        $('#js-total-qty').text(total_qty);
                                        $('#js-sub-total').text(sub_total);
                                        $('#id_total_payable').text(sub_total);
                                        cart_handle(); //For changing account section. This cart_handle() function is called from the top (fn no.-3)
                                        // console.log(e.isImmediatePropagationStopped());
                                    } // else ended
                                    
                                } // 2nd success ended
                            });
                        } // else condition ended

                        
                    }) // blur ended
        
                });
                //-----------Editable quantity in the cart ends------------


                //-----------Editable price in the cart starts------------
                $('#show-cart .js-price-editable').click(function(){
                    $(this).css({'outline': '2px solid red', 'background': '#fbeec6'});
                    $(this).keypress(function(e){
                        if(!(e.key in keycodes)){
                            e.preventDefault(); // Preventing characters without numbers
                        };
                        e.stopImmediatePropagation() // Preventing continuous loop
                        if(e.which == 13){
                            e.preventDefault();
                            var after_changing_price = parseInt($(this).text()) || 0; // NaN value will replace with 0;
                            product_code = $(this).data('product_code')
                            var current_qty = parseInt($(this).prev().text());

                            $(this).css({'outline': '', 'background': ''});
                            $(this).blur();
                            
                            if(after_changing_price == ''){
                                $('.alert').show() // undone- add alert message
                                $(this).css({'outline': '2px solid red', 'background': '#fbeec6'});
                                $(this).focus();
                            };
                            if(after_changing_price == 0){
                                $('.alert').show() // undone- add alert message
                                $(this).css({'outline': '2px solid red', 'background': '#fbeec6'});
                                $(this).focus();
                            };

                            if(after_changing_price != '' && after_changing_price != 0){
                                product_qty = current_qty;
                                console.log('product qty: ',product_qty)
                                $('.alert').hide();
                                $(this).blur();
                                $.ajax({
                                    type: 'POST',
                                    url: '/find_product/',
                                    data: {
                                        'csrfmiddlewaretoken': csrf,
                                        'product_code': product_code,
                                        'product_qty': product_qty,
                                    },
                                    success: function(response){
                                        remove_product = response.product_code;
                                        price = response.product_price_after_discount
                        
                                        var lim1 = product_codes.length
                                        for(var i=0; i < lim1; i++){
                                            for(var j=0; j < lim1; j++){
                                                if(remove_product == product_codes[i]){
                                                    product_codes.splice(i, 1);
                                                    product_prices.splice(i, 1);
                                                };  
                                            };
                                        };

                                        total_price = 0;
                                        // console.log(typeof(product_prices[1]))
                                        for(var i=0; i < current_qty; i++){
                                            product_prices.push(after_changing_price);
                                            product_codes.push(remove_product);
                                            total_price = total_price + after_changing_price;
                                        };

                                        total_qty = product_prices.length;
                                        sub_total = 0;
                                        $.each(product_prices, function(index, value){
                                            sub_total += value;
                                        });
                                        $('#js-total-price' + remove_product).text(total_price);
                                        $('#js-sub-total').text(sub_total);
                                        $('#id_total_payable').text(sub_total);
                                        cart_handle(); //For changing account section. This cart_handle() function is called from the top (fn no.-3)
                                        // console.log(e.isImmediatePropagationStopped());
                                        
                                    } // 2nd success ended
                                });
                            } // else condition ended
                        };   
                    }); // keypress ended


                    $(this).blur(function(e){
                        e.stopImmediatePropagation()
                        var after_changing_price = parseInt($(this).text()) || 0; // NaN value will replace with 0;
                        product_code = $(this).data('product_code')
                        var current_qty = parseInt($(this).prev().text());

                        $(this).css({'outline': '', 'background': ''});
                        console.log(after_changing_price)
                        if(after_changing_price == ''){
                            $('.alert').show() // undone- add alert message
                            $(this).css({'outline': '2px solid red', 'background': '#fbeec6'});
                            $(this).focus();
                        };
                        if(after_changing_price == 0){
                            $('.alert').show() // undone- add alert message
                            $(this).css({'outline': '2px solid red', 'background': '#fbeec6'});
                            $(this).focus();
                        };
                        if(after_changing_price != '' && after_changing_price != 0){
                            product_qty = current_qty;
                            $('.alert').hide();
                            $.ajax({
                                type: 'POST',
                                url: '/find_product/',
                                data: {
                                    'csrfmiddlewaretoken': csrf,
                                    'product_code': product_code,
                                    'product_qty': product_qty,
                                },
                                success: function(response){
                                    $(this).blur();
                                    remove_product = response.product_code;
                                    price = response.product_price_after_discount
                    
                                    var lim1 = product_codes.length
                                    for(var i=0; i < lim1; i++){
                                        for(var j=0; j < lim1; j++){
                                            if(remove_product == product_codes[i]){
                                                product_codes.splice(i, 1);
                                                product_prices.splice(i, 1);
                                            };  
                                        };
                                    };

                                    total_price = 0;
                                    // console.log(typeof(product_prices[1]))
                                    for(var i=0; i < current_qty; i++){
                                        product_prices.push(after_changing_price);
                                        product_codes.push(remove_product);
                                        total_price = total_price + after_changing_price;
                                    };

                                    total_qty = product_prices.length;
                                    sub_total = 0;
                                    $.each(product_prices, function(index, value){
                                        sub_total += value;
                                    });
                                    $('#js-total-price' + remove_product).text(total_price);
                                    $('#js-sub-total').text(sub_total);
                                    $('#id_total_payable').text(sub_total);
                                    cart_handle(); //For changing account section. This cart_handle() function is called from the top (fn no.-3)
                                    // console.log(e.isImmediatePropagationStopped());
                                    
                                } // 2nd success ended
                            });
                        } // else condition ended

                    }) // blur ended


                });
                //-----------Editable price in the cart ended------------


            }, // 1st success ended
        }); // ajax ended



    }); // 1st event ended here.




    // For account section starts.
    $('#id_other_fee').keypress(function(e){
        var other_fee;
        var sub_total_;
        var total_payable;

        if(!(e.key in keycodes)){
            e.preventDefault();
        }
        if(e.which == 13 || e.which == 9){
            $(this).blur()
            var discount;
            other_fee = parseInt($(this).val()) || 0;
            var discount_value = parseInt($('#id_discount').val()) || 0;
            var check_discount = $('#id_dis_checkbox').is(":checked");
            sub_total_ = parseInt($('#js-sub-total').text());
            total_payable = parseInt($('#id_total_payable').text());

            if(check_discount == true){
                discount = get_discount(sub_total_, discount_value);
            }else{
                discount = discount_value;
            };

            if(sub_total_ == total_payable){
                total_payable = (sub_total_ - discount) + other_fee;
            }else{
                if(sub_total_ !== total_payable){
                    total_payable = (sub_total_ - discount) + other_fee; 
                };
            };
            $('#id_total_payable').text(total_payable);
            return_and_due_amount() // Calling function no.-4
        };
        $(this).blur(function(){
            var discount;
            other_fee = parseInt($(this).val()) || 0;
            var discount_value = parseInt($('#id_discount').val()) || 0;
            var check_discount = $('#id_dis_checkbox').is(":checked");
            sub_total_ = parseInt($('#js-sub-total').text());
            total_payable = parseInt($('#id_total_payable').text());

            if(check_discount == true){
                discount = get_discount(sub_total_, discount_value);
            }else{
                discount = discount_value;
            };

            if(sub_total_ == total_payable){
                total_payable = (sub_total_ - discount) + other_fee;
            }else{
                if(sub_total_ !== total_payable){
                    total_payable = (sub_total_ - discount) + other_fee; 
                };
            };
            $('#id_total_payable').text(total_payable);
            return_and_due_amount() // Calling function no.-4
        }); 
    });


    $('#id_discount').keypress(function(e){
        var check_discount;
        var discount_value;
        var sub_total__;
        var total_payable_;
        var other_fee_;
        if(!(e.key in keycodes)){
            e.preventDefault();
        }
        if(e.which == 13 || e.which == 9){
            $(this).blur();
            discount_value = parseInt($(this).val()) || 0;
            check_discount = $('#id_dis_checkbox').is(":checked");
            sub_total__ = parseInt($('#js-sub-total').text());
            total_payable_ = parseInt($('#id_total_payable').text());
            other_fee_ = parseInt($('#id_other_fee').val()) || 0;
            if(check_discount == true){
                total_payable_ = (sub_total__ - get_discount(sub_total__, discount_value)) + other_fee_;
            }else{
                total_payable_ = (sub_total__ - discount_value) + other_fee_;
            }
            
            $('#id_total_payable').text(total_payable_);
            return_and_due_amount() // Calling function no.-4
        };

        $(this).blur(function(){
            discount_value = parseInt($(this).val()) || 0;
            check_discount = $('#id_dis_checkbox').is(":checked");
            sub_total__ = parseInt($('#js-sub-total').text());
            total_payable_ = parseInt($('#id_total_payable').text());
            other_fee_ = parseInt($('#id_other_fee').val()) || 0;
            if(check_discount == true){
                total_payable_ = (sub_total__ - get_discount(sub_total__, discount_value)) + other_fee_;
            }else{
                total_payable_ = (sub_total__ - discount_value) + other_fee_;
            };
            
            $('#id_total_payable').text(total_payable_);
            return_and_due_amount() // Calling function no.-4
        });

    });

    $('#id_paid_or_take').keypress(function(e){
        if(!(e.key in keycodes)){
            e.preventDefault();
        };
        if(e.which == 13 || e.which == 9){
            $(this).blur();
            var take_amount = parseInt($(this).val()) || 0;
            var total_payable = parseInt($('#id_total_payable').text());
            var return_amount = take_amount - total_payable;
            if(total_payable > take_amount){
                var due_amount = total_payable - take_amount;
                return_amount = 0;
            }else{
                due_amount = 0;
            }
            $('#id_return_amount').text(return_amount);
            $('#id_due_amount').text(due_amount);

        };
        $(this).blur(function(){
            var take_amount = parseInt($(this).val()) || 0;
            var total_payable = parseInt($('#id_total_payable').text());
            var return_amount = take_amount - total_payable;
            if(total_payable > take_amount){
                var due_amount = total_payable - take_amount;
                return_amount = 0;
            }else{
                due_amount = 0;
            }
            $('#id_return_amount').text(return_amount);
            $('#id_due_amount').text(due_amount);
        });
    });
    // For account section ended here.






});