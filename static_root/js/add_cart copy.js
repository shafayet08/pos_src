$(document).ready(function(){
    var warning = `
                <div class="alert alert-danger" role="alert">
                You typed nothing. Please enter quantity.
                </div>`

    var product_prices = [];
    var products = [];
    var sub_total_list = [];
    $('#product-view-area-scroll .product-btn .add-cart').click(function(){
        var product = $(this).data('product')
        var product_id = $(this).data('product_id')
        var selling_price = parseInt($(this).data('selling_price'));
        var discount_from_product = parseInt($(this).data('discount'));
        var product_price_after_discount = selling_price - discount_from_product;


        // appending array list with product price after discount
        product_prices.push(product_price_after_discount)
        products.push(product)
        
        var qty = 0;
        var total_price = 0;
        var total_qty = 0;
        var sub_total = 0;
        
        for(var i in product_prices){
            if(product_price_after_discount == product_prices[i]){
                total_price = total_price + product_price_after_discount;
                qty = qty + 1;
                sub_total_list.push(total_price);
            }
            total_qty = total_qty + 1;
        }
        for(var i in sub_total_list){
            sub_total = sub_total + sub_total_list[i]
        }
        // qty = 0;
        // for(var i=0; i < products.length; i++){
        //     if(product == products[i]){
        //         qty += 1;
        //     }
        // };
        var id_total_price = '#show-cart #total_price' + product_id.toString() + '__';
        if(qty == 1){
            var cart_item = `
                                <tr>
                                    <td>${product}</td>
                                    <td id="${product}${product_id}" 
                                        contenteditable="true"
                                        data-qty=${qty}> ${qty}</td>
                                    <td id="price${product_id}_" contenteditable="true">${product_price_after_discount}</td>
                                    <td id="total_price${product_id}__">${total_price}</td>
                                    <td>
                                        <a href="#"><i class="fas fa-times"></i></a>               
                                    </td>
                                </tr> 
                            `
            $('#style-custom-narrow #show-cart').append(cart_item);
        };
        if(qty > 1){
            $('#show-cart #'+ product.toString() + product_id.toString()).text(qty)
            $(id_total_price).text(total_price);
            
        };
        // console.log(qty)
        // var total_price = 0;
        // var total_qty = 0;
        // for(var price in product_price){
        //     total_price += product_price[price];
        //     total_qty += 1;
        // }

        // Taking id in a variable
        var id_qty = '#show-cart #' + product.toString() + product_id.toString();
        var id_price = '#price' + product_id.toString() + '_';
        

        // Adding border in qty and price
        
        $(id_qty).click(function(){
            before_changing_qty = parseInt($(this).text());
            $(this).css({'border': '2px solid red', 'background': '#fbeec6'});

        });
        var checking = true;
        $(id_qty).click(function(e){
            if(checking){
                before_changing_qty = $(this).text()
                checking = false;
                console.log('before_changing qty: ', before_changing_qty);
            }
            
            $(this).css({'border': '2px solid red', 'background': '#fbeec6'});
            e.preventDefault();
            var keycode = [0,1,2,3,4,5,6,7,8,9];
            $(this).keypress(function(e){
                if(!(e.key in keycode)){
                    e.preventDefault(); 
                }
                if(e.which == 13){
                    $(this).css({'border': '', 'background': ''});
                    $(this).blur();
                    after_changing_qty = $(this).text()
                    if(after_changing_qty == ''){
                        $('.main-content').prepend(warning) // undone- add alert message
                        $(this).css({'border': '2px solid red', 'background': '#fbeec6'});
                        $(this).focus();
                    }
                    if(after_changing_qty != ''){
                        after_changing_qty = parseInt(after_changing_qty)
                        console.log('after ',before_changing_qty);
                        $('.alert').remove() // undone- add alert message
                        if(before_changing_qty < after_changing_qty){
                            var last_qty = after_changing_qty - before_changing_qty;
                            for(var i; i < last_qty; i++){
                                products.push(product)
                            }
                            console.log(products)
                        }
                    };
                    
                };
            });
            
            
            if(before_changing_qty < after_changing_qty){
                var last_qty = after_changing_qty - before_changing_qty;
                total_qty = last_qty + total_qty;
            }
            
        })
        // $(id_qty).keypress(function(e){
        //     if(e.which == 13){
        //         e.preventDefault();
        //         $(this).css({'border': '', 'background': ''}) 
        //     }
        //     after_changing_qty = $(this).text();
        //     console.log(after_changing_qty);  
        // });
        // $(id_qty).blur(function(){
        //     $(this).css({'border': '', 'background': ''})
        //     after_changing_qty = $(this).text();
        //     console.log(after_changing_qty);  
        // });
        // $(id_selling_price).click(function(){
        //     $(this).css({'border': '2px solid red', 'background': '#fbeec6'});
        //     var keycode = [0,1,2,3,4,5,6,7,8,9]
        //     $(this).keypress(function(e){
        //         if(!(e.key in keycode)){
        //             e.preventDefault(); 
        //         }
        //     })
        //     var before_changing_selling_price = $(this).text();
        //     console.log(before_changing_selling_price);
        // });
        // // Removing border in qty and price
        // $(id_selling_price).blur(function(){
        //     $(this).css({'border': '', 'background': ''})
        // });
        
        // $(id_selling_price).keypress(function(e){
        //     if(e.which == 13){
        //         e.preventDefault();
        //         $(this).css({'border': '', 'background': ''}) 
        //     }  
        // });

        

        $('tfoot #total-qty').text(total_qty);
        $('tfoot #sub-total').text(total_price);

    });



});




//     $('.add-cart').each(function(i){
//         p[i] = $(this).data('price');
//     })
//     console.log(p);
// })