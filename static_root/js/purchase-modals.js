$(document).ready(function(){

    // # Deleting Sale 
    $('.js-delete-purchase').click(function(){
        var purchase_id = $(this).data('sale_id')
        const csrf = $('input[name=csrfmiddlewaretoken').val();
        console.log(csrf) 
        $('#purchase-delete-modal-show').dialog({
            title: 'Do you want to delete?',
            modal: true,
            resizable: false,
            draggable: true,
    
        });
        $('#purchase_delete_no').click(function(e){
            e.stopImmediatePropagation()
            $('#purchase-delete-modal-show').dialog('close'); 
        })
        $('#purchase_delete_yes').click(function(){
            $.ajax({
                type: 'POST',
                url: '/delete-purchase-invoice/',
                dataType: 'json',
                data: {
                    'csrfmiddlewaretoken': csrf,
                    'purchase_id': purchase_id,
                },
                success: function(response){
                    if(response.data == true){
                        $('#purchase-delete-modal-show').dialog('close'); 
                        Toast.fire({
                            icon: 'success',
                            title: 'Success! The item successfully deleted.'
                        });
                        setTimeout(function(){
                            location.reload()
                        },2000);
                        
                        
                    }else{
                        $('#purchase-delete-modal-show').dialog('close'); 
                        Toast.fire({
                            icon: 'error',
                            title: 'error! Somthing went wrong.'
                        });
                    }
                } 
            });

        });
    });// # Deleting Sale ended


    $('#add-return-purchase-product').click(function(){
        console.log('hello button')
        $('#show-return-purchase-product-modal').dialog({
            title: 'Add New Return Purchase Product',
            width: 600,
            modal: true,
            resizable: false,
            draggable: true,
    
        });
    });




    



})