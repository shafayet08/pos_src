$(document).ready(function(){
    $('.js-create-customer').click(function(){
        $('#show-create-customer-modal').dialog({
            title: 'Create New Customer',
            width: 570,
            height: 600,
            modal: true,
            resizable: false,
            draggable: true,
    
        });
    });
    
    // This is not needed for this project just commented out for future
    // // # Deleting Sale 
    // $('.js-delete-customer').click(function(){
    //     console.log('clicked!')
    //     var customer_id = $(this).data('customer_id')
    //     const csrf = $('input[name=csrfmiddlewaretoken').val(); 
    //     $('#customer-delete-modal-show').dialog({
    //         title: 'Do you want to delete?',
    //         modal: true,
    //         resizable: false,
    //         draggable: true,
    
    //     });
    //     $('#customer_delete_no').click(function(e){
    //         e.stopImmediatePropagation()
    //         $('#customer-delete-modal-show').dialog('close'); 
    //     })
    //     $('#customer_delete_yes').click(function(){
    //         $.ajax({
    //             type: 'POST',
    //             url: '/delete-customer/',
    //             dataType: 'json',
    //             data: {
    //                 'csrfmiddlewaretoken': csrf,
    //                 'customer_id': customer_id,
    //             },
    //             success: function(response){
    //                 if(response.data == true){
    //                     $('#customer-delete-modal-show').dialog('close'); 
    //                     Toast.fire({
    //                         icon: 'success',
    //                         title: 'Success! The customer successfully deleted.'
    //                     });
    //                     setTimeout(function(){
    //                         location.reload()
    //                     },2000);
                        
                        
    //                 }else{
    //                     $('#customer-delete-modal-show').dialog('close'); 
    //                     Toast.fire({
    //                         icon: 'error',
    //                         title: 'error! Somthing went wrong.'
    //                     });
    //                 }
    //             } 
    //         });

    //     });
    // });// # Deleting Sale ended    
});
