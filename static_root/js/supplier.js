

//Modal Event Purchase 

let supplierModal = document.getElementById('modal-supplier-js'),
    openSupplierModal = document.getElementById('add-supplier-js'),
    closeSupplierModal = document.querySelector('.close-supplier-modal');

openSupplierModal.addEventListener('click', function() {
    supplierModal.style.display = 'block';
})

closeSupplierModal.addEventListener('click', function(){
    supplierModal.style.display = 'none';
})

window.addEventListener('click', function(e) {
    if(e.target == supplierModal) {
        supplierModal.style.display = 'none';
    }
})


