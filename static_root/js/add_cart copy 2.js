$(document).ready(function(){
    var warning = `
                <div class="alert alert-danger" role="alert">
                You typed nothing. Please enter quantity.
                </div>`

    var product_price = []
    var products = []
    $('.product-btn .add-cart').click(function(){
        var product = $(this).data('product')
        var product_id = $(this).data('product_id')
        var selling_price = parseInt($(this).data('selling_price'), 10);
        var discount_from_product = parseInt($(this).data('discount'), 10);
        var product_price_after_discount = selling_price - discount_from_product;


        // appending array list with product price after discount
        product_price.push(product_price_after_discount)
        products.push(product)
        


        var qty = 0;
        for(var i=0; i < products.length; i++){
            if(product == products[i]){
                qty += 1;
            }

        };
        if(qty == 1){
            var cart_item = `
                                <tr>
                                    <td>${product}</td>
                                    <td id="${product}${product_id}" 
                                        contenteditable="true"
                                        data-qty=${qty}> ${qty}</td>
                                    <td id="${product}${selling_price}" contenteditable="true">${product_price_after_discount}</td>
                                    
                                    <td>
                                        <a href="#"><i class="fas fa-times"></i></a>               
                                    </td>
                                </tr> 
                            `
            $('#show-cart').append(cart_item);
        };
        if(qty > 1){
            $('#show-cart #'+ product.toString() + product_id.toString()).text(qty)
        };
        var total_price = 0;
        var total_qty = 0;
        for(var price in product_price){
            total_price += product_price[price];
            total_qty += 1;
        }

        // Taking id in a variable
        var id_qty = '#show-cart #' + product.toString() + product_id.toString()
        var id_selling_price = '#' +  product.toString() + selling_price.toString()
        var before_changing_qty;
        var after_changing_qty;
        var tes = 1
        // Adding border in qty and price
        
        // $(id_qty).click(function(){
        //     before_changing_qty = parseInt($(this).text());
        //     $(this).css({'border': '2px solid red', 'background': '#fbeec6'});

        // });
        $(id_qty).click(function(e){
            before_changing_qty = parseInt($(this).text());
            $(this).css({'border': '2px solid red', 'background': '#fbeec6'});
            e.preventDefault();
            var keycode = [0,1,2,3,4,5,6,7,8,9];
            $(this).keypress(function(e){
                if(!(e.key in keycode)){
                    e.preventDefault(); 
                }
                if(e.which == 13){
                    $(this).css({'border': '', 'background': ''});
                    $(this).blur();
                    after_changing_qty = $(this).text()
                    if(after_changing_qty == ''){
                        $('.main-content').prepend(warning) // undone- add alert message
                        $(this).focus();
                    }
                    if(after_changing_qty != ''){
                        after_changing_qty = parseInt(after_changing_qty)
                        $('.alert').remove() // undone- add alert message
                        if(before_changing_qty < after_changing_qty){
                            var last_qty = after_changing_qty - before_changing_qty;
                            total_qty = last_qty + total_qty;
                        }

                    }
                    
                    // console.log(total_qty);
                }
            })
            console.log(total_qty);
        });
        
        // $(id_qty).keypress(function(e){
        //     if(e.which == 13){
        //         e.preventDefault();
        //         $(this).css({'border': '', 'background': ''}) 
        //     }
        //     after_changing_qty = $(this).text();
        //     console.log(after_changing_qty);  
        // });
        // $(id_qty).blur(function(){
        //     $(this).css({'border': '', 'background': ''})
        //     after_changing_qty = $(this).text();
        //     console.log(after_changing_qty);  
        // });
        $(id_selling_price).click(function(){
            $(this).css({'border': '2px solid red', 'background': '#fbeec6'});
            var keycode = [0,1,2,3,4,5,6,7,8,9]
            $(this).keypress(function(e){
                if(!(e.key in keycode)){
                    e.preventDefault(); 
                }
            })
            var before_changing_selling_price = $(this).text();
            console.log(before_changing_selling_price);
        });
        // Removing border in qty and price
        $(id_selling_price).blur(function(){
            $(this).css({'border': '', 'background': ''})
        });
        
        $(id_selling_price).keypress(function(e){
            if(e.which == 13){
                e.preventDefault();
                $(this).css({'border': '', 'background': ''}) 
            }  
        });

        
        
        
        $('tfoot #total-qty').text(total_qty);
        $('tfoot #sub-total').text(total_price);

    });




});




//     $('.add-cart').each(function(i){
//         p[i] = $(this).data('price');
//     })
//     console.log(p);
// })