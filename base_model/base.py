from django.db import models




'''
Common information
'''
class CommonInfo(models.Model):
    first_name      = models.CharField(max_length=30, verbose_name='First Name')
    last_name       = models.CharField(max_length=30, verbose_name='Last Name')
    email           = models.EmailField(blank=True)
    phone1          = models.CharField(max_length=11, blank=True, null=True, unique=True, help_text='Enter your eleven digit phone number without +88(ex- 01800000000)')
    phone2          = models.CharField(max_length=11, blank=True, null=True, unique=True, help_text='Enter your eleven digit phone number without +88(ex- 01800000000)')
    address1        = models.CharField(max_length=100, blank=True, null=True)
    address2        = models.CharField(max_length=100, blank=True, null=True)
    created         = models.DateTimeField(auto_now_add=True)
    
    class Meta:
        abstract = True

    @property    
    def get_full_name(self):
        if self.first_name and self.last_name:
            return f'{self.first_name} {self.last_name}'
        elif self.first_name:
            return self.first_name
        else:
            return self.last_name