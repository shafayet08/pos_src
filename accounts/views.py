from typing import Generic
from django.db import transaction
from django.shortcuts import render, redirect
from django.contrib import messages
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

import datetime
import ast
import json

from accounts.forms import (
    CreditModelForm,
    CreditTransferModelForm,
    WithdrawModelForm,
    SupplierPaymentForm
)
from accounts.models import CashFlow, Credit, CreditMethod, CreditTransfer, Statement, SupplierPayment, Withdraw

from sales.models import Sale
from products.models import Product
from expenses.models import Expense


# class CashFlowList(Generic.ListView):
# 	objects = CashFlow.objects.all()
# 	for item in objects:
# 		item.description = eval(str(item.description)
		
# 	queryset = Post.objects.filter(status=1).order_by('-created_on')
# 	template_name = 'index.html'
# 	paginate_by = 3

# Create your views here.
def cash_flow(request):

	page = request.GET.get('page', 1)
	objects = CashFlow.objects.all().order_by("-created")
	

	for item in objects:
		item.description = eval(str(item.description))

	paginator = Paginator(objects, 10)

	try:
		objects = paginator.page(page)
	except PageNotAnInteger:
		objects = paginator.page(1)
	except EmptyPage:
		objects = paginator.page(paginator.num_pages)


	context = {
        'title': 'Transaction History',
        'objects': objects,
    }

    # objects = CashFlow.objects.all()
    # for item in objects:
    #     item.description = eval(str(item.description))

    # context = {
    #     'title': 'Transaction History',
    #     'objects': objects,
    # }
	return render(request, 'accounts/cash-flow.html', context)


def statement(request):
    statements = Statement.objects.all()
    cash = CreditMethod.objects.get(method='cash')
    mobile_banking = CreditMethod.objects.get(method='mobile banking')
    bank = CreditMethod.objects.get(method='bank')

    context = {
        'title': 'Accounts',
        'cash': cash,
        'mobile_banking': mobile_banking,
        'bank': bank,
        'statements': statements,
    }

    return render(request, 'accounts/statement.html', context)


def credit_page(request):
    credit = Credit.objects.all()
    creditForm = CreditModelForm()
    credit_method = CreditMethod.objects.all()

    total_credit = sum(item.total_amount for item in credit_method)

    context = {
        'title': 'Credit',
        'credit_method': credit_method,
        'credits': credit,
        'creditForm': creditForm,
        'total_credit': total_credit,
    }
    return render(request, 'accounts/credit-page.html', context)


def add_credit(request):
    if request.method == 'POST':
        with transaction.atomic():
            form = CreditModelForm(request.POST)
            if form.is_valid():
                credited_amount = request.POST.get('amount')
                payment_method = CreditMethod.objects.get(
                    id=request.POST.get('credit_method'))
                instance = form.save(commit=False)
                print('id.... ', instance.id)
                instance.added_by = request.user
                instance.save()  # Here is a signal

                messages.success(
                    request, f'{credited_amount} Tk. has been successfully credited to {payment_method.method.upper()}.')
                return redirect(request.META['HTTP_REFERER'])


def credit_transfer_page(request):
    credit_transfer = CreditTransfer.objects.all()
    creditTransferForm = CreditTransferModelForm()

    context = {
        'title': 'Credit Transfer',
        'credit_transfer': credit_transfer,
        'creditTransferForm': creditTransferForm,
    }
    return render(request, 'accounts/credit-transfer-page.html', context)


def transfer_credit(request):
    if request.method == 'POST':
        with transaction.atomic():
            form = CreditTransferModelForm(request.POST)
            transfer_from = request.POST.get('transfer_from')
            transfer_to = request.POST.get('transfer_to')
            transfer_amount = request.POST.get('amount')
            transfer_from_credit_method = CreditMethod.objects.get(
                method=transfer_from)

            if transfer_from_credit_method.total_amount < int(transfer_amount):
                messages.error(
                    request, f'Oops! There is not enough balance in {transfer_from.upper()} to transfer.')
                return redirect(request.META['HTTP_REFERER'])
            elif transfer_to == transfer_from:
                messages.error(
                    request, f'Oops! You cannot transfer credit from {transfer_from.upper()} to {transfer_from_credit_method.method.upper()}.')
                return redirect(request.META['HTTP_REFERER'])
            else:
                if form.is_valid():
                    instance = form.save(commit=False)
                    instance.added_by = request.user
                    instance.save()

                    messages.success(
                        request, f'Credit has been transferred successfully from {transfer_from.upper()} to {transfer_to.upper()}.')
                    return redirect(request.META['HTTP_REFERER'])


def withdraw_credit_page(request):
    withdraw_credit = Withdraw.objects.all()
    withdrawForm = WithdrawModelForm()

    context = {
        'title': 'Withdraw Credit',
        'withdraw_credit': withdraw_credit,
        'withdrawForm': withdrawForm,
    }
    return render(request, 'accounts/withdraw-credit-page.html', context)


def withdraw_credit(request):
    if request.method == 'POST':
        with transaction.atomic():
            form = WithdrawModelForm(request.POST)
            withdraw_from = request.POST.get('withdraw_from')
            withdraw_amount = request.POST.get('amount')
            withdraw_from_credit_method = CreditMethod.objects.get(
                method=withdraw_from)

            if withdraw_from_credit_method.total_amount < int(withdraw_amount):
                messages.error(
                    request, f'Oops! There is not enough balance in {withdraw_from.upper()} to withdraw.')
                return redirect(request.META['HTTP_REFERER'])
            else:
                if form.is_valid():
                    instance = form.save(commit=False)
                    instance.added_by = request.user
                    instance.save()

                    messages.success(
                        request, f'{withdraw_amount} Tk has been withdrawn successfully from {withdraw_from.upper()}.')
                    return redirect(request.META['HTTP_REFERER'])


# Supplier payment page
def supplier_payment(request):
    supplier_payments = SupplierPayment.objects.all().order_by('-created')
    form = SupplierPaymentForm()
    context = {
        'title': 'Supplier Payments',
        'supplier_payments': supplier_payments,
        'form': form,
        'title': 'Supplier Payment'
    }
    return render(request, 'accounts/supplier_payment.html', context)


# Creating Supplier payment
def add_supplier_payment(request):
    if request.method == 'POST':
        with transaction.atomic():
            form = SupplierPaymentForm(request.POST)
            if form.is_valid():
                return _extracted_from_add_supplier_payment(form, request)


def _extracted_from_add_supplier_payment(form, request):
    purchase_invoice = form.cleaned_data.get('purchase_invoice')
    payment_amount = form.cleaned_data.get('amount')
    total_due_amount = purchase_invoice.get_total_due_amount
    if purchase_invoice.is_complete:
        messages.error(
            request, 'You cannot pay because you do not have due for this purchase')
        return redirect(request.META['HTTP_REFERER'])

    instance = form.save(commit=False)
    if total_due_amount < payment_amount:
        instance.amount = total_due_amount

    supplier = purchase_invoice.supplier
    instance.added_by = request.user
    instance.supplier = supplier
    instance.save()
    messages.success(request, 'Payment successfully added')
    return redirect(request.META['HTTP_REFERER'])


# Making Yearly Report for selling
def loss_profit(request):
    if request.method == 'POST':
        '''
        Getting current year's  starting date
        and ending date from user
        '''
        starting_year_date = request.POST.get('min')
        ending_year_date = request.POST.get('max')
        date_from_user = True
        '''
        Getting current year's  all Sale objects
        and getting total from it
        '''
        sales = Sale.objects.filter(
            delivery_date__gte=starting_year_date,
            delivery_date__lte=ending_year_date,
            is_complete=True
        )
        subtotal = sum(sale.get_sub_total for sale in sales)
        total_sold_amount_with_discount = sum(
            sale.get_total_sold_price for sale in sales)
        total_payable = sum(sale.get_total_payable for sale in sales)
        total_other_fee = sum(sale.other_fee for sale in sales)
        total_discount = sum(sale.get_discount for sale in sales)

        '''
        Getting current year's  all Product objects
        and getting total from it
        '''
        products = Product.objects.filter(
            created__gte=starting_year_date,
            created__lte=ending_year_date
        )
        total_cost_amount_on_product = sum(
            product.cost_price for product in products)

        '''
        Getting current year's  all Expense objects
        and getting total from it
        '''
        expenses = Expense.objects.filter(
            created__gte=starting_year_date,
            created__lte=ending_year_date,
        )
        total_expense = sum(expense.amount for expense in expenses)

        '''
        Getting current year's  Gross Profit
        '''
        gross_profit = total_sold_amount_with_discount - total_cost_amount_on_product

        '''
        Getting current year's  Net Profit
        '''
        net_profit = total_payable - \
            (total_cost_amount_on_product + total_expense)

        '''
        Getting current year's  Total Debit
        '''
        total_debit = total_cost_amount_on_product + total_discount + total_expense

        context = {
            'title': 'Loss-Profit',
            'subtotal': subtotal,
            'total_discount': total_discount,
            'total_other_fee': total_other_fee,
            'total_sold_amount_with_discount': total_sold_amount_with_discount,
            'total_payable': total_payable,
            'total_cost_amount_on_product': total_cost_amount_on_product,
            'total_expense': total_expense,
            'total_debit': total_debit,
            'gross_profit': gross_profit,
            'net_profit': net_profit,
            'starting_year_date': starting_year_date,
            'ending_year_date': ending_year_date,
            'date_from_user': date_from_user,

        }

        return render(request, 'accounts/loss-profit/loss-profit.html', context)

    '''
    Getting current year's  starting date
    and ending date
    '''
    today = datetime.date.today()
    current_year = today.year
    starting_year_date = datetime.date(current_year, 1, 31)
    ending_year_date = datetime.date(current_year, 12, 31)
    date_from_user = False

    '''
    Getting current year's  all Sale objects
    and getting total from it
    '''
    sales = Sale.objects.filter(
        delivery_date__gte=starting_year_date,
        delivery_date__lte=ending_year_date,
        is_complete=True
    )
    subtotal = sum(sale.get_sub_total for sale in sales)
    total_sold_amount_with_discount = sum(
        sale.get_total_sold_price for sale in sales)
    total_payable = sum(sale.get_total_payable for sale in sales)
    total_other_fee = sum(sale.other_fee for sale in sales)
    total_discount = sum(sale.get_discount for sale in sales)

    '''
    Getting current year's  all Product objects
    and getting total from it
    '''
    products = Product.objects.filter(
        created__gte=starting_year_date,
        created__lte=ending_year_date
    )
    total_cost_amount_on_product = sum(
        product.cost_price for product in products)

    '''
    Getting current year's  all Expense objects
    and getting total from it
    '''
    expenses = Expense.objects.filter(
        created__gte=starting_year_date,
        created__lte=ending_year_date,
    )
    total_expense = sum(expense.amount for expense in expenses)

    '''
    Getting current year's  Gross Profit
    '''
    gross_profit = total_sold_amount_with_discount - total_cost_amount_on_product

    '''
    Getting current year's  Net Profit
    '''
    net_profit = total_payable - (total_cost_amount_on_product + total_expense)

    '''
    Getting current year's  Total Debit
    '''
    total_debit = total_cost_amount_on_product + total_discount + total_expense

    context = {
        'title': 'Loss-Profit',
        'subtotal': subtotal,
        'total_discount': total_discount,
        'total_other_fee': total_other_fee,
        'total_sold_amount_with_discount': total_sold_amount_with_discount,
        'total_payable': total_payable,
        'total_cost_amount_on_product': total_cost_amount_on_product,
        'total_expense': total_expense,
        'total_debit': total_debit,
        'gross_profit': gross_profit,
        'net_profit': net_profit,
        'starting_year_date': 'This Year',
        'ending_year_date': 'This Year',
        'date_from_user': date_from_user,

    }

    return render(request, 'accounts/loss-profit/loss-profit.html', context)
