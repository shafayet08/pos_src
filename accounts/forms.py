from django import forms
from django.db import models

from accounts.models import Credit, CreditTransfer, SupplierPayment, Withdraw

class CreditModelForm(forms.ModelForm):
    class Meta:
        model = Credit
        exclude = ('added_by', 'edited_by',)


class CreditTransferModelForm(forms.ModelForm):
    class Meta:
        model = CreditTransfer
        exclude = ('added_by', 'edited_by',)

class WithdrawModelForm(forms.ModelForm):
    class Meta:
        model = Withdraw
        exclude = ('added_by', 'edited_by',)

class SupplierPaymentForm(forms.ModelForm):
    class Meta:
        model = SupplierPayment
        exclude = ['added_by', 'edited_by', 'supplier']