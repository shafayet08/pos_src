from django.contrib import admin
from accounts.models import (Credit, CreditMethod,
                            CreditTransfer,
                            Withdraw, 
                            Statement,
                            CashFlow)
# Register your models here.

admin.site.register(Credit)
admin.site.register(CreditMethod)
admin.site.register(CreditTransfer)
admin.site.register(Withdraw)
admin.site.register(Statement)
admin.site.register(CashFlow)
