from django.urls import path
from accounts import views
import accounts

app_name = 'accounts'
urlpatterns = [
    path('cash-flow/', views.cash_flow, name='cash_flow'),
    path('statement/', views.statement, name='statement'),
    path('credit-page/', views.credit_page, name='credit_page'),
    path('add-credit/', views.add_credit, name='add_credit'),
    path('credit-transfer-page/', views.credit_transfer_page, name='credit_transfer_page'),
    path('transfer-credit/', views.transfer_credit, name='transfer_credit'),
    path('withdraw-credit-page/', views.withdraw_credit_page, name='withdraw_credit_page'),
    path('withdraw-credit/', views.withdraw_credit, name='withdraw_credit'),


    #Supplier Payment
    path('supplier-payment/', views.supplier_payment, name='supplier_payment'),
    path('add-supplier-payment/', views.add_supplier_payment, name='add_supplier_payment'),


    #Loss profit views-url
    path('loss-profit/', views.loss_profit, name='loss_profit'),
    
]