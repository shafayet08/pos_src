$(document).ready(function(){
    $('#add-credit').click(function(){
        $('#add-credit-modal-show, transfer-credit').dialog({
            title: 'Add Credit',
            width: 600,
            modal: true,
            resizable: false,
            draggable: true,
    
        });
    });

    $('#transfer-credit').click(function(){
        $('#transfer-credit-modal-show').dialog({
            title: 'Credit Transfer',
            width: 600,
            modal: true,
            resizable: false,
            draggable: true,
    
        });
    });

    $('#withdraw-credit').click(function(){
        $('#withdraw-modal-show').dialog({
            title: 'Do you want to proceed?',
            width: 600,
            modal: true,
            resizable: false,
            draggable: true,
    
        });
    });

    $('#add-supplier-payment').click(function(){
        $('#add-supplier-payment-modal-show').dialog({
            title: 'Add New Supplier Payment',
            width: 600,
            modal: true,
            resizable: false,
            draggable: true,
    
        });
    });
    
});