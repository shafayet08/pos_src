$(document).ready(function(){
    // Selecting supplier id and appply 'select2' method return purchase product form
    $("#id_purchase_invoice, #id_payment_method").select2({
        dropdownParent: $("#add-supplier-payment-modal-show"),
        placeholder: 'Select an option',
        theme: "classic",
        width: '100%'
    });
})