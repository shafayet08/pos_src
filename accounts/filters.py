from django.forms.fields import DateTimeField
import django_filters
from django_filters import DateFilter
from accounts.models import Statement

class StatementFilter(django_filters.FilterSet):
    starting_date = DateFilter(field_name='date', lookup_expr='gte', label='Starting Date')
    ending_date = DateFilter(field_name='date', lookup_expr='lte', label='Ending Date')

    class Meta:
        model = Statement
        exclude= '__all__'
    
