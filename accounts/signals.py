from datetime import date
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.db import transaction

from accounts.models import (
    CreditMethod,
    Credit, CreditTransfer,
    Statement, Withdraw,
    CashFlow,
    SupplierPayment
)
# A callable function-1(CreditMethod) for creating 'CashFlow'.


def create_cashflow_for_credit_method(obj, credit_method=None, is_debit=False, context=None):

    if credit_method is not None:
        credit_method = CreditMethod.objects.get(method=credit_method)
        CashFlow.objects.create(
            credit_method=credit_method,
            description=context,
            amount=obj.amount,
            remaining_credit=credit_method.total_amount,
            is_debit=is_debit,)
    else:
        raise ValueError('went somethig wrong')


@receiver(post_save, sender=Credit)
def add_credit_to_credit_method(sender, instance, created, **kwargs):
    if created or created == False:
        with transaction.atomic():
            credit_method_id = instance.credit_method.id
            credit_method = CreditMethod.objects.get(id=credit_method_id)
            credit_method.total_amount = credit_method.total_amount + instance.amount
            credit_method.save()

            # Creating and updating CashFlow Table
            obj = Credit.objects.get(id=instance.id)
            context = {
                'credit id': obj.id,
                'title': obj.title,
                'description': obj.description,
            }
            is_debit = False
            credit_method = instance.credit_method
            # Calling function-1(CreditMethod) for creating 'CashFlow'.
            create_cashflow_for_credit_method(
                obj, credit_method, is_debit, context)


# Transfer
@receiver(post_save, sender=CreditTransfer)
def credit_transfer(sender, instance, created, **kwargs):
    if created or created == False:
        credit_method = CreditMethod.objects.get(method=instance.transfer_from)
        credit_method.total_amount = credit_method.total_amount - instance.amount
        credit_method.save()

        credit_method = CreditMethod.objects.get(method=instance.transfer_to)
        credit_method.total_amount = credit_method.total_amount + instance.amount
        credit_method.save()

        # Creating and updating CashFlow Table
        obj = CreditTransfer.objects.get(id=instance.id)
        context = {
            'transfer id': obj.id,
            'transfer from': instance.transfer_from,
            'transfer to': instance.transfer_to,
            'description': obj.description,
        }
        is_debit = True
        credit_method = instance.transfer_from
        # Calling function-1(CreditMethod) for creating 'CashFlow'.
        create_cashflow_for_credit_method(
            obj, credit_method, is_debit, context)


@receiver(post_save, sender=Withdraw)
def withdraw_credit(sender, instance, created, **kwargs):
    if created or created == False:
        credit_method = CreditMethod.objects.get(method=instance.withdraw_from)
        credit_method.total_amount = credit_method.total_amount - instance.amount
        credit_method.save()

        # Creating and updating CashFlow Table
        obj = Withdraw.objects.get(id=instance.id)
        context = {
            'withdraw id': obj.id,
            'withdraw from': instance.withdraw_from,
            'description': obj.description,
        }
        is_debit = True
        credit_method = instance.withdraw_from
        # Calling function-1(CreditMethod) for creating 'CashFlow'.
        create_cashflow_for_credit_method(
            obj, credit_method, is_debit, context)


# Creating Statement for adding Credit, Credit Transfer and Credit Withdraw
@receiver(post_save, sender=Credit)
def create_statement_for_credit(sender, instance, created, **kwargs):
    if created:
        Statement.objects.create(
            statement_id=instance.id,
            title=instance.title,
            description=instance.description,
            credit_to=instance.credit_method,
            amount=instance.amount
        )
    else:
        statement_id = instance.id
        statement = Statement.objects.get(statement_id=statement_id)

        statement.title = instance.title
        statement.description = instance.description
        statement.credit_to = instance.credit_method.method
        statement.amount = instance.amount

        statement.save()


@receiver(post_save, sender=CreditTransfer)
def create_statement_for_credit_transfer(sender, instance, created, **kwargs):
    if created:
        Statement.objects.create(
            statement_id=instance.id,
            title=instance.title,
            description=instance.description,
            transfer_from=instance.transfer_from,
            transfer_to=instance.transfer_to,
            amount=instance.amount
        )
    else:
        statement_id = instance.id
        statement = Statement.objects.get(statement_id=statement_id)

        statement.title = instance.title
        statement.description = instance.description
        statement.transfer_from = instance.transfer_from
        statement.transfer_to = instance.transfer_to
        statement.amount = instance.amount

        statement.save()


@receiver(post_save, sender=Withdraw)
def create_statement_for_withdraw_credit(sender, instance, created, **kwargs):
    if created:
        Statement.objects.create(
            statement_id=instance.id,
            title=instance.title,
            description=instance.description,
            withdraw_from=instance.withdraw_from,
            amount=instance.amount
        )
    else:
        statement_id = instance.id
        statement = Statement.objects.get(statement_id=statement_id)

        statement.title = instance.title
        statement.description = instance.description
        statement.withdraw_from = instance.withdraw_from
        statement.amount = instance.amount

        statement.save()


# Creating CashFlow and Updating CreditMethod when SupplierPayment is created
@receiver(post_save, sender=SupplierPayment)
def create_cashflow_and_update_credit_method(sender, instance, created, **kwargs):
    if created:
        total_purchase_amount = instance.purchase_invoice.get_total_purchase_amount
        payment_method = instance.payment_method
        credit_method = CreditMethod.objects.get(method=payment_method)
        if total_purchase_amount < instance.amount:
            credit_method.total_amount = credit_method.total_amount - total_purchase_amount
        else:
            credit_method.total_amount = credit_method.total_amount - instance.amount
        credit_method.save()

        # Creating and updating CashFlow Table
        obj = SupplierPayment.objects.get(id=instance.id)
        context = {
            'id': obj.id,
            'title': 'supplier payment',
            'description': obj.description,
        }
        is_debit = True
        credit_method = instance.payment_method
        # Calling function-1(CreditMethod) for creating 'CashFlow'.
        create_cashflow_for_credit_method(
            obj, credit_method, is_debit, context)

        purchase_invoice = instance.purchase_invoice
        supplier_payments = purchase_invoice.supplierpayment_set.all()
        total_supplier_payment = sum(item.amount for item in supplier_payments)

        if total_purchase_amount != total_supplier_payment:
            purchase_invoice.is_complete = False
        else:
            purchase_invoice.is_complete = True

        purchase_invoice.save()
