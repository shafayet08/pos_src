from datetime import date
from suppliers.models import Supplier
from purchases.models import PurchaseInvoice, ReturnPurchaseProduct
from expenses.models import Expense
from sales.models import Sale
from django.db import models
# from django.contrib.auth.models import User
from django.conf import settings

# Create your models here.
class CreditMethod(models.Model):
    CASH = 'cash'
    MOBILE_BANKING = 'mobile banking'
    BANK  = 'bank'

    CHOICES = [
        (CASH, 'Cash'),
        (MOBILE_BANKING, 'Mobile Banking'),
        (BANK, 'Bank'),
    ]

    method = models.CharField(max_length=15, choices=CHOICES)
    total_amount = models.IntegerField(default=0)

    def __str__(self):
        return self.method
    



#Add Credit Table
class Credit(models.Model):
    credit_method = models.ForeignKey(CreditMethod, on_delete=models.SET_NULL, null=True)
    title = models.CharField(max_length=50)
    description = models.CharField(max_length=100, blank=True)
    amount = models.IntegerField(default=0)
    created = models.DateTimeField(auto_now_add=True)
    edited = models.DateTimeField(auto_now=True)
    added_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null= True, related_name='add+')
    def __str__(self):
        return self.title


#Credit Transfer Table
class CreditTransfer(models.Model):
    CASH = 'cash'
    MOBILE_BANKING = 'mobile banking'
    BANK  = 'bank'

    FROM = [
        (CASH, 'Cash'),
        (MOBILE_BANKING, 'Mobile Banking'),
        (BANK, 'Bank'),
    ]
    TO = [
        (CASH, 'Cash'),
        (MOBILE_BANKING, 'Mobile Banking'),
        (BANK, 'Bank'),
    ]

    title = models.CharField(max_length=50)
    description = models.CharField(max_length=100, blank=True)
    transfer_from = models.CharField(max_length=15, choices=FROM, verbose_name='Transfer From')
    transfer_to = models.CharField(max_length=15, choices=TO, verbose_name='Transfer To')
    amount = models.IntegerField(default=0)
    created = models.DateTimeField(auto_now_add=True)
    edited = models.DateTimeField(auto_now=True)
    added_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null= True, related_name='add+')
    def __str__(self):
        return self.title


#Withdraw Credit Table
class Withdraw(models.Model):
    CASH = 'cash'
    MOBILE_BANKING = 'mobile banking'
    BANK  = 'bank'

    FROM = [
        (CASH, 'Cash'),
        (MOBILE_BANKING, 'Mobile Banking'),
        (BANK, 'Bank'),
    ]

    title = models.CharField(max_length=50)
    description = models.CharField(max_length=100, blank=True)
    withdraw_from = models.CharField(max_length=15, choices=FROM, verbose_name='Withdraw From')
    amount = models.IntegerField(default=0)
    created = models.DateTimeField(auto_now_add=True)
    edited = models.DateTimeField(auto_now=True)
    added_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null= True, related_name='add+')

    def __str__(self):
        return self.title


class Statement(models.Model):
    statement_id = models.IntegerField()
    created = models.DateTimeField(auto_now_add=True)
    title = models.CharField(max_length=50, blank=True)
    description = models.CharField(max_length=100, blank=True)
    credit_to = models.CharField(max_length=20, blank=True)
    withdraw_from = models.CharField(max_length=20, blank=True)
    transfer_from = models.CharField(max_length=20, blank=True)
    transfer_to = models.CharField(max_length=20, blank=True)
    amount = models.IntegerField(default=0)

# class CloseAccount(models.Model):
#     created = models.DateTimeField(auto_now_add=True)
#     cash = models.IntegerField(default=0)
#     bank = models.IntegerField(default=0)
#     mobile_bangking = models.IntegerField(default=0)


class SupplierPayment(models.Model):
    CASH = 'cash'
    MOBILE_BANKING = 'mobile banking'
    BANK  = 'bank'

    CHOICES = [
        (CASH, 'Cash'),
        (MOBILE_BANKING, 'Mobile Banking'),
        (BANK, 'Bank'),
    ]

    purchase_invoice = models.ForeignKey(PurchaseInvoice, models.CASCADE)
    supplier =  models.ForeignKey(Supplier, models.CASCADE, null=True)
    payment_method  = models.CharField(max_length=15, choices=CHOICES)    
    amount = models.IntegerField()
    description = models.TextField(max_length=100, blank=True, null=True)
    created = models.DateTimeField(auto_now_add=True)
    added_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null= True, related_name='add+')
    edited_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null= True, related_name='edit+', blank=True)


class CashFlow(models.Model):
    CASH = 'cash'
    MOBILE_BANKING = 'mobile banking'
    BANK  = 'bank'

    CHOICES = [
        (CASH, 'Cash'),
        (MOBILE_BANKING, 'Mobile Banking'),
        (BANK, 'Bank'),
    ]

    created = models.DateTimeField(auto_now_add=True)
    credit_method = models.CharField(max_length=15, choices=CHOICES)
    description = models.TextField()
    amount = models.IntegerField()
    remaining_credit = models.IntegerField()
    is_debit = models.BooleanField(default=False)


    # credit = models.ForeignKey(Credit, on_delete=models.CASCADE, blank=True, null=True)
    # credit_transfer = models.ForeignKey(CreditTransfer, on_delete=models.CASCADE, blank=True, null=True)
    # withdraw = models.ForeignKey(Withdraw, on_delete=models.CASCADE, blank=True, null=True)
    # expense = models.ForeignKey(Expense, on_delete=models.CASCADE, blank=True, null=True)
    # sale = models.ForeignKey(Sale, on_delete=models.CASCADE, blank=True, null=True)
    # supplier_payment = models.ForeignKey(SupplierPayment, on_delete=models.CASCADE, blank=True, null=True)
    # return_purchase_product = models.ForeignKey(ReturnPurchaseProduct, on_delete=models.CASCADE, blank=True, null=True)






# CASH = 'cash'
# MOBILE_BANKING = 'mobile banking'
# BANK  = 'bank'

# CHOICES = [
#     (CASH, 'Cash'),
#     (MOBILE_BANKING, 'Mobile Banking'),
#     (BANK, 'Bank'),
# ]

# common_id       = models.IntegerField()
# date            = models.DateTimeField(auto_now_add=True)
# account_type    = models.TextField(max_length=15, choices=CHOICES)
# description     = models.CharField(max_length=150, blank=True, null=True)
# is_debit        = models.BooleanField(default=False)
# is_credit       = models.BooleanField(default=True)
# amount          = models.IntegerField(default=0)