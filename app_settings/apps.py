from django.apps import AppConfig


class AppSettingsConfig(AppConfig):
    name = 'app_settings'

    def ready(self):
        import app_settings.signals