from django import forms
from django.contrib.auth import models
from django.forms import fields
from app_settings.models import GeneralSetting


class GeneralSettingForm(forms.ModelForm):
    class Meta:
        model = GeneralSetting
        fields = '__all__'