from django.contrib import admin

from app_settings.models import GeneralSetting
# Register your models here.

admin.site.register(GeneralSetting)
