from app_settings.context_processor import notification
from django.urls import path
from app_settings import views

app_name = 'app_settings'
urlpatterns = [
    path('general-setting/', views.general_setting, name='general_setting'),
    path('notification/', views.notification, name='notification')
  
]

