from django.shortcuts import render, redirect
from django.contrib import messages

from django.contrib.auth.decorators import login_required, user_passes_test

from app_settings.forms import GeneralSettingForm
from app_settings.models import GeneralSetting
from inventories.models import Inventory


@login_required()
@user_passes_test(lambda u: u.is_active or u.is_manager or u.is_owner or u.is_superuser)
def general_setting(request):
    general_setting = GeneralSetting.objects.last()
    form = GeneralSettingForm(instance=general_setting)
    if request.method == 'POST':
        form = GeneralSettingForm(
            request.POST or None, request.FILES or None, instance=general_setting)
        if form.is_valid():
            form.save()
            messages.success(request, "Success!")
            return redirect('app_settings:general_setting')

    context = {
        'title': 'General Settings',
        'form': form,
    }

    return render(request, 'app_settings/general-setting.html', context)


# Notification, commented out for future use
# def view_all_notification(request):
#     gsetting = GeneralSetting.objects.last()
#     ntf_no = gsetting.notification
#     inventories = Inventory.objects.all()
#     notifications = []
#     for item in inventories:
#         remaining_product = item.get_total_remaining_product
#         if remaining_product <= ntf_no:
#             notifications.append(item)
#     context = {
#         'title': 'All Notificatoins',
#         'notifications': notifications,
#     }
#     return render(request, 'app_settings/view-notification', context)


def notification(request):

    # assigning initial value as 'False' for gsetting
    gsetting = False
    try:
        gsetting = GeneralSetting.objects.last()
    except Exception as e:
        print(e)
    # checking if gsetting has queryset or not
    notifications = []
    if gsetting:
        ntf_no = gsetting.notification

        inventories = Inventory.objects.all()

        if inventories:
            for item in inventories:
                remaining_product = item.get_total_remaining_product
                if remaining_product and remaining_product <= ntf_no:
                    notifications.append(item)

    context = {
        'notifications': notifications,
    }
    return render(request, 'app_settings/notification.html', context)
