from inventories.views import inventory
from app_settings.models import GeneralSetting
from products.models import Product
from inventories.models import Inventory




def notification(request):

    #For General Setting
    gsetting = False
    try:
        gsetting = GeneralSetting.objects.last()
    except:
        pass

    if gsetting:
        site_title = gsetting.company_name or 'Company Name'
        site_logo = gsetting.logo or False
    else:
          site_title = False
          site_logo = False
          site_title = 'Company Name'

    return {
            'site_title': site_title,
            'site_logo': site_logo,
        }



# def notification(request):

#     #For General Setting
#     gsetting = False
#     try:
#         gsetting = GeneralSetting.objects.last()
#     except:
#         pass

#     if gsetting:
#         site_title = gsetting.company_name or 'Company Name'
#         site_logo = gsetting.logo or False

#         #For showing Product Notification
#         ntf_no = gsetting.notification
#         inventories = Inventory.objects.all()
#         notifications = []
#         if inventories:
#             for item in inventories:
#                 remaining_product = item.get_total_remaining_product
#                 if remaining_product and remaining_product <= ntf_no:
#                     notifications.append(item)

#         total_notification = len(notifications)
#     else:
#           site_title = False
#           site_logo = False
#           notifications = False
#           total_notification = 0
#           site_title = 'Company Name'

#     return {
#             'site_title': site_title,
#             'site_logo': site_logo,
#             'notifications': notifications,
#             'total_notification': total_notification,
#         }
