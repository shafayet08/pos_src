from django.db.models.signals import pre_save
from django.dispatch import receiver
from django.core.files.base import ContentFile

from PIL import Image
from io import BytesIO
from app_settings.models import GeneralSetting



@receiver(pre_save, sender=GeneralSetting)
def generate_company_logo_thumbnail(sender, instance, **kwargs):
    try:
        logo = Image.open(instance.logo)
        logo = logo.convert("RGB")

        if logo.height > 300 or logo.width > 300:
            THUMBNAIL_SIZE = (300, 300)
            logo.thumbnail(THUMBNAIL_SIZE)
            temp_thumb = BytesIO()
            logo.save(temp_thumb, 'JPEG')
            temp_thumb.seek(0)

            instance.logo.save(
                instance.logo.name,
                ContentFile(temp_thumb.read()),
                save=False,
            )
            temp_thumb.close()
    except:
        pass