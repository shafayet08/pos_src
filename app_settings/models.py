from django.db import models

# Create your models here.
class GeneralSetting(models.Model):
    company_name = models.CharField(max_length=50)
    logo = models.ImageField(upload_to='company_logo', blank=True, null=True)
    owner = models.CharField(max_length=25)
    phone1 = models.CharField(max_length=11)
    phone2 = models.CharField(max_length=11, blank=True, null=True)
    email1 = models.EmailField(verbose_name='Email-1')
    email2 = models.EmailField(verbose_name='Email-2', blank=True, null=True)
    address = models.CharField(max_length=50)
    notification = models.IntegerField(default=0, verbose_name='Get Product Stock Notification')
    def __str__(self):
        return self.company_name
    
