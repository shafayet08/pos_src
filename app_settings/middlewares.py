from django.http import HttpResponse

class UnderConstructionMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response
    
    def __call__(self, request):
        print('I am first')
        response = HttpResponse('Our website is now under construction')
        print('I am second')
        return response