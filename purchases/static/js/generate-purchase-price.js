$(document).ready(function(){


    $('#js-generate-total-purchase-price').click(function(e){
        var purchase_submit_btn = `<button type="submit" class="btn btn-info btn-md btn-block float-right">Submit</button>`
        
        e.stopImmediatePropagation()
        var cost_prices = [];
        var product_qty = [] 
        var i = 0;
        var j = 0;
        var qty_check = false;
        var cost_price_check = false;
        $('.js-cost-price :input').each(function(){
            
            var cost_price = $(this).val() | 0; // taking the value of Cost Price
            var qty_id = ("id_form-"+i+"-qty") // taking the input id of qty
            var qty = parseInt($('#'+qty_id).val() | 0) 

            if(cost_price > 0 && qty == 0){
                $('#'+qty_id).css({'background': '#fbeec6'});
                $('#'+qty_id).focus()
                cost_price_check = false;

            }

            if(cost_price > 0 && qty != 0){
                $('#'+qty_id).css({ 'background': ''});
                $('#'+qty_id).blur();
                cost_price_check = true;
            }
            i++;

            cost_prices.push(cost_price)
            
        });
        
        $('.js-product-qty :input').each(function(){
            var qty = $(this).val() | 0; // taking the value of Cost Price
            var cost_price_id = ("id_form-"+j+"-cost_price") // taking the input id of qty
            var cost_price = parseInt($('#'+cost_price_id).val() | 0) 
            if(qty > 0 && cost_price == 0){
                $('#'+cost_price_id).css({'background': '#fbeec6'});
                $('#'+cost_price_id).focus()
                qty_check = false;
            }
            if(cost_price > 0 && qty != 0){
                $('#'+cost_price_id).css({'background': ''});
                $('#'+cost_price_id).blur();
                qty_check = true;
            }
            j++;
            product_qty.push(qty);
            
        });

        

        if(qty_check == true && cost_price_check == true){
            var final_payment = 0;
            for(var i = 0; i < cost_prices.length; i++){
                var total = cost_prices[i] * product_qty[i]
                final_payment = (final_payment + total)
                
            }

            var html = `<div class="card text-white bg-info mb-3">
                            <div class="row card-header">
                                <div class="col-sm-6">
                                    <h3 class="">Payment Details</h3>
                                </div>
                                <div class="col-sm-6">
                                    <h3 class=" float-sm-right">Final Payment BDT. <strong>${final_payment.toLocaleString()}</strong> </h3>
                                </div>
                            </div>
                            <div class="card-body text-info">
                            <div class="row">
                                <!-- select -->
                                <div class="col-sm-6 form-group">
                                    <label class="text-white" for="purchase-payment">Select Payment Method</label>
                                    <select id="purchase-payment" class="form-control" name="payment_method" required>
                                        <option value="" selected>---------</option>
                                        <option value="cash">Cash</option>
                                        <option>Mobile Banking</option>
                                        <option>Bank</option>
                                    </select>
                                </div>
                                <div class="col-sm-6 form-group">
                                    <label class="text-white" for="id_purchase_payment">Pay</label>
                                    <input type="number" class="form-control" id="id_purchase_payment" value="" name="purchase_payment" required>
                                </div>
                            </div>
                            </div>
                        </div>`


            $('#purchase-sipnner').removeClass('d-none')
            setTimeout(function(){
                $('#purchase-sipnner').addClass('d-none')
                $('#final-payment').removeClass('d-none')
                $('#final-payment').html(html)
                $('#purchase-submit-btn').html(purchase_submit_btn)
            },1000);
            
        };
        if(qty_check == false && cost_price_check == false){
            $('#purchase-sipnner').addClass('d-none')
            $('#final-payment').addClass('d-none')
        };

    })
})