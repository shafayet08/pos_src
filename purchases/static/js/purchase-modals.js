$(document).ready(function(){

    // # Deleting Sale 
    $('.js-delete-purchase-return').click(function(){
        var purchase_return_id = $(this).data('purchase_return_id')
        const csrf = $('input[name=csrfmiddlewaretoken').val();
        $('#delete-purchase-return-show').dialog({
            title: 'Do you want to delete?',
            modal: true,
            resizable: false,
            draggable: true,
    
        });
        $('#delete_purchase_return_no').click(function(e){
            e.stopImmediatePropagation()
            $('#delete-purchase-return-show').dialog('close'); 
        })
        $('#delete_purchase_return_yes').click(function(){
            $.ajax({
                type: 'POST',
                url: '/delete-purchase-return/',
                dataType: 'json',
                data: {
                    'csrfmiddlewaretoken': csrf,
                    'purchase_return_id': purchase_return_id,
                },
                success: function(response){
                    if(response.data == true){
                        $('#purchase-delete-modal-show').dialog('close'); 
                        setTimeout(function(){
                            Toast.fire({
                                icon: 'success',
                                title: 'Success! The item successfully deleted.'
                            });
                            location.reload()
                        },2000);
                        
                        
                    }else{
                        $('#purchase-delete-modal-show').dialog('close'); 
                        Toast.fire({
                            icon: 'error',
                            title: response.error
                        });
                    }
                } 
            });

        });
    });// # Deleting Sale ended


    $('#add-return-purchase-product').click(function(){
        console.log('hello button')
        $('#show-return-purchase-product-modal').dialog({
            title: 'Add New Return Purchase Product',
            width: 600,
            modal: true,
            resizable: false,
            draggable: true,
    
        });
    });




    



})