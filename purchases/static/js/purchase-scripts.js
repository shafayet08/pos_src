$(document).ready(function(){

    // Selecting supplier id and appply 'select2' method purchase product form
    $("#id_supplier").select2({

    });

    // Selecting all product id and appply 'select2' method in purchase product form
    $('.card-body select').each(function(){
        $('#' + $(this).attr('id')).select2({

        });
        
    });



    // Selecting supplier id and appply 'select2' method return purchase product form
    $("#id_purchase_invoice, #id_product").select2({
        dropdownParent: $("#show-return-purchase-product-modal"),
        placeholder: 'Select an option',
        theme: "classic",
        width: '100%'
    });
});