from django.urls import path
from purchases import views

app_name = 'purchases'
urlpatterns = [
    path('create-purchase/', views.create_purchase, name='create_purchase'),
    # path('update-purchase-invoice/<int:pk>/', views.update_purchase_invoice, name='update_purchase_invoice'),
    path('delete-purchase-invoice/', views.delete_purchase_invoice, name='delete_purchase_invoice'),

    # Purchase return product
    path('delete-purchase-return/', views.delete_purchase_return, name='delete_purchase_return'),
    path('return-purchase-product/', views.return_purchase_product, name='return_purchase_product'),
    
    path('purchase-history/', views.purchase_history, name='purchase_history'),
    path('purchase-invoice-details/<int:pk>/', views.purchase_invoice_details, name='purchase_invoice_details'),
    path('purchase-report/', views.purchase_report, name='purchase_report'),
    

]
# args=[str(self.id)]
# args=[str:pk]