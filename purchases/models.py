from django.db import models
from django.conf import settings
from django.utils.translation import ugettext_lazy as _

from suppliers.models import Supplier
from products.models import Product

class PurchaseInvoice(models.Model):

    CASH = 'cash'
    MOBILE_BANKING = 'mobile banking'
    BANK  = 'bank'
    CHEQUE = 'cheque'

    CHOICES = [
       (CASH, _('Cash')),
       (MOBILE_BANKING, _('Mobile Banking')),
       (BANK, _('Bank')),
       (CHEQUE, _('Cheque')),
   ]

    supplier        = models.ForeignKey(Supplier, on_delete=models.SET_NULL, null=True)
    created         = models.DateTimeField(auto_now_add=True)
    payment_method  = models.CharField(max_length=15, choices=CHOICES)
    is_complete     = models.BooleanField(default=False)


    def __str__(self):
        return f'Inoivce Id-{str(self.id)}'
    
    

    @property
    def get_total_due_amount(self):
        if self.is_complete == False:
            total_payment = sum(item.amount for item in self.supplierpayment_set.all())
            return self.get_total_purchase_amount - total_payment

    @property
    def get_total_purchase_amount(self):
        purchases = self.purchase_set.all()
        return sum(item.get_total_price for item in purchases)

    @property
    def get_total_qty(self):
        purchases = self.purchase_set.all()
        total = 0
        for item in purchases:
            total = total + item.qty
        return total

    @property
    def get_supplier(self):
        try:
            purchase = self.purchase_set.first()
            return purchase.supplier.get_full_name
        except:
            pass

    @property
    def get_supplier_address1(self):
        try:
            purchase = self.purchase_set.first()
            return purchase.supplier.address1
        except:
            pass

    @property
    def get_supplier_phone1(self):
        try:
            purchase = self.purchase_set.first()
            phone1 = purchase.supplier.phone1
            if phone1:
                return phone1
        except:
            pass
        
    @property
    def get_supplier_phone2(self):
        try:
            purchase = self.purchase_set.first()
            phone2 = purchase.supplier.phone2
            if phone2:
                return phone2
        except:
            pass

    @property
    def get_supplier_email(self):
        try:
            return self.purchase_set.first().supplier.email
        except:
            pass
    
    @property
    def get_sales_man(self):
        try:
            return self.purchase_set.first().added_by
        except:
            pass
    

# Create your models here.
class Purchase(models.Model):
    product = models.ForeignKey(Product, on_delete=models.SET_NULL, null=True)
    supplier = models.ForeignKey(Supplier, on_delete=models.SET_NULL, null=True)
    purchase_invoice = models.ForeignKey(PurchaseInvoice, on_delete=models.SET_NULL, null=True)
    brand = models.CharField(max_length=20, blank=True)
    cost_price = models.IntegerField()
    selling_price = models.IntegerField(default=0)
    qty = models.IntegerField()
    discount = models.IntegerField(default=0)
    is_discount_in_parcentage = models.BooleanField(default=False, verbose_name='Discount %')
    created = models.DateTimeField(auto_now_add=True)
    purchase_by = models.CharField(max_length=20)
    added_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True, related_name='add+')
    edited_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True, related_name='edit+', blank=True)

    def __str__(self):
        return str(self.id)

    @property
    def get_total_price(self):
        return self.qty * self.cost_price


class ReturnPurchaseProduct(models.Model):
    purchase_invoice     = models.ForeignKey(PurchaseInvoice, on_delete=models.CASCADE)
    product              = models.ForeignKey(Product, on_delete=models.SET_NULL, null=True)
    cost_price           = models.IntegerField()
    selling_price        = models.IntegerField(null=True, blank=True)
    qty                  = models.IntegerField(default=0)
    created              = models.DateTimeField(auto_now_add=True)
    added_by             = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null= True, related_name='add+')
    edited_by            = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null= True, related_name='edit+', blank=True)

    def get_purchase_price(self):
        return self.purchase_invoice.purchase_set.get(product=self.product).cost_price
