from datetime import date
from purchases.models import PurchaseInvoice, ReturnPurchaseProduct
from django.db.models.signals import post_save, pre_delete
from django.dispatch import receiver


from accounts.models import CashFlow,CreditMethod, SupplierPayment
from expenses.models import Expense
from products.models import Product
from utils.create_cashflow import create_cashflow



# @receiver(pre_delete, sender=PurchaseInvoice)
# def pre_purchase_invoice_delete(sender, instance, **kwargs):
    
#     purchase_items = instance.purchase_set.all()
#     for item in purchase_items:
#         product_id = item.product.id
#         product = Product.objects.get(id=product_id) 
#         product.purchase_qty = product.purchase_qty - item.qty
#         product.save()
    
#     payment_method = instance.payment_method
#     credit_method = CreditMethod.objects.get(method=payment_method)
#     credit_method.total_amount = credit_method.total_amount + instance.total_paid_amount
#     credit_method.save()



@receiver(post_save, sender=ReturnPurchaseProduct)
# Update CreditMethod and create CashFlow after creating ReturnPurchaseProduct
def update_and_create_credit_method_and_cash_flow(sender, instance, created, **kwargs):
    if created:
        #Adding purchase quantity to the product model
        product = Product.objects.get(id=instance.product.id)
        product.purchase_qty = product.purchase_qty - instance.qty
        product.save()

        #adding total_purchase_amount to the creditmethod model 
        payment_method = instance.purchase_invoice.payment_method
        credit_method = CreditMethod.objects.get(method=payment_method)
        total_purchase_amount = instance.cost_price * instance.qty
        credit_method.total_amount = credit_method.total_amount + total_purchase_amount
        credit_method.save()

        #creating 'CashFlow'
        context = {
            'purchase invoice id': instance.purchase_invoice.id,
            'return product id': instance.id,
            'title': 'purchase return',
        }
        amount = total_purchase_amount
        create_cashflow(credit_method, context, amount, is_debit=False)
