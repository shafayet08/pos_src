from django import forms
from django.forms import fields

from suppliers.models import Supplier
from purchases.models import Purchase, PurchaseInvoice, ReturnPurchaseProduct
from products.models import Product


class ModelPurchaseForm(forms.ModelForm):
    class Meta:
        model = Purchase
        fields = (
                    'product', 'brand', 'cost_price', 
                    'selling_price', 'qty', 'discount', 
                    'is_discount_in_parcentage', 'purchase_by'
                )
        def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)
            self.render_required_fields = True



class PurchaseInvoiceUpdateForm(forms.ModelForm):
    class Meta:
        model = PurchaseInvoice
        fields = ['is_complete']

class ReturnPurchaseProductForm(forms.ModelForm):
    class Meta:
        model = ReturnPurchaseProduct
        exclude = ['added_by', 'edited_by', 'cost_price',]



class SupplierForm(forms.ModelForm):
    class Meta:
        model = Purchase
        fields = ('supplier',)

class SupplierModelForm(forms.ModelForm):
    class Meta:
        model = Supplier
        exclude = ('added_by', 'updated_by',)


