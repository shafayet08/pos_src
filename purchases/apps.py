from django.apps import AppConfig


class PurchasesConfig(AppConfig):
    name = 'purchases'
    
    def ready(self):
        import purchases.signals