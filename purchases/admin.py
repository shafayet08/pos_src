from django.contrib import admin

from purchases.models import Purchase,PurchaseInvoice
# Register your models here.
admin.site.register(Purchase)
admin.site.register(PurchaseInvoice)

