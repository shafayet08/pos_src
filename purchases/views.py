from django.http import HttpResponseRedirect, JsonResponse
from django.urls import reverse
from django.shortcuts import render, redirect
from django.forms.formsets import formset_factory
from django.db import transaction, IntegrityError
from django.contrib import messages


from django.contrib.auth.decorators import login_required, user_passes_test


from purchases.forms import (ModelPurchaseForm, 
                            SupplierForm,
                            PurchaseInvoiceUpdateForm
                        )

from purchases.models import (
                    Purchase, 
                    PurchaseInvoice, 
                    ReturnPurchaseProduct
                )
from purchases.forms import ReturnPurchaseProductForm
from products.models import Product
from products.forms import ProductModelForm
from suppliers.models import Supplier
from suppliers.forms import SupplierModelForm
from accounts.models import CreditMethod, CashFlow, SupplierPayment
from utils.create_cashflow import create_cashflow





@login_required()
@user_passes_test(lambda u: u.is_active or u.is_manager or u.is_owner or u.is_superuser)
def create_purchase(request):
    supplierModelForm = SupplierModelForm()
    productModelForm = ProductModelForm()

    total_order = request.GET.get('order_no')
    if total_order:
        total_order = int(total_order)
        if total_order and total_order > 0:
            formSet = formset_factory(ModelPurchaseForm, extra=total_order)
            
            sForm = SupplierForm()
    else:
        formSet = ''
        sForm = ''



    if request.method == "POST":
        
        formSet = formSet(request.POST)
        sForm = SupplierForm(request.POST)
        
        if formSet.is_valid() and sForm.is_valid():
            supplier = Supplier.objects.get(id=request.POST.get('supplier'))

            # Minus purchase payment from Credit Method.
            payment_method = request.POST.get('payment_method')
            if payment_method != '':
                # payment_method = payment_method.lower()
                # purchase_payment = int(request.POST.get('purchase_payment'))
                # credit_method = CreditMethod.objects.get(method=payment_method)
                # credit_method.total_amount = credit_method.total_amount - purchase_payment
                # credit_method.save()

                                
                purchase_payment = int(request.POST.get('purchase_payment'))
                # creating  Purchase Invoice Table
                PurchaseInvoice.objects.create()
                pur_invoice = PurchaseInvoice.objects.last()
                
            try:
                with transaction.atomic():
                    for form in formSet:
                        data = form.cleaned_data
                        if data:
                            instance = form.save(commit=False)
                            instance.supplier = supplier
                            instance.purchase_invoice = pur_invoice
                            instance.added_by = request.user
                            instance.save()

                            purchase = Purchase.objects.last()
                            product_id = purchase.product.id
                            product = Product.objects.get(id=product_id)
                            product.brand = purchase.brand

                            purchases = Purchase.objects.filter(product=product)
                            total_products = purchases.count()
                            total_product_amount = sum(item.cost_price for item in purchases)
                            total_average_product_price = total_product_amount / total_products

                            product.cost_price = total_average_product_price
                            
                            if purchase.selling_price != 0:
                                product.selling_price = purchase.selling_price


                            product.discount = purchase.discount
                            product.is_discount_in_parcentage = purchase.is_discount_in_parcentage
                            product.purchase_qty = product.purchase_qty + purchase.qty
                            product.save()

                    # updating last created PurchaseInvoice
                    total_purchase_amount = pur_invoice.get_total_purchase_amount
                    if total_purchase_amount != purchase_payment and not total_purchase_amount < purchase_payment:
                        pur_invoice.supplier = supplier
                        pur_invoice.payment_method = payment_method
                        pur_invoice.is_complete = False
                        pur_invoice.save()

                        SupplierPayment.objects.create(
                            purchase_invoice=pur_invoice,
                            payment_method=payment_method,
                            amount=purchase_payment,
                            description='First Payment',
                            added_by=request.user
                        )
                    else:
                        if total_purchase_amount <= purchase_payment:
                            pur_invoice.supplier = supplier
                            pur_invoice.payment_method = payment_method
                            pur_invoice.is_complete = True
                            pur_invoice.save()

                            SupplierPayment.objects.create(
                                purchase_invoice=pur_invoice,
                                payment_method=payment_method,
                                amount=total_purchase_amount,
                                added_by=request.user
                            )

                    messages.success(request, 'Purchase has been successfully Done!')
                    return redirect(request.META['HTTP_REFERER'])

            except Exception as e:
                print(e)
                messages.error(request, e)
                return redirect(request.META['HTTP_REFERER'])


    context = {
        'formSet': formSet,
        'sForm': sForm,
        'form': supplierModelForm,
        'productModelForm': productModelForm,
    }
    return render(request, 'purchases/create_purchase.html', context)


@login_required()
@user_passes_test(lambda u: u.is_active or u.is_manager or u.is_owner or u.is_superuser)
def purchase_history(request):
    invoices = PurchaseInvoice.objects.all()
    context = {
        'invoices': invoices
    }
    return render(request, 'purchases/purchase-history.html', context)

#Updating purchase invoice
# def update_purchase_invoice(request, pk):
#     try:    
#         purchase_invoice= PurchaseInvoice.objects.get(pk=pk)
#         form = PurchaseInvoiceUpdateForm(instance=purchase_invoice)
#         if request.method == 'POST':
#             form = PurchaseInvoiceUpdateForm(request.POST, instance=purchase_invoice)
#             if form.is_valid():
#                 form.save()
#                 messages.success(request, 'Success! Your Purchase has been completed.')
#                 return redirect('purchases:purchase_history')
#         return render(request, 'purchases/purchase-invoice-update-form.html', {'form': form})
#     except:
#         messages.error(request, 'Oops! Your Purchase Invoice is not found.')
#         return redirect('purchases:purchase_history')
    



# Deleting purchase invoice
@login_required()
@user_passes_test(lambda u: u.is_active or u.is_manager or u.is_owner or u.is_superuser)
def delete_purchase_invoice(request):
    if request.is_ajax:
        purchase_id = request.POST.get('purchase_id')
        PurchaseInvoice.objects.get(id=purchase_id).delete() 
        data = True
    else:
        data = False

    return JsonResponse({'data': data})  



@login_required()
@user_passes_test(lambda u: u.is_active or u.is_manager or u.is_owner or u.is_superuser)
def return_purchase_product(request):
    return_purchase_products = ReturnPurchaseProduct.objects.all()
    form = ReturnPurchaseProductForm()
    if request.method == 'POST':
        form = ReturnPurchaseProductForm(request.POST)
        if form.is_valid():
            try:
                with transaction.atomic():
                    purchase_invoice = form.cleaned_data.get('purchase_invoice')
                    product = form.cleaned_data.get('product')
                    return_qty = form.cleaned_data.get('qty')

                    purchase_product = purchase_invoice.purchase_set.get(product=product)

                    #Preventing adding return puchase product from pending Purchase(PurchaseInvoice)
                    if purchase_invoice.is_complete == False:
                        messages.error(request, 'Cannot return Purchase Product because the Purchase is not complete yet')
                        return redirect(request.META['HTTP_REFERER'])

                    if purchase_product.qty < return_qty:
                        messages.error(request, f'Oops! You are returning {return_qty} quantity but purchase quantity is {purchase_product.qty}')
                        return redirect(request.META['HTTP_REFERER'])
                    else:
                        instance = form.save(commit=False)
                        instance.cost_price = purchase_product.cost_price
                        instance.added_by = request.user
                        instance.save()
                        
                        purchase_product.qty = purchase_product.qty - return_qty
                        purchase_product.save()
                    
                    #deleting product from purchase
                    if purchase_product.qty == 0:
                        purchase_product.delete()
                    
                    if purchase_invoice.purchase_set.count() == 0:
                        purchase_invoice.delete()

                    messages.success(request, 'Return Purchase Product has been Successfully added')
                    return redirect(request.META['HTTP_REFERER'])
            except Exception as e:
                print(e)
                messages.error(request, e)
                return redirect(request.META['HTTP_REFERER'])
        else:
            print(form.errors)
            messages.error(request, form.errors)
            return redirect(request.META['HTTP_REFERER'])

    context = {
        'title': 'Purchase Return',
        'return_purchase_products': return_purchase_products,
        'form': form,
    }
    return render(request, 'purchases/return-purchase-product.html', context)
def delete_purchase_return(request):
    pass
    #ajax request
    if request.method == 'POST':
        id = request.POST.get('purchase_return_id')
        obj = ReturnPurchaseProduct.objects.get(id=id)
    if obj:
        try:
            with transaction.atomic():
                purchase_invoice = obj.purchase_invoice
                selling_price = obj.product.selling_price
                if selling_price > 0:
                    selling_price = selling_price
                else:
                    selling_price = obj.selling_price
                
                Purchase.objects.create(
                    purchase_invoice = obj.purchase_invoice,
                    product = obj.product,
                    supplier = purchase_invoice.supplier,
                    cost_price = obj.cost_price,
                    qty = obj.qty,
                    selling_price = selling_price,
                )
                credit_method = CreditMethod.objects.get(method=purchase_invoice.payment_method)
                total_product_price = obj.cost_price * obj.qty
                credit_method.total_amount = credit_method.total_amount - total_product_price
                
                #Adding purchase quantity to the product model
                product = Product.objects.get(id=obj.product.id)
                product.purchase_qty = product.purchase_qty + obj.qty
                product.save() 

                #Creating CashFlow
                is_debit=True
                amount = total_product_price
                context = {
                    'purchase return id': obj.id,
                    'title': 'delete purchase return',
                    'purchase invoice id': purchase_invoice.id, 
                }
                create_cashflow(credit_method, context, amount, is_debit)
                #Creating CashFlow ends
                
                credit_method.save()
                obj.delete()
                context = {
                    'data': True,
                }
        except Exception as e:
            print(e)
            context = {
                'error': e,
                'data': False,
            }
    
    return JsonResponse(context)

@login_required()
@user_passes_test(lambda u: u.is_active or u.is_manager or u.is_owner or u.is_superuser)
def purchase_invoice_details(request, pk):
    invoice = PurchaseInvoice.objects.get(id=pk)
    details = invoice.purchase_set.all().order_by('-id')
    supplier = invoice.purchase_set.first()


    context = {
        'details': details,
        'invoice': invoice,
        'supplier': supplier,
    }
    return render(request, 'purchases/pr-invoice-details.html', context)


@login_required()
@user_passes_test(lambda u: u.is_active or u.is_manager or u.is_owner or u.is_superuser)
def purchase_report(request):
    invoices = PurchaseInvoice.objects.all()
    context = {
        'invoices': invoices
    }
    return render(request, 'purchases/purchase-report.html', context)





















    # def create_purchase(request):
    # supplierModelForm = SupplierModelForm()
    # productModelForm = ProductModelForm()

    # total_order = request.GET.get('order_no')
    # if total_order:
    #     total_order = int(total_order)
    #     if total_order and total_order > 0:
    #         formSet = formset_factory(ModelPurchaseForm, extra=total_order)
            
    #         sForm = SupplierForm()
    # else:
    #     formSet = ''
    #     sForm = ''



    # if request.method == "POST":
    #     supplier = Supplier.objects.get(id=request.POST.get('supplier'))



    #     formSet = formSet(request.POST)
        
    #     if formSet.is_valid():
    #         # Minus purchase payment from Credit Method.
    #         payment_method = request.POST.get('payment_method').lower()
    #         purchase_payment = int(request.POST.get('purchase_payment'))
    #         credit_method = CreditMethod.objects.get(method=payment_method)

    #         credit_method.total_amount = credit_method.total_amount - purchase_payment
    #         credit_method.save()

    #         # creating  Purchase Invoice Table
    #         PurchaseInvoice.objects.create()
    #         pur_invoice = PurchaseInvoice.objects.last()

    #         try:
    #             with transaction.atomic():
    #                 for form in formSet:
    #                     data = form.cleaned_data
    #                     if data:
    #                         instance = form.save(commit=False)
    #                         instance.supplier = supplier
    #                         instance.purchase_invoice = pur_invoice
    #                         instance.added_by = request.user
    #                         instance.save()

    #                         purchase = Purchase.objects.last()
    #                         product_id = purchase.product.id
    #                         product = Product.objects.get(id=product_id)
    #                         product.origin = purchase.brand
    #                         product.cost_price = purchase.cost_price
    #                         product.selling_price = (product.selling_price + purchase.selling_price) / 2
    #                         product.discount = purchase.discount
    #                         product.is_discount_in_parcentage = purchase.is_discount_in_parcentage
    #                         product.purchase_qty = product.purchase_qty + purchase.qty
    #                         product.save()
            
    #             messages.success(request, 'Purchase has been successfully Done!')
    #             return redirect(request.META['HTTP_REFERER'])

    #         except IntegrityError:
    #             messages.success(request, 'Went Integrity error! Contact to the Developer')
    #             return redirect(request.META['HTTP_REFERER'])


    #         # return HttpResponseRedirect(reverse('purchases:purchase_invoice_details', args=(pur_invoice.id,)))

    #     else:
    #         messages.error(request, 'Oops! Something went wrong. Please create again. ')
    #         return redirect(request.META['HTTP_REFERER'])


    # context = {
    #     'formSet': formSet,
    #     'sForm': sForm,
    #     'supplierModelForm': supplierModelForm,
    #     'productModelForm': productModelForm,
    # }
    # return render(request, 'purchases/create_purchase.html', context)
