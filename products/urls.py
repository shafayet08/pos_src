from django.urls import path

from products import views

app_name = 'products'
urlpatterns = [
    path('', views.dashboard, name='dashboard'),
    path('category/', views.product_category, name='category'),
    path('category-details/<int:pk>', views.category_details, name='category_details'),
    path('create-product-cat', views.create_product_cat, name='create_category'),


    path('product/', views.product, name='product'),
    path('create-product/', views.create_product, name='create_product'),
    path('update-product/<int:pk>/', views.update_product, name='update_product'),
    path('delete-product/', views.delete_product, name='delete_product'),


    path('sales-order/', views.salesOrder, name='sales_order'),
    path('customer-list/', views.customerList, name='customer_list'),
    path('customerProfile/', views.customerProfile, name='customerProfile'),


    path('sales-order/search/', views.search, name='search_product'),
    path('find_product/', views.find_product, name='find_product'),

    path('print-barcode/', views.print_barcode, name='print_barcode'),

]
