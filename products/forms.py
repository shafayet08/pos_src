from django.db import models
from products.models import Product, Category
from django import forms

from products.models import Product
class SearchProductForm(forms.ModelForm):
    class Meta:
        model = Product
        fields = ['name']

class CategoryModelForm(forms.ModelForm):
    class Meta:
        model = Category
        exclude = ['added_by', 'updated_by']

class ProductModelForm(forms.ModelForm):
    class Meta:
        model   = Product
        exclude = ('added_by', 'updated_by', 'purchase_qty', 'cost_price', 'barcode',)


class BarcodeForm(forms.Form):
    product = forms.ModelChoiceField(Product.objects.all(), label="Product")
    qty = forms.IntegerField(label="Quantity")
    class Meta:
        model = Product
        fields = ['product, qty']
