from accounts.models import SupplierPayment
from accounts.views import supplier_payment
from inventories.models import Inventory
from inventories.views import inventory
from django.http import JsonResponse, response
from django.shortcuts import redirect, render
from django.contrib.auth.models import User
from django.contrib import messages
from django.template.loader import render_to_string
from django.forms import formset_factory

from django.contrib.auth.decorators import login_required, user_passes_test

import datetime
from dateutil.relativedelta import relativedelta
import json

from products.models import Category, Product
from sales.models import Sale
from sales.forms import SaleUpdateForm
from products.forms import CategoryModelForm, ProductModelForm, BarcodeForm
from purchases.models import PurchaseInvoice
from expenses.models import Expense
from sales.models import DamageProduct
from customers.models import Customer
from customers.forms import CustomerModelForm
from inventories.models import Inventory
from app_settings.models import GeneralSetting




@login_required(redirect_field_name='users:login')
def dashboard(request):

    title = 'Dashboard'
    today = datetime.datetime.now()


    today = datetime.datetime(today.year, today.month, today.day)
    next_day = today + datetime.timedelta(days=1)
    yesterday = today - datetime.timedelta(days=1)

    # Initialising start week and ending week
    current_week = today - datetime.timedelta(days=today.isoweekday())

    last_week = current_week - datetime.timedelta(days=7)
    next_week = current_week + datetime.timedelta(days=7)


    starting_date_of_current_month = datetime.datetime(today.year, today.month, 1)
    current_month = starting_date_of_current_month
    next_month = current_month + relativedelta(months=+1)
    last_month = starting_date_of_current_month - datetime.timedelta(days=1)

    current_year = datetime.datetime(today.year, 1, 1)
    next_year = current_year +  relativedelta(years=1)
    last_year = current_year -  relativedelta(years=1)

    '''
    Today vs Yesterday
    '''
    # Today start
    sales = Sale.objects.filter(
                        delivery_date__gte=today,
                        delivery_date__lt=next_day,
                        is_complete=True
                    )

    today_total_sale_amount_with_discount = sum(sale.get_total_sold_price for sale in sales)
    today_total_discount = sum(sale.get_discount for sale in sales)
    today_total_other_fee = sum(sale.other_fee for sale in sales)
    today_total_sales =  sales.count()



    # Getting salewise total purchase price from product not from purchase product list
    today_sale_wise_total_purchase_amount = 0
    try:
        for sale in sales:
            sale_items = sale.saleitem_set.all()
            sale_wise_purchase_price_from_product = sum(item.product.cost_price for item in sale_items)
            today_sale_wise_total_purchase_amount += sale_wise_purchase_price_from_product
    except:
        pass


    damage_products = DamageProduct.objects.filter(created__gte=today, created__lt=next_day)
    today_total_product_damage = sum(item.damage_qty for item in damage_products)
    today_total_product_damage_amount = sum(item.product.cost_price for item in damage_products)

    customers = Customer.objects.filter(created__gte=today, created__lt=next_day)
    today_total_customer_added = customers.count()

    purchase_invoices = PurchaseInvoice.objects.filter(
                        created__gte=today, 
                        created__lt=next_day,
                        is_complete=True,
                    )

    today_total_purchase_amount = sum(invoice.get_total_purchase_amount for invoice in purchase_invoices)

    expenses = Expense.objects.filter(created__gte=today, created__lt=next_day)
    today_total_expense = sum(expense.amount for expense in expenses)

    supplier_payments_obj = SupplierPayment.objects.filter(created__gte=today, created__lt=next_day)
    today_total_supplier_payment = sum(item.amount for item in supplier_payments_obj)

    today_gross_profit = today_total_sale_amount_with_discount - today_sale_wise_total_purchase_amount
    
    #Getting today's  Net Profit
    today_total_payable = sum(item.get_total_payable for item in sales)
    today_total_damage_amount = sum(item.get_total_damage_amount for item in damage_products)
    today_total_net_profit = today_total_payable - (today_sale_wise_total_purchase_amount + today_total_expense + today_total_damage_amount)
    # Today end 
    #--------------------



    # Yesterday start
    sales = Sale.objects.filter(
                        delivery_date__gte=yesterday,
                        delivery_date__lt=today, 
                        is_complete=True
                    )
    yesterday_total_sale_amount_with_discount = sum(sale.get_total_sold_price for sale in sales)
    yesterday_total_discount = sum(sale.get_discount for sale in sales)
    yesterday_total_other_fee = sum(sale.other_fee for sale in sales)
    yesterday_total_sales =  sales.count()


    # Getting salewise total purchase price from product not from purchase product list
    
    yesterday_sale_wise_total_purchase_amount = 0
    try:
        for sale in sales:
            sale_items = sale.saleitem_set.all()
            sale_wise_purchase_price_from_product = sum(item.product.cost_price for item in sale_items)
            yesterday_sale_wise_total_purchase_amount = yesterday_sale_wise_total_purchase_amount + sale_wise_purchase_price_from_product
    except:
        pass

    damage_products = DamageProduct.objects.filter(
                                                    created__gte=yesterday, 
                                                    created__lt=today
                                                )
    yesterday_total_product_damage = sum(item.damage_qty for item in damage_products)
    yesterday_total_product_damage_amount = sum(item.product.cost_price for item in damage_products)
    customers = Customer.objects.filter(created__gte=yesterday, created__lt=today)
    yesterday_total_customer_added = customers.count()

    purchase_invoices = PurchaseInvoice.objects.filter(
                        created__gte=yesterday, 
                        created__lt=today,
                        is_complete=True,
                    )
    yesterday_total_purchase_amount = sum(invoice.get_total_purchase_amount for invoice in purchase_invoices)
    
    expenses = Expense.objects.filter(created__gte=yesterday, created__lt=today)
    yesterday_total_expense = sum(expense.amount for expense in expenses)

    supplier_payments_obj = SupplierPayment.objects.filter(created__gte=yesterday, created__lt=today)
    yesterday_total_supplier_payment = sum(item.amount for item in supplier_payments_obj)


    yesterday_gross_profit = yesterday_total_sale_amount_with_discount - yesterday_sale_wise_total_purchase_amount
    
    #Getting yesterday's total net profit
    yesterday_total_payable = sum(item.get_total_payable for item in sales)
    yesterday_total_damage_amount = sum(item.get_total_damage_amount for item in damage_products)
    yesterday_total_net_profit = yesterday_total_payable - (yesterday_sale_wise_total_purchase_amount + yesterday_total_expense + yesterday_total_damage_amount)
 
    # Yesterday end

    '''
    Today vs Yesterday ended
    '''






    '''
    This week  vs Last week - start
    '''
    # This week start
    sales = Sale.objects.filter(
                delivery_date__gte=current_week,
                delivery_date__lt=next_week, 
                is_complete=True
            )
    this_week_total_sale_amount_with_discount = sum(sale.get_total_sold_price for sale in sales)
    this_week_total_discount = sum(sale.get_discount for sale in sales)
    this_week_total_other_fee = sum(sale.other_fee for sale in sales)
    this_week_total_sales =  sales.count()


    # Getting salewise total purchase price from product not from purchase product list
    this_week_sale_wise_total_purchase_amount = 0
    for sale in sales:
        sale_items = sale.saleitem_set.all()
        sale_wise_purchase_price_from_product = sum(item.product.cost_price for item in sale_items)
        this_week_sale_wise_total_purchase_amount = this_week_sale_wise_total_purchase_amount + sale_wise_purchase_price_from_product
    

    damage_products = DamageProduct.objects.filter(
                created__gte=current_week,
                created__lt=next_week

            )
    this_week_total_product_damage = sum(item.damage_qty for item in damage_products)
    this_week_total_product_damage_amount = sum(item.product.cost_price for item in damage_products)
    customers = Customer.objects.filter(
                created__gte = current_week,
                created__lt=next_week

            )
    this_week_total_customer_added = customers.count()

    purchase_invoices = PurchaseInvoice.objects.filter(
                created__gte=current_week,
                created__lt=next_week,
                is_complete=True,
            )
    this_week_total_purchase_amount = sum(invoice.get_total_purchase_amount for invoice in purchase_invoices)
    
    expenses = Expense.objects.filter(
                created__gte=current_week,
                created__lt=next_week,
            )
    this_week_total_expense = sum(expense.amount for expense in expenses)

    supplier_payments_obj = SupplierPayment.objects.filter(
                created__gte=current_week,
                created__lt=next_week,
            )
    this_week_total_supplier_payment = sum(item.amount for item in supplier_payments_obj)


    this_week_gross_profit = this_week_total_sale_amount_with_discount - this_week_sale_wise_total_purchase_amount
    
    
    #Getting this week's total net profit
    this_week_total_payable = sum(item.get_total_payable for item in sales)
    this_week_total_damage_amount = sum(item.get_total_damage_amount for item in damage_products)
    this_week_total_net_profit = this_week_total_payable - (this_week_sale_wise_total_purchase_amount + this_week_total_expense + this_week_total_damage_amount)
 
    # This week end 
    #-----0-----
    

    # Last week start

    sales = Sale.objects.filter(
                delivery_date__gte=last_week,
                delivery_date__lt=current_week, 
                is_complete=True
            )
    last_week_total_sale_amount_with_discount = sum(sale.get_total_sold_price for sale in sales)
    last_week_total_discount = sum(sale.get_discount for sale in sales)
    last_week_total_other_fee = sum(sale.other_fee for sale in sales)
    last_week_total_sales =  sales.count()


     # Getting salewise total purchase price from product not from purchase product list
    last_week_sale_wise_total_purchase_amount = 0
    try:
        for sale in sales:
            sale_items = sale.saleitem_set.all()
            sale_wise_purchase_price_from_product = sum(item.product.cost_price for item in sale_items)
            last_week_sale_wise_total_purchase_amount = last_week_sale_wise_total_purchase_amount + sale_wise_purchase_price_from_product
    except:
        pass

    damage_products = DamageProduct.objects.filter(
                created__gte=last_week,
                created__lt=current_week

            )
    last_week_total_product_damage = sum(item.damage_qty for item in damage_products)
    last_week_total_product_damage_amount = sum(item.product.cost_price for item in damage_products)
    customers = Customer.objects.filter(
                created__gte = last_week,
                created__lt=current_week

            )
    last_week_total_customer_added = customers.count()

    purchase_invoices = PurchaseInvoice.objects.filter(
                created__gte=last_week,
                created__lt=current_week,
                is_complete=True,
            )
    last_week_total_purchase_amount = sum(invoice.get_total_purchase_amount for invoice in purchase_invoices)
    
    expenses = Expense.objects.filter(
                created__gte=last_week,
                created__lt=current_week,
            )
    last_week_total_expense = sum(expense.amount for expense in expenses)

    supplier_payments_obj = SupplierPayment.objects.filter(
                created__gte=last_week,
                created__lt=current_week,
            )
    last_week_total_supplier_payment = sum(item.amount for item in supplier_payments_obj)


    last_week_gross_profit = last_week_total_sale_amount_with_discount - last_week_sale_wise_total_purchase_amount
    
    
    #Getting last week's total net profit
    last_week_total_payable = sum(item.get_total_payable for item in sales)
    last_week_total_damage_amount = sum(item.get_total_damage_amount for item in damage_products)
    last_week_total_net_profit = last_week_total_payable - (last_week_sale_wise_total_purchase_amount + last_week_total_expense + last_week_total_damage_amount)
 

    # Last week end
    '''
    This week  vs Last week - end
    '''










    '''
    This Month vs Last Month start
    '''
    # This Month start
    sales = Sale.objects.filter(
                delivery_date__gte=current_month,
                delivery_date__lt=next_month, 
                is_complete=True
            )
    this_month_total_sale_amount_with_discount = sum(sale.get_total_sold_price for sale in sales)
    this_month_total_discount = sum(sale.get_discount for sale in sales)
    this_month_total_other_fee = sum(sale.other_fee for sale in sales)
    this_month_total_sales =  sales.count()


    # Getting salewise total purchase price from product not from purchase product list
    this_month_sale_wise_total_purchase_amount = 0
    try:
        for sale in sales:
            sale_items = sale.saleitem_set.all()
            sale_wise_purchase_price_from_product = sum(item.product.cost_price for item in sale_items)
            this_month_sale_wise_total_purchase_amount = this_month_sale_wise_total_purchase_amount + sale_wise_purchase_price_from_product
    except:
        pass   

    damage_products = DamageProduct.objects.filter(
                created__gte=current_month,
                created__lt=next_month

            )
    this_month_total_product_damage = sum(item.damage_qty for item in damage_products)
    this_month_total_product_damage_amount = sum(item.product.cost_price for item in damage_products)
    customers = Customer.objects.filter(
                created__gte = current_month,
                created__lt=next_month

            )
    this_month_total_customer_added = customers.count()

    purchase_invoices = PurchaseInvoice.objects.filter(
                created__gte=current_month,
                created__lt=next_month,
                is_complete=True,
            )
    this_month_total_purchase_amount = sum(invoice.get_total_purchase_amount for invoice in purchase_invoices)
    
    expenses = Expense.objects.filter(
                created__gte=current_month,
                created__lt=next_month,
            )
    this_month_total_expense = sum(expense.amount for expense in expenses)

    supplier_payments_obj = SupplierPayment.objects.filter(
                created__gte=current_month,
                created__lt=next_month,
            )
    this_month_total_supplier_payment = sum(item.amount for item in supplier_payments_obj)


    this_month_gross_profit = this_month_total_sale_amount_with_discount - this_month_sale_wise_total_purchase_amount
    

    
    #Getting this month's total net profit
    this_month_total_payable = sum(item.get_total_payable for item in sales)
    this_month_total_damage_amount = sum(item.get_total_damage_amount for item in damage_products)
    this_month_total_net_profit = this_month_total_payable - (this_month_sale_wise_total_purchase_amount + this_month_total_expense + this_month_total_damage_amount)
 
    # This month end 
    #-----0-----
    

    # Last month start

    sales = Sale.objects.filter(
                delivery_date__gte=last_month,
                delivery_date__lt=current_month, 
                is_complete=True
            )
    last_month_total_sale_amount_with_discount = sum(sale.get_total_sold_price for sale in sales)
    last_month_total_discount = sum(sale.get_discount for sale in sales)
    last_month_total_other_fee = sum(sale.other_fee for sale in sales)
    last_month_total_sales =  sales.count()



    # Getting salewise total purchase price from product not from purchase product list
    last_month_sale_wise_total_purchase_amount = 0
    try:
        for sale in sales:
            sale_items = sale.saleitem_set.all()
            sale_wise_purchase_price_from_product = sum(item.product.cost_price for item in sale_items)
            last_month_sale_wise_total_purchase_amount = last_month_sale_wise_total_purchase_amount + sale_wise_purchase_price_from_product
    except:
        pass

    damage_products = DamageProduct.objects.filter(
                created__gte=last_month,
                created__lt=current_month
            )


    last_month_total_product_damage = sum(item.damage_qty for item in damage_products)
    last_month_total_product_damage_amount = sum(item.product.cost_price for item in damage_products)
    customers = Customer.objects.filter(
                created__gte = last_month,
                created__lt=current_month

            )
    last_month_total_customer_added = customers.count()

    purchase_invoices = PurchaseInvoice.objects.filter(
                created__gte=last_month,
                created__lt=current_month,
                is_complete=True,
            )
    last_month_total_purchase_amount = sum(invoice.get_total_purchase_amount for invoice in purchase_invoices)
    
    expenses = Expense.objects.filter(
                created__gte=last_month,
                created__lt=current_month,
            )
    last_month_total_expense = sum(expense.amount for expense in expenses)

    supplier_payments_obj = SupplierPayment.objects.filter(
                created__gte=last_month,
                created__lt=current_month,
            )
    last_month_total_supplier_payment = sum(item.amount for item in supplier_payments_obj)


    last_month_gross_profit = last_month_total_sale_amount_with_discount - last_month_sale_wise_total_purchase_amount
    
    
    #Getting this week's total net profit
    last_month_total_payable = sum(item.get_total_payable for item in sales)
    last_month_total_damage_amount = sum(item.get_total_damage_amount for item in damage_products)
    last_month_total_net_profit = last_month_total_payable - (last_month_sale_wise_total_purchase_amount + last_month_total_expense + last_month_total_damage_amount)
 

    # last month end
    '''
    This Month vs Last Month end
    '''








    '''
    This Year vs Last Year start
    '''
    # This Month start
    sales = Sale.objects.filter(
                delivery_date__gte=current_year,
                delivery_date__lt=next_year, 
                is_complete=True
            )
    this_year_total_sale_amount_with_discount = sum(sale.get_total_sold_price for sale in sales)
    this_year_total_discount = sum(sale.get_discount for sale in sales)
    this_year_total_other_fee = sum(sale.other_fee for sale in sales)
    this_year_total_sales =  sales.count()



    # Getting salewise total purchase price from product not from purchase product list
    this_year_sale_wise_total_purchase_amount = 0
    try:
        for sale in sales:
            sale_items = sale.saleitem_set.all()
            sale_wise_purchase_price_from_product = sum(item.product.cost_price for item in sale_items)
            this_year_sale_wise_total_purchase_amount = this_year_sale_wise_total_purchase_amount + sale_wise_purchase_price_from_product
    except:
        pass


    damage_products = DamageProduct.objects.filter(
                created__gte=current_year,
                created__lt=next_year

            )
    this_year_total_product_damage = sum(item.damage_qty for item in damage_products)
    this_year_total_product_damage_amount = sum(item.product.cost_price for item in damage_products)
    customers = Customer.objects.filter(
                created__gte = current_year,
                created__lt=next_year

            )
    this_year_total_customer_added = customers.count()

    purchase_invoices = PurchaseInvoice.objects.filter(
                created__gte=current_year,
                created__lt=next_year,
                is_complete=True,
            )
    this_year_total_purchase_amount = sum(invoice.get_total_purchase_amount for invoice in purchase_invoices)
    
    expenses = Expense.objects.filter(
                created__gte=current_year,
                created__lt=next_year,
            )
    this_year_total_expense = sum(expense.amount for expense in expenses)

    supplier_payments_obj = SupplierPayment.objects.filter(
                created__gte=current_year,
                created__lt=next_year,
            )
    this_year_total_supplier_payment = sum(item.amount for item in supplier_payments_obj)


    this_year_gross_profit = this_year_total_sale_amount_with_discount - this_year_sale_wise_total_purchase_amount
    
    
    #Getting this week's total net profit
    this_year_total_payable = sum(item.get_total_payable for item in sales)
    this_year_total_damage_amount = sum(item.get_total_damage_amount for item in damage_products)
    this_year_total_net_profit = this_year_total_payable - (this_year_sale_wise_total_purchase_amount + this_year_total_expense + this_year_total_damage_amount)
 


    # This Year end 
    #-----0-----
    



    # Last Year start

    sales = Sale.objects.filter(
                delivery_date__gte=last_year,
                delivery_date__lt=current_year, 
                is_complete=True
            )
    last_year_total_sale_amount_with_discount = sum(sale.get_total_sold_price for sale in sales)
    last_year_total_discount = sum(sale.get_discount for sale in sales)
    last_year_total_other_fee = sum(sale.other_fee for sale in sales)
    last_year_total_sales =  sales.count()



    # Getting salewise total purchase price from product not from purchase product list
    last_year_sale_wise_total_purchase_amount = 0
   
    for sale in sales:
        sale_items = sale.saleitem_set.all()  
        sale_wise_purchase_price_from_product = sum(item.product.cost_price for item in sale_items)
        last_year_sale_wise_total_purchase_amount = last_year_sale_wise_total_purchase_amount + sale_wise_purchase_price_from_product



    damage_products = DamageProduct.objects.filter(
                created__gte=last_year,
                created__lt=current_year

            )
    last_year_total_product_damage = sum(item.damage_qty for item in damage_products)
    last_year_total_product_damage_amount = sum(item.product.cost_price for item in damage_products)
    customers = Customer.objects.filter(
                created__gte = last_year,
                created__lt=current_year

            )
    last_year_total_customer_added = customers.count()

    purchase_invoices = PurchaseInvoice.objects.filter(
                created__gte=last_year,
                created__lt=current_year,
                is_complete=True,
            )
    last_year_total_purchase_amount = sum(invoice.get_total_purchase_amount for invoice in purchase_invoices)
    
    expenses = Expense.objects.filter(
                created__gte=last_year,
                created__lt=current_year,
            )
    last_year_total_expense = sum(expense.amount for expense in expenses)

    supplier_payments_obj = SupplierPayment.objects.filter(
                created__gte=last_year,
                created__lt=current_year,
            )
    last_year_total_supplier_payment = sum(item.amount for item in supplier_payments_obj)


    last_year_gross_profit = last_year_total_sale_amount_with_discount - last_year_sale_wise_total_purchase_amount
    
    
    #Getting this week's total net profit
    last_year_total_payable = sum(item.get_total_payable for item in sales)
    last_year_total_damage_amount = sum(item.get_total_damage_amount for item in damage_products)
    last_year_total_net_profit = last_year_total_payable - (last_year_sale_wise_total_purchase_amount + last_year_total_expense + last_year_total_damage_amount)
 

    # last Year end
    '''
    This Year vs Last Year end
    '''




    context = {
        'title': title,
        #Today start
        'today_sale_wise_total_purchase_amount': today_sale_wise_total_purchase_amount,
        'today_total_purchase_amount': today_total_purchase_amount,
        'today_total_sale_amount_with_discount': today_total_sale_amount_with_discount,
        'today_total_discount': today_total_discount,
        'today_total_other_fee': today_total_other_fee,
        'today_total_sales': today_total_sales,
        'today_total_product_damage': today_total_product_damage,
        'today_total_product_damage_amount': today_total_product_damage_amount,
        'today_total_customer_added': today_total_customer_added,
        'today_total_expense': today_total_expense,
        'today_total_supplier_payment': today_total_supplier_payment,
        'today_gross_profit': today_gross_profit,
        'today_total_net_profit': today_total_net_profit,
        #Today end
        #---------------------------------------------------
        #yesterday start
        'yesterday_sale_wise_total_purchase_amount': yesterday_sale_wise_total_purchase_amount,
        'yesterday_total_purchase_amount': yesterday_total_purchase_amount,
        'yesterday_total_sale_amount_with_discount': yesterday_total_sale_amount_with_discount,
        'yesterday_total_discount': yesterday_total_discount,
        'yesterday_total_other_fee': yesterday_total_other_fee,
        'yesterday_total_sales': yesterday_total_sales,
        'yesterday_total_product_damage': yesterday_total_product_damage,
        'yesterday_total_product_damage_amount': yesterday_total_product_damage_amount,
        'yesterday_total_customer_added': yesterday_total_customer_added,
        'yesterday_total_expense': yesterday_total_expense,
        'yesterday_total_supplier_payment': yesterday_total_supplier_payment,
        'yesterday_gross_profit': yesterday_gross_profit,
        'yesterday_total_net_profit': yesterday_total_net_profit,
        #yesterday end
        #---------------------------------------------------
        #---------------------------------------------------
        #Last week start
        'last_week_sale_wise_total_purchase_amount': last_week_sale_wise_total_purchase_amount,
        'last_week_total_purchase_amount': last_week_total_purchase_amount,
        'last_week_total_sale_amount_with_discount': last_week_total_sale_amount_with_discount,
        'last_week_total_discount': last_week_total_discount,
        'last_week_total_other_fee': last_week_total_other_fee,
        'last_week_total_sales': last_week_total_sales,
        'last_week_total_product_damage': last_week_total_product_damage,
        'last_week_total_product_damage_amount': last_week_total_product_damage_amount,
        'last_week_total_customer_added': last_week_total_customer_added,
        'last_week_total_expense': last_week_total_expense,
        'last_week_total_supplier_payment': last_week_total_supplier_payment,
        'last_week_gross_profit': last_week_gross_profit,
        'last_week_total_net_profit': last_week_total_net_profit,
        #Last week end
        #---------------------------------------------------    
        #This week start
        'this_week_sale_wise_total_purchase_amount': this_week_sale_wise_total_purchase_amount,
        'this_week_total_purchase_amount': this_week_total_purchase_amount,
        'this_week_total_sale_amount_with_discount': this_week_total_sale_amount_with_discount,
        'this_week_total_discount': this_week_total_discount,
        'this_week_total_other_fee': this_week_total_other_fee,
        'this_week_total_sales': this_week_total_sales,
        'this_week_total_product_damage': this_week_total_product_damage,
        'this_week_total_product_damage_amount': this_week_total_product_damage_amount,
        'this_week_total_customer_added': this_week_total_customer_added,
        'this_week_total_expense': this_week_total_expense,
        'this_week_total_supplier_payment': this_week_total_supplier_payment,
        'this_week_gross_profit': this_week_gross_profit,
        'this_week_total_net_profit': this_week_total_net_profit,
        #This week end
    
        #---------------------------------------------------
        #---------------------------------------------------
        
        #Last month start
        'last_month_sale_wise_total_purchase_amount': last_month_sale_wise_total_purchase_amount,
        'last_month_total_purchase_amount': last_month_total_purchase_amount,
        'last_month_total_sale_amount_with_discount': last_month_total_sale_amount_with_discount,
        'last_month_total_discount': last_month_total_discount,
        'last_month_total_other_fee': last_month_total_other_fee,
        'last_month_total_sales': last_month_total_sales,
        'last_month_total_product_damage': last_month_total_product_damage,
        'last_month_total_product_damage_amount': last_month_total_product_damage_amount,
        'last_month_total_customer_added': last_month_total_customer_added,
        'last_month_total_expense': today_total_expense,
        'last_month_total_supplier_payment': last_month_total_supplier_payment,
        'last_month_gross_profit': last_month_gross_profit,
        'last_month_total_net_profit': last_month_total_net_profit,
        #Last month end
        #---------------------------------------------------
        #This month start
        'this_month_sale_wise_total_purchase_amount': this_month_sale_wise_total_purchase_amount,
        'this_month_total_purchase_amount': this_month_total_purchase_amount,
        'this_month_total_sale_amount_with_discount': this_month_total_sale_amount_with_discount,
        'this_month_total_discount': this_month_total_discount,
        'this_month_total_other_fee': this_month_total_other_fee,
        'this_month_total_sales': this_month_total_sales,
        'this_month_total_product_damage': this_month_total_product_damage,
        'this_month_total_product_damage_amount': this_month_total_product_damage_amount,
        'this_month_total_customer_added': this_month_total_customer_added,
        'this_month_total_expense': this_month_total_expense,
        'this_month_total_supplier_payment': this_month_total_supplier_payment,
        'this_month_gross_profit': this_month_gross_profit,
        'this_month_total_net_profit': this_month_total_net_profit,
        #This month end
        #---------------------------------------------------
        #---------------------------------------------------
        #Last year start
        'last_year_sale_wise_total_purchase_amount': last_year_sale_wise_total_purchase_amount,
        'last_year_total_purchase_amount': last_year_total_purchase_amount,
        'last_year_total_sale_amount_with_discount': last_year_total_sale_amount_with_discount,
        'last_year_total_discount': last_year_total_discount,
        'last_year_total_other_fee': last_year_total_other_fee,
        'last_year_total_sales': last_year_total_sales,
        'last_year_total_product_damage': last_year_total_product_damage,
        'last_year_total_product_damage_amount': last_year_total_product_damage_amount,
        'last_year_total_customer_added': last_year_total_customer_added,
        'last_year_total_expense': last_year_total_expense,
        'last_year_total_supplier_payment': last_year_total_supplier_payment,
        'last_year_gross_profit': last_year_gross_profit,
        'last_year_total_net_profit': last_year_total_net_profit,
        #Last year end
        #---------------------------------------------------
        #This year start
        'this_year_sale_wise_total_purchase_amount': this_year_sale_wise_total_purchase_amount,
        'this_year_total_purchase_amount': this_year_total_purchase_amount,
        'this_year_total_sale_amount_with_discount': this_year_total_sale_amount_with_discount,
        'this_year_total_discount': this_year_total_discount,
        'this_year_total_other_fee': this_year_total_other_fee,
        'this_year_total_sales': this_year_total_sales,
        'this_year_total_product_damage': this_year_total_product_damage,
        'this_year_total_product_damage_amount': this_year_total_product_damage_amount,
        'this_year_total_customer_added': this_year_total_customer_added,
        'this_year_total_expense': this_year_total_expense,
        'this_year_total_supplier_payment': this_year_total_supplier_payment,
        'this_year_gross_profit': this_year_gross_profit,
        'this_year_total_net_profit': this_year_total_net_profit,
        #This year end

    
    }


    return render(request, 'products/dashboard.html', context)




@login_required()
@user_passes_test(lambda u: u.is_active or u.is_salesman or u.is_manager or u.is_owner or u.is_superuser)
def salesOrder(request):
    products = Product.objects.all()
    sales = Sale.objects.all()
    customerForm = CustomerModelForm()
    context = {
        'title': 'Sales-orders',
        'products': products,
        'sales': sales,
        'customerForm': customerForm,
    }
    return render(request, 'products/sales-order.html', context)


@login_required()
@user_passes_test(lambda u: u.is_active or u.is_salesman or u.is_manager or u.is_owner or u.is_superuser)
def customerList(request):
    return render(request, 'products/customer/list-style-customer.html')


@login_required()
@user_passes_test(lambda u: u.is_active or u.is_salesman or u.is_manager or u.is_owner or u.is_superuser)
def customerProfile(request):
    return render(request, 'products/customer/customer-profile-view.html')


#Product Search
def search(request):
    if request.is_ajax():
        response = None
        searchTerm =request.POST.get('searchTerm')
        qs = (
            Product.objects.filter(name__icontains=searchTerm) |
            Product.objects.filter(product_code__icontains=searchTerm)
        )
        if len(qs) > 0 and len(searchTerm) > 0:
            data = []
            for product in qs:
                item = {
                    'id': product.id,
                    'name': product.name,
                    'product_pic': product.product_pic.url,
                    'selling_price': product.selling_price,
                    'get_selling_price_after_discount': product.get_selling_price_after_discount,
                    'get_discount': product.get_discount,
                    'get_product_slug': product.get_product_slug,
                    'product_code': product.product_code,
                }
                data.append(item)
            response = data
        else:
            response = 'No product found...'
    return JsonResponse(response, safe=False)



@login_required()
@user_passes_test(lambda u: u.is_active or u.is_salesman or u.is_manager or u.is_owner or u.is_superuser)
def product_category(request):
    form = CategoryModelForm()
    categories = Category.objects.all()
    context = {
        'title': 'Product Categories',
        'form': form,
        'categories': categories,

    }
    return render(request, 'products/category.html', context)


@login_required()
@user_passes_test(lambda u: u.is_active or u.is_salesman or u.is_manager or u.is_owner or u.is_superuser)
def category_details(request, pk):
    productModelForm = ProductModelForm()
    category = Category.objects.get(pk=pk)
    products = category.product_set.all()
    context = {
        'title': 'Products',
        'products': products,
        'category': category,
        'productModelForm': productModelForm,
        }
    return render(request, 'products/category-details.html', context)


@login_required()
@user_passes_test(lambda u: u.is_active or u.is_manager or u.is_owner or u.is_superuser)
def create_product_cat(request):
    if request.method == 'POST':
        form = CategoryModelForm(request.POST, request.FILES)
        if form.is_valid():
            instance = form.save(commit=False)
            instance.added_by = request.user
            instance.save()

            messages.success(request, 'Category has been successfully created !')
            return redirect(request.META['HTTP_REFERER'])
        else:
            messages.error(request, 'Oops! Something went wrong. Please create again. ')
            return redirect(request.META['HTTP_REFERER'])


@login_required()
@user_passes_test(lambda u: u.is_active or u.is_salesman or u.is_manager or u.is_owner or u.is_superuser)
def product(request):
    products = Product.objects.all()
    productModelForm = ProductModelForm()
    context = {
        'title': 'Products',
        'products': products,
        'productModelForm': productModelForm,
    }
    return render(request, 'products/product.html', context)


@login_required()
@user_passes_test(lambda u: u.is_active or u.is_salesman or u.is_manager or u.is_owner or u.is_superuser)
def create_product(request):
    if request.method == 'POST':
        form = ProductModelForm(request.POST, request.FILES)
        if form.is_valid():
            print('form is ready to save')
            instance = form.save(commit=False)
            instance.added_by = (request.user)
            instance.save()
            messages.success(request, 'Product has been successfully created !')
        else:
            messages.error(request, f'Oops! {form.errors.as_ul}. ')
    return redirect(request.META['HTTP_REFERER'])


@login_required()
@user_passes_test(lambda u: u.is_active or u.is_salesman or u.is_manager or u.is_owner or u.is_superuser)
def update_product(request, pk):
    product = Product.objects.get(pk=pk)
    form = ProductModelForm(instance=product)
    print(form)
    if request.method == 'POST':
        form = ProductModelForm(request.POST, request.FILES, instance=product)
        if form.is_valid():
            instance = form.save(commit=False)
            instance.updated_by = request.user
            instance.save()
            messages.success(request, 'Product has been successfully updated !')
            return redirect('products:product')
        else:
            messages.error(request, f'Oops! {form.errors.as_ul}. ')
    context = {
        'title': 'Update Product',
        'form': form,
    }
    return render(request, 'products/product-update-form.html', context)

@login_required()
@user_passes_test(lambda u: u.is_active or u.is_manager or u.is_owner or u.is_superuser)
def delete_product(request):
    if request.is_ajax:
        product_id = request.POST.get('product_id')
        Product.objects.get(id=product_id).delete() 
        data = True
    else:
        data = False

    return JsonResponse({'data': data})  






'''
This view works for adding product in the cart section
using scanner, click event and search. This is very important
'''
def find_product(request):
    if request.is_ajax():
        product_code = request.POST.get('product_code')
        product = Product.objects.get(product_code=product_code)

        inventory = Inventory.objects.get(product=product)
        remaining_product_qty = inventory.get_total_remaining_product
         
        product_price_after_discount = product.selling_price - product.get_discount
        product_in_stock = True

        try:
            product_qty = request.POST.get('product_qty')
            product_qty = int(product_qty)
            
            if remaining_product_qty < product_qty:
                product_in_stock = False
        except:
            product_qty = ''
        

    response = {
        'product_name': product.name.lower(),
        'product_price_after_discount': product_price_after_discount,
        'product_in_stock': product_in_stock,
        'product_code': product.product_code,
        'product_available': remaining_product_qty,
    }
    return JsonResponse(response, safe=False)



# Printing Barcode
@login_required()
@user_passes_test(lambda u: u.is_active or u.is_salesman or u.is_manager or u.is_owner or u.is_superuser)
def print_barcode(request):
    formset = False
    total_barcode_type = False
    try:
        total_barcode_type = int(request.GET.get('total_type'))
    except:
        pass

    print(total_barcode_type)
    if total_barcode_type:
        formset = formset_factory(BarcodeForm, extra=total_barcode_type)
    # if request.method == 'POST':
    if request.method == 'POST':
        formset = formset(request.POST)
        if formset.is_valid():
            product_name = request.POST.get('product_name')
            selling_price = request.POST.get('selling_price')
            selling_price_with_discount = request.POST.get('selling_price_with_discount')

            parent_product_list = []
            for form in formset:
                data = form.cleaned_data
                if data:
                    child_product_list = []
                    child_product_list = [data['product'] for _ in range(data['qty'])]
                    parent_product_list += child_product_list
            if parent_product_list:    
                context = {
                    'parent_product_list': parent_product_list,
                    'product_name': product_name,
                    'selling_price': selling_price,
                    'selling_price_with_discount': selling_price_with_discount,
                }
                return render(request, 'products/print-product-barcode.html', context)
            else:
                    messages.error(request, 'Oops! Something went wrong.')
                    return redirect(request.META['HTTP_REFERER'])
    
    context = {
        'formset': formset,
    }
    return render(request, 'products/generate-product-barcode.html', context)