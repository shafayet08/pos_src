$(document).ready(function(){
    
    //======== Callable functions are started =========
    //=================================================
    // 1. A function that handles toastr notification -> starts
    // this function has moved to a new file callde ' toastr_notification.js '
    // 1. A function that handles toastr notification -> ended


    // 2. A function that handels getting discount in parcentage -> starts
    function get_discount(value, parcentage){
        return (value * parcentage) / 100;
    };
    // 2. A function that handels getting discount in parcentage -> ended


    // 3. A callable function that handles down account section -> starts
    function cart_handle(){
        // For changing account section.
        var discount;
        var sub_total_ = parseInt($('#js-sub-total').text());
        var discount_value = parseInt($('#id_discount').val()) || 0;
        var take_amount = parseInt($('#id_paid_or_take').val()) || 0;
        var other_fee = parseInt($('#id_other_fee').val()) || 0;
        var total_payable = parseInt($('#id_total_payable').text());
        var check_discount = $('#id_dis_checkbox').is(":checked");
        
        // checking if the discount is in parcentage or not
        if(check_discount == true){
            discount = get_discount(sub_total_, discount_value);
        }else{
            discount = discount_value;
        };

        // Comparing sub total and total_payable amount
        if(sub_total_ == total_payable){
            total_payable = (sub_total_ - discount) + other_fee;
        }else{
            if(sub_total_ !== total_payable){
                total_payable = (sub_total_ - discount) + other_fee;
            };
        };
        var return_amount = take_amount - total_payable;
        
        // Comparing total_payable and take amount and it returns 'return amount and due amount
        if(total_payable > take_amount){
            var due_amount = total_payable - take_amount;
            return_amount = 0;
        }else{
            due_amount = 0;
        }
        
        $('#id_total_payable').text(total_payable);
        $('#id_return_amount').text(return_amount);
        $('#id_due_amount').text(due_amount);
    };
    // 3. A callable function that handles down account section -> ended
    
    // 4. A callable function that handels 'return amount and due amount' -> starts
    function return_and_due_amount(){
        var take_amount = parseInt($('#id_paid_or_take').val()) || 0;
        var total_payable = parseInt($('#id_total_payable').text());
        var return_amount = take_amount - total_payable;
        if(total_payable > take_amount){
            var due_amount = total_payable - take_amount;
            return_amount = 0;
        }else{
            due_amount = 0;
        }
        $('#id_return_amount').text(return_amount);
        $('#id_due_amount').text(due_amount);
    };
    // 4. A callable function that handels 'return amount and due amount' -> ended


    //========== Started Main work =========
    //======================================
    var product_prices = [];
    var products = [];
    var sub_total_list = [];
    // var product_codes = [];
    const keycodes = [0,1,2,3,4,5,6,7,8,9];
    const csrf = $('input[name=csrfmiddlewaretoken').val();


    // // Scanner
    // $('#psearch-input').keypress(function(e){
    //     if(e.which == 13){
    //         product_code = e.target.value;
    //         $.ajax({
    //             type: 'POST',
    //             url: '/find_product/',
    //             data: {
    //                 'csrfmiddlewaretoken': csrf,
    //                 'product_code': product_code,
    //             },
    //             success: function(response){
    //                 var product = response.product_name;
    //                 var selling_price = response.selling_price;
    //                 var discount_from_product = response.discount_from_product;
    //                 var product_price_after_discount = selling_price - discount_from_product;

                    
    //                 //appending product prices with new ones;
    //                 product_prices.push(product_price_after_discount);
    //                 //appending products with new ones;
    //                 products.push(product);
    //                 var qty = 0;
    //                 var total_price = 0;
    //                 var total_qty = 0;
    //                 var sub_total = 0;
            
    //                 // Looping through all product prices for total quntity
    //                 // and extending sub total list.
    //                 for(var i in product_prices){
    //                     if(product_price_after_discount == product_prices[i] && product == products[i]){
    //                         total_price = total_price + product_price_after_discount;
    //                         sub_total_list.push(total_price);
    //                     }
    //                     total_qty = total_qty + 1;
    //                 };
    //                 // Looping through all products for total quantity individually
    //                 for(var i=0; i < products.length; i++){
    //                     if(product == products[i] && product_price_after_discount == product_prices[i]){
    //                         qty++;
    //                     }
    //                 };
    //                 // Looping through all the product prices for sub total
    //                 for(var i in product_prices){
    //                     sub_total = sub_total + product_prices[i]
    //                 };
    //                 if(qty == 1){
    //                     var cart_item = `
    //                                         <tr>
    //                                             <td class="js-product">${product}</td>
    //                                             <td id="js-qty${product}" 
    //                                                 class="js-qty-editable" 
    //                                                 data-data_product_="${product}"
    //                                                 contenteditable="true">
    //                                                 ${qty}
    //                                             </td>
    //                                             <td id="js-price${product}" 
    //                                                 class="js-price-editable"
    //                                                 contenteditable="true"
    //                                                 data-price="${product_price_after_discount}">
    //                                                 ${product_price_after_discount}
    //                                             </td>
    //                                             <td id="js-total-price${product}">${total_price}</td>
    //                                             <td class="js-remove icon-center-pos" data-data_product="${product}">
    //                                                 <i class="fas fa-times">               
    //                                             </td>
    //                                         </tr> 
    //                                     `;
    //                     $('#js-cart-table #show-cart').append(cart_item);
    //                 };
    //                 if(qty > 1){
    //                     $('#show-cart #js-qty' + product.toString()).text(qty);
    //                     $('#show-cart #js-total-price' + product.toString()).text(total_price);
    //                 };
    //                 $('#js-total-qty').text(total_qty);
    //                 $('#js-sub-total').text(sub_total);
    //                 $('#id_total_payable').text(sub_total);
                    
    //                 //For changing account section. This cart_handle() is called from the above (fn no.-3)
    //                 cart_handle();


    //                 $('#show-cart .js-remove').click(function(){
    //                     var data_product = $(this).data('data_product');
    //                     var remove_price = parseInt($(this).siblings('#js-price' + data_product).text()); 
    //                     // Removing products and prices
    //                     lim1 = products.length
    //                     for(var i=0; i < lim1; i++){
    //                         for(var j=0; j < lim1; j++){
    //                             if(data_product == products[i] && remove_price == product_prices[i]){
    //                                 products.splice(i, 1);
    //                                 product_prices.splice(i, 1);
    //                             };  
    //                         };
    //                     };
            
    //                     total_qty = products.length;
    //                     sub_total = 0;
    //                     $.each(product_prices, function(index, value){
    //                         sub_total += value;
    //                     });
            
    //                     $('#js-total-qty').text(total_qty);
    //                     $('#js-sub-total').text(sub_total);
    //                     $('#id_total_payable').text(sub_total);
    //                     cart_handle(); //For changing account section. This cart_handle() function is called from the top (fn no.-3)
    //                     $(this).parent().remove();
    //                 }); // Inner event of 1st event ends;


    //             }, // success ended
    //         });
    //     };
    // }); // Scanner ended
    










    $('.product-view-area  .js-add-to-cart').click(function(){
        
        var product = $(this).data('product');
        var product_id = $(this).data('product_id');
        var selling_price = parseInt($(this).data('selling_price'));
        var discount_from_product = parseInt($(this).data('discount'));
        var product_price_after_discount = selling_price - discount_from_product;

        //appending product prices with new ones;
        product_prices.push(product_price_after_discount);
        //appending products with new ones;
        products.push(product);
        var qty = 0;
        var total_price = 0;
        var total_qty = 0;
        var sub_total = 0;

        // Looping through all product prices for total quntity
        // and extending sub total list.
        for(var i in product_prices){
            if(product_price_after_discount == product_prices[i] && product == products[i]){
                total_price = total_price + product_price_after_discount;
                // sub_total_list.push(total_price);
            }
            total_qty = total_qty + 1;
        };
        // Looping through all products for total quantity individually
        for(var i=0; i < products.length; i++){
            if(product == products[i] && product_price_after_discount == product_prices[i]){
                qty++;
            }
        };
        // Looping through all the product prices for sub total
        for(var i in product_prices){
            sub_total = sub_total + product_prices[i]
        };
        if(qty == 1){
            var cart_item = `
                                <tr>
                                    <td class="js-product">${product}</td>
                                    <td id="js-qty${product}" 
                                        class="js-qty-editable" 
                                        data-data_product_="${product}"
                                        contenteditable="true">
                                        ${qty}
                                    </td>
                                    <td id="js-price${product}" 
                                        class="js-price-editable"
                                        contenteditable="true"
                                        data-price="${product_price_after_discount}">
                                        ${product_price_after_discount}
                                    </td>
                                    <td id="js-total-price${product}">${total_price}</td>
                                    <td class="js-remove icon-center-pos" data-data_product="${product}">
                                        <i class="fas fa-times">               
                                    </td>
                                </tr> 
                            `;
            $('#js-cart-table #show-cart').append(cart_item);
        };
        if(qty > 1){
            $('#show-cart #js-qty' + product.toString()).text(qty);
            $('#show-cart #js-total-price' + product.toString()).text(total_price);
        };
        $('#js-total-qty').text(total_qty);
        $('#js-sub-total').text(sub_total);
        $('#id_total_payable').text(sub_total);
        
        //For changing account section. This cart_handle() is called from the above (fn no.-3)
        cart_handle();

        //-----------Removing item from cart starts------------
        // Inner event of 1st event starts;
        $('#show-cart .js-remove').click(function(){
            var data_product = $(this).data('data_product');
            var remove_price = parseInt($(this).siblings('#js-price' + data_product).text()); 
            // Removing products and prices
            lim1 = products.length
            for(var i=0; i < lim1; i++){
                for(var j=0; j < lim1; j++){
                    if(data_product == products[i] && remove_price == product_prices[i]){
                        products.splice(i, 1);
                        product_prices.splice(i, 1);
                    };  
                };
            };

            total_qty = products.length;
            sub_total = 0;
            $.each(product_prices, function(index, value){
                sub_total += value;
            });

            $('#js-total-qty').text(total_qty);
            $('#js-sub-total').text(sub_total);
            $('#id_total_payable').text(sub_total);
            cart_handle(); //For changing account section. This cart_handle() function is called from the top (fn no.-3)
            $(this).parent().remove();
        }); // Inner event of 1st event ends;
        //-----------Removing item from cart ends------------

        
        //-----------Editable quantity in the cart starts------------
        // Inner 2nd event starts
        $('#show-cart .js-qty-editable').click(function(){
            $(this).css({'outline': '2px solid red', 'background': '#fbeec6'});

            var after_changing_qty;
            $(this).keypress(function(e){
                if(!(e.key in keycodes)){
                    e.preventDefault(); // Preventing characters without numbers
                };
                e.stopImmediatePropagation() // Preventing continuous loop
                if(e.which == 13){
                    e.preventDefault();
                    after_changing_qty = $(this).text();
                    $(this).css({'outline': '', 'background': ''});
                    $(this).blur();
                    
                    if(after_changing_qty == ''){
                        // alert_('error', '<strong>Quantity cannot be empty.Please provide quantity.</strong>');
                        $('.alert').show() // undone- add alert message
                        $(this).css({'outline': '2px solid red', 'background': '#fbeec6'});
                        $(this).focus();
                    };
                    if(after_changing_qty == 0){
                        // alert_('error', '<strong>Quantity cannot be 0.Please provide quantity.</strong>');
                        $('.alert').show() // undone- add alert message
                        $(this).css({'outline': '2px solid red', 'background': '#fbeec6'});
                        $(this).focus();
                    };
                    after_changing_qty = parseInt(after_changing_qty);
                    if(after_changing_qty != '' && after_changing_qty != 0){
                        
                        $('.alert').hide();
                        $(this).blur();
                        remove_product = $(this).data('data_product_');
                        price = parseInt($(this).siblings('#js-price' + remove_product).text());
        
                        var lim1 = parseInt(products.length)
                        for(var i=0; i < lim1; i++){
                            for(var j=0; j < lim1; j++){
                                if(remove_product == products[i] && price == product_prices[i]){
                                    products.splice(i, 1);
                                    product_prices.splice(i, 1);
                                };  
                            };
                        };

                        total_price = 0;
                        // console.log(typeof(product_prices[1]))
                        for(var i=0; i < after_changing_qty; i++){
                            product_prices.push(price);
                            products.push(remove_product);
                            total_price = total_price + price;
                        };

                        total_qty = product_prices.length;
                        sub_total = 0;
                        $.each(product_prices, function(index, value){
                            sub_total += value;
                        });
                        $('#js-total-price' + remove_product).text(total_price);
                        $('#js-total-qty').text(total_qty);
                        $('#js-sub-total').text(sub_total);
                        $('#id_total_payable').text(sub_total);
                        cart_handle(); //For changing account section. This cart_handle() function is called from the top (fn no.-3)
                    };
                    console.log(e.isImmediatePropagationStopped());
                };   
            }); // ends
            // For blur
            $(this).blur(function(){
                after_changing_qty_ = $(this).text();
                console.log(after_changing_qty_);
                if(after_changing_qty_ == '' || after_changing_qty_ == 0){
                    // alert_('error', '<strong>Please provide quantity.</strong>');
                    $('.alert').show() // undone- add alert message
                    $(this).css({'outline': '2px solid red', 'background': '#fbeec6'});
                    $(this).focus();
                }; 
                if(after_changing_qty_ != '' && after_changing_qty_ != 0){
                    $(this).css({'outline': '', 'background': ''});
                };  
            });// ends
        }); // Inner 2nd event ends
        //-----------Editable quantity in the cart ends------------


        
        //-----------Editable price in the cart starts------------
        $('#show-cart .js-price-editable').click(function(){
            $(this).css({'outline': '2px solid red', 'background': '#fbeec6'});
            // const after_changing_price;

            $(this).keypress(function(e){
                if(!(e.key in keycodes)){
                    e.preventDefault();
                };
                e.stopImmediatePropagation() // Preventing continuous loop
                if(e.which == 13){
                    const real_price = parseInt($(this).data('price'));
                    const after_changing_price = parseInt($(this).text()) || 0; // NaN value will replace with 0;
                    const remove_product_ = $(this).prev().data('data_product_');
                    const current_qty = parseInt($(this).prev().text());

                    $(this).css({'outline': '', 'background': ''});
                    $(this).blur()
                    if(after_changing_price == '' || isNaN(after_changing_price)){
                        // alert_('error', '<strong>Quantity cannot be empty.Please provide quantity.</strong>');
                        $('.alert').show() // undone- add alert message
                        $(this).css({'outline': '2px solid red', 'background': '#fbeec6'});
                        $(this).focus();
                    };
                    if(after_changing_price == 0){
                        // alert_('error', '<strong>Quantity cannot be 0.Please provide quantity.</strong>');
                        $('.alert').show() // undone- add alert message
                        $(this).css({'outline': '2px solid red', 'background': '#fbeec6'});
                        $(this).focus();
                    };
                    if(after_changing_price != '' && after_changing_price != 0){
                        $('.alert').hide();
                        $(this).blur();
                        lim1 = products.length;
                        for(var i=0; i < lim1; i++){
                            for(var j=0; j < lim1; j++){
                                if(remove_product_ == products[i]){
                                    products.splice(i, 1);
                                    product_prices.splice(i, 1);
                                };  
                            };
                        };

                        total_price = 0;
                        for(var i=0; i < current_qty; i++){
                            product_prices.push(after_changing_price);
                            products.push(remove_product_);
                            total_price = total_price + after_changing_price;
                        };
                        sub_total = 0;
                        $.each(product_prices, function(index, value){
                            sub_total += value;
                        });

                        $('#js-total-price' + remove_product_).text(total_price);
                        $('#js-sub-total').text(sub_total);
                        $('#id_total_payable').text(sub_total);
                        cart_handle(); //For changing account section. This cart_handle() function is called from the top (fn no.-3)
                    };
                };
            });
            $(this).blur(function(){
                after_changing_price = $(this).text();
                if(after_changing_price == '' || after_changing_price == 0){
                    // alert_('error', '<strong>Please provide quantity.</strong>');
                    $('.alert').show() // undone- add alert message
                    $(this).css({'outline': '2px solid red', 'background': '#fbeec6'});
                    $(this).focus();
                }; 
                if(after_changing_price != '' && after_changing_price != 0){
                    $(this).css({'outline': '', 'background': ''});
                };  
            });// ends
        });// ends

        //-----------Editable price in the cart ends------------

        
    }); // 1st event ends;


    $('#id_other_fee').keypress(function(e){
        var other_fee;
        var sub_total_;
        var total_payable;

        if(!(e.key in keycodes)){
            e.preventDefault();
        }
        if(e.which == 13 || e.which == 9){
            $(this).blur()
            var discount;
            other_fee = parseInt($(this).val()) || 0;
            var discount_value = parseInt($('#id_discount').val()) || 0;
            var check_discount = $('#id_dis_checkbox').is(":checked");
            sub_total_ = parseInt($('#js-sub-total').text());
            total_payable = parseInt($('#id_total_payable').text());

            if(check_discount == true){
                discount = get_discount(sub_total_, discount_value);
            }else{
                discount = discount_value;
            };

            if(sub_total_ == total_payable){
                total_payable = (sub_total_ - discount) + other_fee;
            }else{
                if(sub_total_ !== total_payable){
                    total_payable = (sub_total_ - discount) + other_fee; 
                };
            };
            $('#id_total_payable').text(total_payable);
            return_and_due_amount() // Calling function no.-4
        };
        $(this).blur(function(){
            var discount;
            other_fee = parseInt($(this).val()) || 0;
            var discount_value = parseInt($('#id_discount').val()) || 0;
            var check_discount = $('#id_dis_checkbox').is(":checked");
            sub_total_ = parseInt($('#js-sub-total').text());
            total_payable = parseInt($('#id_total_payable').text());

            if(check_discount == true){
                discount = get_discount(sub_total_, discount_value);
            }else{
                discount = discount_value;
            };

            if(sub_total_ == total_payable){
                total_payable = (sub_total_ - discount) + other_fee;
            }else{
                if(sub_total_ !== total_payable){
                    total_payable = (sub_total_ - discount) + other_fee; 
                };
            };
            $('#id_total_payable').text(total_payable);
            return_and_due_amount() // Calling function no.-4
        }); 
    });


    $('#id_discount').keypress(function(e){
        var check_discount;
        var discount_value;
        var sub_total__;
        var total_payable_;
        var other_fee_;
        if(!(e.key in keycodes)){
            e.preventDefault();
        }
        if(e.which == 13 || e.which == 9){
            $(this).blur();
            discount_value = parseInt($(this).val()) || 0;
            check_discount = $('#id_dis_checkbox').is(":checked");
            sub_total__ = parseInt($('#js-sub-total').text());
            total_payable_ = parseInt($('#id_total_payable').text());
            other_fee_ = parseInt($('#id_other_fee').val()) || 0;
            if(check_discount == true){
                total_payable_ = (sub_total__ - get_discount(sub_total__, discount_value)) + other_fee_;
            }else{
                total_payable_ = (sub_total__ - discount_value) + other_fee_;
            }
            
            $('#id_total_payable').text(total_payable_);
            return_and_due_amount() // Calling function no.-4
        };

        $(this).blur(function(){
            discount_value = parseInt($(this).val()) || 0;
            check_discount = $('#id_dis_checkbox').is(":checked");
            sub_total__ = parseInt($('#js-sub-total').text());
            total_payable_ = parseInt($('#id_total_payable').text());
            other_fee_ = parseInt($('#id_other_fee').val()) || 0;
            if(check_discount == true){
                total_payable_ = (sub_total__ - get_discount(sub_total__, discount_value)) + other_fee_;
            }else{
                total_payable_ = (sub_total__ - discount_value) + other_fee_;
            };
            
            $('#id_total_payable').text(total_payable_);
            return_and_due_amount() // Calling function no.-4
        });

    });

    $('#id_paid_or_take').keypress(function(e){
        if(!(e.key in keycodes)){
            e.preventDefault();
        };
        if(e.which == 13 || e.which == 9){
            $(this).blur();
            var take_amount = parseInt($(this).val()) || 0;
            var total_payable = parseInt($('#id_total_payable').text());
            var return_amount = take_amount - total_payable;
            if(total_payable > take_amount){
                var due_amount = total_payable - take_amount;
                return_amount = 0;
            }else{
                due_amount = 0;
            }
            $('#id_return_amount').text(return_amount);
            $('#id_due_amount').text(due_amount);

        };
        $(this).blur(function(){
            var take_amount = parseInt($(this).val()) || 0;
            var total_payable = parseInt($('#id_total_payable').text());
            var return_amount = take_amount - total_payable;
            if(total_payable > take_amount){
                var due_amount = total_payable - take_amount;
                return_amount = 0;
            }else{
                due_amount = 0;
            }
            $('#id_return_amount').text(return_amount);
            $('#id_due_amount').text(due_amount);
        });
    });
});