$(document).ready(function(){
    $('.js-create-product').click(function(){
        $('#show-product-modal').dialog({
            title: 'Create New Product',
            width: 600,
            height: 570,
            modal: true,
            resizable: false,
            draggable: true,
    
        });
        console.log('I am from expense click event')
    });

        // # Deleting Sale 
        $('.js-delete-product').click(function(){
            var product_id = $(this).data('product_id')
            const csrf = $('input[name=csrfmiddlewaretoken').val(); 
            $('#product-delete-modal-show').dialog({
                title: 'Do you want to delete?',
                modal: true,
                resizable: false,
                draggable: true,
        
            });
            $('#product_delete_no').click(function(e){
                e.stopImmediatePropagation()
                $('#product-delete-modal-show').dialog('close'); 
            })
            $('#product_delete_yes').click(function(){
                $.ajax({
                    type: 'POST',
                    url: '/delete-product/',
                    dataType: 'json',
                    data: {
                        'csrfmiddlewaretoken': csrf,
                        'product_id': product_id,
                    },
                    success: function(response){
                        if(response.data == true){
                            $('#product-delete-modal-show').dialog('close'); 
                            Toast.fire({
                                icon: 'success',
                                title: 'Success! The product successfully deleted.'
                            });
                            setTimeout(function(){
                                location.reload()
                            },2000);
                            
                            
                        }else{
                            $('#product-delete-modal-show').dialog('close'); 
                            Toast.fire({
                                icon: 'error',
                                title: 'error! Somthing went wrong.'
                            });
                        }
                    } 
                });
    
            });
        });// # Deleting Sale ended    
});
