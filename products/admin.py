from django.contrib import admin

from . models import Category, Product

from import_export.admin import ImportExportModelAdmin


# admin.site.register(Product)

@admin.register(Product)
class ViewAdmin(ImportExportModelAdmin):
    pass
    
admin.site.register(Category)
