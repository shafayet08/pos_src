from import_export import resources
from products.models import Product

class ProductResource(resources.ModelResource):
    class Meta:
        model = Product
