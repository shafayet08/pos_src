from django.core.files import File
from django.db.models.signals import post_save, pre_save
from django.dispatch import receiver
from django.core.files.base import ContentFile


from PIL import Image
import barcode
from barcode.writer import ImageWriter
from io import BytesIO
from random import randint

from products.models import Product
from inventories.models import Inventory



# Callable function to create product code and barcode start 
def generateProductCode(number):
    starting_range = 10**(number-1)
    ending_range = (10**number)-1
    return randint(starting_range, ending_range)




@receiver(pre_save, sender=Product)
def generate_product_thumbnail(sender, instance, **kwargs):
    product_pic = Image.open(instance.product_pic)
    product_pic = product_pic.convert("RGB")

    if product_pic.height > 300 or product_pic.width > 300:
        THUMBNAIL_SIZE = (300, 300)
        product_pic.thumbnail(THUMBNAIL_SIZE)
        temp_thumb = BytesIO()
        product_pic.save(temp_thumb, 'JPEG')
        temp_thumb.seek(0)

        instance.product_pic.save(
            instance.product_pic.name,
            ContentFile(temp_thumb.read()),
            save=False,
        )
        temp_thumb.close()


@receiver(post_save, sender=Product)
def createInventory(sender, instance, created, **kwargs):
    if created:
        Inventory.objects.create(product=instance)













# @receiver(post_save, sender=Product)
# def createProductCode(sender, instance, created, **kwargs):
#     options = {
#         'module_width': 0.1,
#         'module_height': 1.2,
#         'text_distance': 0.3,
#         'font_size': 5,
#         'quiet_zone': 0.5,
#     }
#     def generateProductCode(number):
#         starting_range = 10**(number-1)
#         ending_range = (10**number)-1
#         return randint(starting_range, ending_range)
    
#     '''
#     A checking process whether the Product table is empthy or not
#     '''
#     if Product.objects.all().count() != 0: 
#         products = Product.objects.all()

#         product_codes = []
#         for i in products:
#             if i.product_code:
#                 product_codes.append(i.product_code) 

        
#         generated_product_code = 0
#         if not instance.product_code:
#             while True:
#                 generated_product_code = (generateProductCode(12))
#                 if not generated_product_code in product_codes:
#                     instance.product_code = generated_product_code
#                     instance.save()
#                     break
                 
#         if generated_product_code != 0:
#             print(generated_product_code)
#             THUMBNAIL_SIZE = (300, 300)
#             ean = barcode.get('code128', str(generated_product_code), writer=ImageWriter())
#             ean.save('media/barcode/' + instance.name, options)
#             instance.barcode = ('barcode/'+instance.name + ".png")
#             instance.save()



# @receiver(post_save, sender=Product)
# def createProductCode(sender, instance, created, **kwargs):
#     def generateProductCode(number):
#         starting_range = 10**(number-1)
#         ending_range = (10**number)-1
#         return randint(starting_range, ending_range)
    
#     '''
#     A checking process whether the Product table is empthy or not
#     '''
#     if Product.objects.all().count() != 0: 
#         products = Product.objects.all()

#         product_codes = []
#         for i in products:
#             if i.product_code:
#                 product_codes.append(i.product_code) 

#         generated_product_code = int('1' + str(generateProductCode(12)))
        
#         if instance.product_code == "":
#             if created:
#                 while True:
#                     if generated_product_code not in product_codes:
#                         instance.product_code = generated_product_code
#                         instance.save()
#                         break
#                     generated_product_code = int('1' + str(generateProductCode(12)))
                    
#         if instance.barcode == "" and instance.product_code != "":
#             code128 = barcode.get_barcode_class('code128')
#             generated_barcode = code128(str(instance.product_code), writer=ImageWriter())
#             buffer = BytesIO()
#             generated_barcode.write(buffer)
#             instance.barcode.save(f'{instance.name}.png', File(buffer), save=False)
#             instance.save()