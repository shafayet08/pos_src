from django.db import models
from django.conf import settings
from django.db.models.fields import CharField, EmailField
from django.utils.translation import ugettext_lazy as _
from django.utils.text import slugify


from PIL import Image
import barcode
from barcode.writer import ImageWriter
from io import BytesIO
from random import randint

from base_model.base import CommonInfo





# Callable function to create product code and barcode start
def generateProductCode(number):
    starting_range = 10**(number-1)
    ending_range = (10**number)-1
    return randint(starting_range, ending_range)


def need_to_generate_product_code():
    products = 0
    if Product.objects.all().count() != 0:
        products = Product.objects.all()

    product_codes = []
    if products:
        product_codes = [p.product_code for p in products]

    while(1):
        generated_product_code = generateProductCode(13)
        ean13 = barcode.get('ean13', str(generated_product_code), writer=ImageWriter())
        ean = str(ean13)
        if ean not in product_codes:
            break
    return ean13

# Callable function to create product code and barcode start  ends



'''
Creating Category table
'''
class Category(models.Model):
    KG      = 'kg'
    PIECE   = 'piece'
    CFT     = 'cft'

    CHOICES = [
        (KG, _('Kg')),
        (PIECE, _('Piece')),
        (CFT, _('Cft')),
    ]

    name         = models.CharField(max_length=30, verbose_name='Category Name')
    cover_pic    = models.ImageField(upload_to='category_cover_pic', default='category_cover_pic/cat_default.jpg')
    product_unit = models.CharField(max_length=5, choices=CHOICES, null=True)
    added_by     = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True, blank=True, related_name='add+')
    updated_by   = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True, blank=True, related_name='edit+')

    def __str__(self):
        return self.name


class Product(models.Model):
    category                    = models.ForeignKey(Category, on_delete=models.SET_NULL, null=True)
    name                        = models.CharField(max_length=30, unique=True)
    purchase_qty                = models.IntegerField(default=0)
    product_code                = models.CharField(max_length=13, unique=True, blank=True)
    product_pic                 = models.ImageField(upload_to='product_pic', default='product_pic/default_product_img.jpg', verbose_name='Product Image')
    barcode                     = models.ImageField(upload_to='barcode/', blank=True)
    description                 = models.TextField(max_length=200, blank=True)
    brand                       = models.CharField(max_length=20, null=True)
    cost_price                  = models.IntegerField(default=0)
    selling_price               = models.IntegerField(default=0)
    discount                    = models.IntegerField(default=0)
    is_discount_in_parcentage   = models.BooleanField(default=True, verbose_name='Discount %')
    created                     = models.DateTimeField(auto_now_add=True)
    updated                     = models.DateTimeField(auto_now=True)
    added_by                    = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True, related_name='add+')
    updated_by                  = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True, related_name='edit+')

    def __str__(self):
        return f'{self.name}-{self.product_code}'

    @property
    def get_discount(self):
        discount = self.discount
        if self.is_discount_in_parcentage == True:
            discount = (self.selling_price * self.discount) / 100
        return discount

    @property
    def get_selling_price_after_discount(self):
        return self.selling_price - self.get_discount

    @property
    def get_product_slug(self):
        return slugify(self.name)


    # def save(self, *args, **kwargs):
    #     self.name = slugify(self.name)

    #     is_product_code = False
    #     if not self.product_code:
    #         product_code = need_to_generate_product_code()
    #         is_product_code = True

    #     if self.product_code:
    #         if len(str(self.product_code)) < 13:
    #             product_code = need_to_generate_product_code()
    #             is_product_code = True

    #         if len(str(self.product_code)) == 13:
    #             product_code = self.product_code
    #             is_product_code = True

    #     if is_product_code:
    #         self.product_code = product_code
    #         print(self.product_code)


    #         ean = barcode.get('code128', str(product_code), writer=ImageWriter())
    #         options = {
    #             'module_width': 0.1,
    #             'module_height': 1.2,
    #             'text_distance': 0.3,
    #             'font_size': 5,
    #             'quiet_zone': 0.5,
    #         }
    #         ean.save('media/barcode/' + self.name, options)
    #         self.barcode = ('barcode/'+ self.name + ".png")
    #         # instance.save()


    #     super().save(*args, **kwargs)


    def save(self, *args, **kwargs):
        self.name = slugify(self.name)

        is_product_code = False
        if not self.product_code:
            product_code = need_to_generate_product_code()
            is_product_code = True

        if self.product_code:
            product_code = self.product_code
            # product_code = barcode.ean.EuropeanArticleNumber13(str(product_code), writer=ImageWriter(), no_checksum=False,)
            print('before: ',product_code)
            product_code = barcode.get('ean13', str(product_code), writer=ImageWriter())
            is_product_code = True
            print('after: ',product_code)
        if is_product_code:
            self._extracted_from_save(product_code)


        super().save(*args, **kwargs)


    def _extracted_from_save(self, product_code):

        # ean = barcode.get('ean13', str(product_code), writer=ImageWriter())
        options = {
            'module_width': 0.2,
            'module_height': 3.0,
            'text_distance': 1,
            # 'font_size': 5,
            'quiet_zone': 1.0,
        }
        product_code.save('media/barcode/' + self.name, options)

        self.barcode = ('barcode/'+ self.name + ".png")
        self.product_code = product_code
