from django.contrib import admin
from django.urls import path, include

from django.contrib.auth.views import (
    PasswordResetView, 
    PasswordResetConfirmView, 
    PasswordResetDoneView, 
    PasswordResetCompleteView
)
from django.contrib import admin

admin.site.site_header = 'XoftNology'                      # default: "Django Administration"
admin.site.index_title = 'XoftNology Dashboard'          # default: "Site administration"
admin.site.site_title = 'Admin' # default: "Django site admin"


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('products.urls', namespace='products')),
    path('', include('customers.urls', namespace='customers')),
    path('', include('suppliers.urls', namespace='suppliers')),
    path('', include('sales.urls', namespace='sales')),
    path('', include('inventories.urls', namespace='inventories')),
    path('', include('expenses.urls', namespace='expenses')),
    path('', include('purchases.urls', namespace='purchases')),
    path('', include('accounts.urls', namespace='accounts')),
    path('', include('users.urls',  namespace='users')),
    path('', include('app_settings.urls', namespace='app_settings')),

    path('password-reset/', PasswordResetView.as_view(
        template_name='users/password_reset/password_reset.html'), 
        name='password_reset'), 

    path('password_reset/done', PasswordResetDoneView.as_view(
        template_name='users/password_reset/password_reset_done.html'), 
        name='password_reset_done'),  

    path('password_reset_confirm/<uidb64>/<token>/', PasswordResetConfirmView.as_view(
        template_name='users/password_reset/password_reset_confirm.html'), 
        name='password_reset_confirm'),

    path('password_reset/complete/', PasswordResetCompleteView.as_view(
        template_name='users/password_reset/password_reset_complete.html'),
        name='password_reset_complete'),



]

from django.conf import settings
from django.conf.urls.static import static

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)