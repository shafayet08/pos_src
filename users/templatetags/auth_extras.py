# from django import template
# from django.contrib.auth.models import Group 

# register = template.Library()

# @register.filter(name='has_group')
# def has_group(user, group_name): 
#     group = Group.objects.get(name=group_name) 
#     return  group in user.groups.all()


# @register.filter(name='has_not_group_permission')
# def has_not_group_permission(user, group_name): 
#     group = Group.objects.get(name=group_name)
#     return  group not in  user.groups.all()

# @register.filter(name='return_group')
# def return_group(user, group_name): 
#     group = Group.objects.get(name=group_name).name
#     return  str(group) 


"""
How to use:
    request.user|has_not_group_permission:"manager"
"""