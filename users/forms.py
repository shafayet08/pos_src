from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import Group
from django.db import models
from django.db.models.fields import CharField

from users.models import User
class UserCreationForm(UserCreationForm):
    # first_name = forms.CharField(required=True)
    # last_name = forms.CharField(required=True)
    # email = forms.EmailField(required=True)
    
    class Meta:
        model = User
        fields = ['email', 'username', 'password1', 'password2', 
            'is_salesman', 'is_manager', 'is_owner',]

class UserUpdateForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ['email', 'username', 'is_salesman', 'is_manager', 'is_owner',]


class GroupForm(forms.ModelForm):
    class Meta:
        model = Group
        fields = '__all__'

# class ProfileForm(forms.ModelForm):
#     class Meta:
#         models = Profile
#         fields = '__all__'
#         exclude = ['is_verified']

