from django.contrib.auth.base_user import BaseUserManager
from django.db import models
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from django.core.mail import send_mail
from django.contrib.auth.models import AbstractBaseUser, AbstractUser, PermissionsMixin

import uuid

class UserManager(BaseUserManager):
    def create_superuser(self, email, username, password, **extra_fields):
        extra_fields.setdefault('is_verified', True)
        extra_fields.setdefault('is_active', True)
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_salesman', True)
        extra_fields.setdefault('is_manager', True)
        extra_fields.setdefault('is_owner', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_active') is not True:
            raise ValueError(_('Superuser must have is_active=True'))
        if extra_fields.get('is_salesman') is not True:
            raise ValueError(_('Superuser must have is_salesman=True'))
        if extra_fields.get('is_manager') is not True:
            raise ValueError(_('Superuser must have is_manager=True'))
        if extra_fields.get('is_manager') is not True:
            raise ValueError(_('Superuser must have is_manager=True'))
        if extra_fields.get('is_owner') is not True:
            raise ValueError(_('Superuser must have is_owner=True'))  

        return self.create_user(email=email, username=username, password=password, **extra_fields) 

    def create_user(self, email, username, password, **extra_fields):
        if not email:
            raise ValueError(_('You must provide an email address')) 

        email = self.normalize_email(email)
        user = self.model(email=email, username=username, **extra_fields)
        user.set_password(password)
        user.save()
        return user



class User(AbstractBaseUser, PermissionsMixin):
    def generate_unique_id():
        try:
            users = User.objects.all()
        except:
            users = False
        
        if users:
            while True:
                id = uuid.uuid4()
                auth_token = User.objects.filter(auth_token=id)
                if not auth_token:
                    break
        else:
            id = uuid.uuid4()

        return id
    email = models.EmailField(_('email address'), unique=True)
    username = models.CharField(max_length=30, unique=True)
    first_name = models.CharField(max_length=30, blank=True)
    last_name = models.CharField(max_length=30, blank=True)
    contact = models.CharField(max_length=11, blank=True)
    location = models.CharField(max_length=100, blank=True)
    bio = models.TextField(max_length=500, blank=True)
    date_joined = models.DateTimeField(default=timezone.now)
    avatar = models.ImageField(upload_to='users/', null=True, blank=True)
    
    auth_token = models.CharField(max_length=100, default=generate_unique_id())
    is_verified = models.BooleanField(default=False)

    is_active = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)
    is_salesman = models.BooleanField(default=False)
    is_manager = models.BooleanField(default=False)
    is_owner = models.BooleanField(default=False)
    

    objects = UserManager()
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username']

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')

    def __str__(self):
        return self.username

    def get_full_name(self):
        """
        Return the first_name plus the last_name, with a space in between.
        """
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()


    def get_short_name(self):
        """Return the short name for the user."""
        return self.first_name

    def email_user(self, subject, message, from_email=None, **kwargs):
        """Send an email to this user."""
        send_mail(subject, message, from_email, [self.email], **kwargs)




































# from django.db import models
# from django.contrib.auth.models import User

# import uuid
# # Create your models here.

# class Profile(models.Model):
#     user = models.OneToOneField(User, on_delete=models.SET_NULL, null=True)
#     first_name = models.CharField(max_length=50, null=True)
#     last_name = models.CharField(max_length=50, null=True)
#     image = models.ImageField(upload_to='profile_images', default='profile_images/profile_default.jpg')
#     nid    = models.FileField(upload_to='users_nid', blank=True, null=True)
#     phone1 = models.CharField(max_length=11)
#     phone2 = models.CharField(max_length=11, blank=True, null=True)
#     address = models.CharField(max_length=100)
#     auth_token = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
#     is_verified = models.BooleanField(default=False)
#     created = models.DateTimeField(auto_now_add=True)

#     def __str__(self):
#         return str(self.user)

