# import uuid
# from django.contrib import messages
# from django.db.models.signals import post_save, pre_save
# from django.dispatch import receiver
# from django.contrib.auth.models import User
# from django.core.files.base import ContentFile
# from django.core.mail import send_mail
# from django.conf import settings

# from io import BytesIO
# from PIL import Image


# from users.models import Profile

# def send_mail_after_user_creation(mail_to, auth_token, username):
#     subject = "Your account needs to be verified"
#     message = f"Hi {username}! Click the link to verify your account http://localhost:8000/verify-account/{auth_token}"
#     email_from = settings.EMAIL_HOST_USER
#     recipient_list = [mail_to,]

#     send_mail(
#         subject,
#         message,
#         email_from,
#         recipient_list,
#         fail_silently=False,
#         )

# @receiver(post_save, sender=User)
# def create_user_profile(sender, instance, created, **kwargs):
#     if created:
#         email = instance.email
#         profile = Profile.objects.create(user=instance, first_name=instance.first_name, last_name=instance.last_name )

#         username = instance.username
#         auth_token = profile.auth_token
#         send_mail_after_user_creation(email, auth_token, username)


# @receiver(post_save, sender=User)
# def save_user_profile(sender,instance, created, **kwargs):
#     profile = instance.profile
#     profile.first_name = instance.first_name
#     profile.last_name = instance.last_name
#     profile.save()



# @receiver(pre_save, sender=Profile)
# def generate_thumbnail(sender, instance, **kwargs):
#     image = Image.open(instance.image)
#     image = image.convert("RGB")

#     if image.height > 300 or image.width > 300:
#         THUMBNAIL_SIZE = (300, 300)
#         image.thumbnail(THUMBNAIL_SIZE)
#         temp_thumb = BytesIO()
#         image.save(temp_thumb, 'JPEG')
#         temp_thumb.seek(0)

#         instance.image.save(
#             instance.image.name,
#             ContentFile(temp_thumb.read()),
#             save=False,
#         )
#         temp_thumb.close()