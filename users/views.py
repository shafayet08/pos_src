from django.http.response import Http404, HttpResponse
from django.shortcuts import redirect, render
from django.contrib import messages

from django.contrib.auth import login as auth_login
from django.contrib.auth import logout as auth_logout
from django.contrib.auth import authenticate
from django.contrib.auth.decorators import login_required, user_passes_test

from django.core.mail import send_mail
from django.conf import settings

from users.forms import UserCreationForm, UserUpdateForm
from users.models import User



# Create your views here.
@login_required()
@user_passes_test(lambda u: u.is_active or u.is_manager or u.is_owner or u.is_superuser)
def user(request):
    subject = "Your account needs to be verified"
    message = 'Hello shafayet! '
    # email_from = settings.EMAIL_HOST_USER
    # recipient_list = ['shafayetbs@gmail.com',]

    # send_mail(
    #     subject,
    #     message,
    #     email_from,
    #     recipient_list,
    #     fail_silently=False,
    #     )


    users = User.objects.all()
    context = {
        'title': 'Users',
        'users': users,
    }
    return render(request, 'users/user.html', context)





@login_required()
@user_passes_test(lambda u: u.is_active)
@user_passes_test(lambda u: u.is_active or u.is_manager or u.is_owner or u.is_superuser)
def update_user(request, pk):
    creator = request.user
    user = User.objects.get(id=pk)
    form = UserUpdateForm(instance=user)
    if request.method == 'POST':
        form = UserUpdateForm(request.POST, instance=user)
        if form.is_valid():
            return _extracted_from_update_user(form, creator, request)
        else:
            print(form.errors)
    context = {
        'title': 'Update User',
        'form': form,
    }
    return render(request, 'users/update-user.html', context)
    

def _extracted_from_update_user(form, creator, request):
    username = form.cleaned_data.get('username')
    designation = form.cleaned_data.get('designation')
    # print(creator.groups.get(name='manager').name)

    if str(designation) in ['manager', 'owner']:
        try:
            if str(creator.groups.get(name='manager').name) == 'manager':
                messages.error(request, 'Oops! You do not have permission to update into manager/owner.')
                return redirect('users:user')
        except:
            pass

    form.save()

    user = User.objects.get(username=username)
    user.groups.clear()
    user.groups.add(designation)

    messages.success(request, 'User has been updated')
    return redirect('users:user')



@login_required()
@user_passes_test(lambda u: u.is_active or u.is_manager or u.is_owner or u.is_superuser)
def create_user(request):
    form = UserCreationForm()
    creator = request.user
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            designation = form.cleaned_data.get('designation')
            if str(designation) in ['manager', 'owner']:
                try:
                    if str(creator.groups.get(name='manager').name) == 'manager':
                        messages.error(request, 'Oops! You do not have permission to update into manager/owner.')
                        return redirect('users:user')
                except:
                    pass
            form.save()
            
            user = User.objects.get(username=username)
            user.groups.add(designation)
            messages.success(request, 'An email has been sent to verify this account')
            return redirect('users:user')
    context = {
        'title': 'Create User',
        'form': form
    }

    return render(request, 'users/user-creation-form.html', context)


def verify_account(request, auth_token):
    try:
        user = User.objects.get(auth_token=auth_token)
        
        if user:
            if user.is_verified:
                messages.info(request, 'Your account is already verified')
                return redirect('users:login')

            user.is_verified = True
            user.save()

            messages.success(request, 'Your account has been verified. You can login now.')
            return redirect('users:login')
  
    except:
        return HttpResponse('Something went wrong')


# User authentication for login
def login(request):
    error = False
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        print(username)
        try:
            user = User.objects.get(email=username)
            print(user)
            if user is not None:
                if not user.is_verified:
                    messages.error(request, 'Your account needs to be verified.')
                    return redirect('users:login')
            
                if user.is_verified:
                    user = authenticate(request, username=username, password=password)
                    if user is not None:
                        auth_login(request, user)
                        messages.success(request, f'Hey {username}! You have been logged in.')
                        return redirect('products:dashboard')
                    else:
                        error = True
        except Exception as e:
            messages.error(request, e)
            return redirect('users:login')

    context = {
        'title': 'POS | Login',
        'error': error,
    }
    
    return render(request, 'users/login.html', context)
                 


# This works for logout
def logout(request):
    auth_logout(request)
    messages.success(request, f'Hey! You have been logged out.')
    return redirect('users:login')


