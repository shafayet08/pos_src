from django.urls import path
from users import views

app_name = 'users'
urlpatterns = [
    path('user/', views.user, name='user'),
    path('update-user/<int:pk>/', views.update_user, name='update_user'),
    path('create-user/', views.create_user, name='create_user'),
    path('verify-account/<auth_token>/', views.verify_account, name='verify_account'),
    path('login/', views.login, name='login'),
    path('logout/', views.logout, name='logout'),
    
]