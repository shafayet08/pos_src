from django.contrib.auth import REDIRECT_FIELD_NAME
from django.contrib.auth.decorators import user_passes_test
from django.contrib import messages
from django.shortcuts import redirect


# def allowed_users(allowed_roles=[]):
#     def decorator(view_func):
#         def wrapper_func(request, *args, **kwargs):
#             if request.user.groups.exists():
#                 group = request.user.groups.first().name
#                 if group in allowed_roles:
#                     return view_func(request, *args, **kwargs)
#                 else:
#                     messages.error(request, 'You are not authorized to view this page.')
#                     return redirect('users:login')
#             else:
#                 messages.error(request, 'You are not authorized to view this page.')
#                 return redirect('users:login')
#         return wrapper_func
#     return decorator